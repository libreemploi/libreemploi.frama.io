from jinja2 import Environment, PackageLoader, select_autoescape

import os
import yaml

header = """---
template: blog.html
---
"""


# on se crée un environnement jinja2 avec les templates du dossier blog_templates
env = Environment(
    loader=PackageLoader('generate_blog','blog_templates'),
    autoescape=select_autoescape(['html', 'xml'])
)

blog_root = "docs/blog/"
blog_index ="docs/blog.md"

# on définit le template jinja pour formatter le résumé de chacun des posts
blog_post_template = env.get_template("post.md")

sourcefiles = []

## Récup les markdowns à la racine des articles de blog
for root, dirs, files in os.walk(blog_root):
    for file in files:
        f = os.path.join(root,file)
        sourcefiles.append(f)
    break

summaries = []
sourcefiles.sort(reverse = True)

# Pour chacun des markdown
for file in sourcefiles:
    # on ouvre et on lit, on va séparer les metadata du contenu,
    # on attend 3 occurences des triple dash "---"
    # les deux premières encadrent les metadata en yaml
    # la 3ème sépare l'accroche du contenu, on ne gardera que l'accroche
    # dans le résumé.
    print("Formattage du résumé de {}...".format(file))
    with open(file) as handle:
        lines = handle.readlines()
        foundHeader = 0
        yml = []
        md = []
        i = 0
        for line in lines:
            if line.startswith("---"):
                foundHeader += 1
            else:
                if foundHeader == 1: # On est positionné dans les metadata en yaml 
                    yml.append(line)                
                elif foundHeader == 2: # On est dans l'accroche du blog post
                    md.append(line)
                elif foundHeader == 3: # On vient de passer la fin de l'accroche : terminé!
                    break
        
        # Forger le nom de l'url du blogpost
        url = os.path.basename(file)
        url = os.path.splitext(url)[0]
        
        # Forger les contenus en single string
        yamlcontent = "".join(yml)
        markdowncontent = "".join(md)

        # Convertir les metadata texte en objet yaml
        yml_obj = yaml.safe_load(yamlcontent)

        # Utiliser le template jinja2 pour formatter le résumé d'article en .md
        post = blog_post_template.render(meta=yml_obj, contents=markdowncontent, url=url)

        # ... et l'ajouter aux résumés
        summaries.append(post)
        handle.close()

with open(blog_index, "w") as index:
    # on écrit le header..
    index.write(header)
    # puis on se met à écrire tous les résumés à la suite dans l'index du blogpost
    index.writelines(summaries)
    index.close()

    print("Génération de {} terminée!".format(blog_index))