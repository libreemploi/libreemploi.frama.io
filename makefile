# make clean
clean:
	@echo "Cleaning up workspace..."
	@rm -rf .venv
	@git reset --hard
	@echo "Done!"
	
# make install
install:
	@echo "Creating new pyton virtualenv under .venv..."
	@python -m venv .venv;
	@echo "Installing Requirements..."
	source .venv/bin/activate; \
	pip install -r requirements.txt
	
# make build
build:
	@echo "Building the website...";
	@source .venv/bin/activate; \
	python ./generate_blog.py; \
	mkdocs build
	@echo "Build Completed!"
	
# make serve 
serve:
	@echo "Building the website...";
	@source .venv/bin/activate; \
	python ./generate_blog.py; \
	mkdocs serve
