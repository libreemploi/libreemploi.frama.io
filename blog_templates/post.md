{% if meta.image %}
![{{meta.title}}](blog/{{meta.title}}/{{meta.image}})
{% endif%}
# [{{ meta.title }}]({{ url }})
écrit par _{{ meta.author }}_ le {{ meta.date }}

{{ contents }}

[lire en entier]({{ url }})

---

