![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

# Pré-requis

Rien d'exceptionnel, des packages souvent présents dans les bonnes distributions linux. Devrait marcher également en WSL pour ceux sous windows.

```
python3
    - pip
    - venv
cmake (si utilisation du makefile)
```

# Préparer le workspace

```
git clone https://framagit.org/libreemploi/libreemploi.frama.io.git
cd libreemploi.frama.io
```

# Build

Deux méthodes, franchement je conseille la méthode avec CMake, plus propre, qui s'occupe toute seule du virtualenv

## Avec CMake

```shell
# clean du workspace
$ make clean

# préparation au build (juste une fois, ou après un clean)
$ make install

# build du site statique
$ make build

# mode serveur de développement
$ make serve
```

Lorsque l'on travaille en `make serve` on peut accéder à notre serveur de développement via l'URL http://127.0.0.1:8000/

## Manuellement

Ce n'est pas recommandé, mais bon si vous y tenez...

#### Install et Clean

```
# Pr
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

#### Build

Une seule fois par session de terminal

```
source .venv/bin/activate
```

puis build avec

```
python ./generate_blog.py
mkdocs build
```

ou passage en serveur de développement:

```
python ./generate_blog.py
mkdocs serve
```
