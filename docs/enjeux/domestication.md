# La Domestication Numérique

> Note: Je n'arrive plus à retrouver où j'ai entendu ce terme de domestication numérique, probablement dans une conférence du camarade Benjamin Bayart, mais à ce jour je n'ai pas retrouvé la source. N'hésitez pas à m'envoyer un message si jamais vous avez la référence.

> **J'ai souvent cherché un terme qui définit les comportements des utilisateurs face à la technologie et qui soit suffisamment pertinent pour faire un parallèle avec un concept que nous connaissons bien. Au détour d'une conférence, ce mot "domestication" m'est apparu comme le terme que je cherchais, car il met en avant un certain nombre de concepts très justes qui décrivent notre enfermement technologique, librement consenti.**
>
> **A travers cet article, nous allons essayer de voir ce qui caractérise cette domestication, comment elle est mise en oeuvre, et comment nous pouvons nous en sortir si nous le souhaitons.**

![Adam Shrez - Monochrome Bull in Alley - Unsplash](/images/adam-sherez-MC1zyHbmfMg-unsplash-cropped-scaled.jpg)

La Domestication Numérique, est un terme emprunté à un vocabulaire animalier, ou encore du servage. Volontairement et à dessin de mettre en parallèle l'aliénation volontaire de l'usager du numérique par ceux qui contrôlent la technologie :

- Principalement, les GAFAM
- Les Constructeurs matériels
- Les Monopoles des Systèmes informatiques (Windows/Android)

Cette domestication prend des formes variables en fonction des usagers mais en général elle fonctionne comme un système d'asservissement et d'aliénation librement consenti par les utilisateurs du numérique :

- **Dépendance** à une technologie, un logiciel, un matériel, un service payant
- **Réalité distordue** : Atteintes aux libertés fondamentales, Contraintes arbitraires, Perte de contrôle
- **Simplification** / **Opacification** technologique : infantilisation des utilisateurs
- La plupart du temps **librement consentie**, sans y voir de problème (voir même des bienfaits)

## Définitions 

Avant de se plonger plus en détail dans ce qui constitue la domestication numérique, prenons le temps d'en étudier les contours, à travers les définitions auquel elle fait référence.

Wikipedia nous décrit la [domestication](https://fr.wikipedia.org/wiki/Domestication) comme l'acquisition, la perte, ou le développement de caractères morphologiques, psysiologiques ou comportementaux nouveaux et héréditaires, résultant d'une interaction prolongée, d'un contrôle, voire d'une sélection délibérée de la part des communautés humaines. 

### Domestication animale

Le concept de domestication numérique est principalement emprunté au vocabulaire animal (agricole/familiers) : 

- Domestiqués pour être exploités (laine, lait, oeufs)
- Domestiqués pour être tués/mangés
- Privés de libertés (animaux domestiques / familiers)

### Domestiques de maison

Un autre concept est celui des Domestiques en tant qu'employés de maison. Employés aidant au confort du domicile d'une famille bourgeoise, de manière consentie et moyennant traitement (et parfois salaire)

Leur statut est Différent de l'esclave, qui lui est forcé et possédé contre son gré. Les domestiques sont un peu comme des employés 

### Par extension, au numérique

Le concept Domestication numérique reprend donc sa définition générale en mettant en avant plusieurs facteurs communs:

- L'habituation progressive à des philosophies privatrices
- La dépendance à une entité domesticatrice
- La difficulté grandissante à s'émanciper, se réensauvager

La triste réalité est que nous sommes (et avons été) conditionnés à des comportements de "consommateurs" numériques qui nous éloignent de notre propre émancipation. Bien heureusement, ce n'est qu'une situation transitioire et nous pouvons nous libérer de nos chaines.

---

# Caractères de la domestication numérique

![Engin Aryurt - Fine Art Portrait - Unsplash](/images/engin-akyurt-l1clu1ZKjSw-unsplash-cropped-scaled.jpg)

Librement consentie, la domestication engendre une soumission, une dépendance, une aliénation:

- **Soumission** : perte de contrôle. Acceptation de règles arbitraires et changeantes. Acceptation des changements tacites "or leave"
- **Dépendance** : difficulté a changer de logiciel, se passer d'un service
  - non interopérables "t'es pas sur facebook ?" contamination sociale, convergence vers des services monopolistiques (comparaison e mail vs facebook)
  - investissement et non mobilité des données : données non transférables, récupération difficile et souvent opaque.
  - Achats contraints a une plateforme (jeux, apps), quitter c'est tout perdre, être banni c'est tout perdre.

- **Aliénation** : Modifications  des TOS sur le temps. Acceptation de l'inacceptable par la dépendance (twitter ère musk). Pas d'alternative. Normalisation des systèmes privateurs (ex: suppression blocages X "as a feature" )

On a une acceptation de l'inacceptable, progressive, suivant une dégradation du service par des atteintes légères, répetées et progressives.  On est en plein dans le syndrome de la [grenouille dans la casserole](https://fr.wikipedia.org/wiki/Fable_de_la_grenouille).

---

# Reconnaître la domestication

![Hugh Han - People in Shibuya Train distracted by their phones - Unsplash](/images/hugh-han-5pkYWUDDthQ-unsplash-cropped-scaled.jpg)

Afin de reconnaitre notre état de domestication, il est important de connaitre les armes employées par les systèmes domesticateurs :

- **Services attrayants**, effets de mode, **UX** (expérience utilisateur) et modernité, beaucoup d'effort est mis sur la qualité de l'expérience utilisateur (au moins au départ)
- **Simplification** de l'expérience utilisateur : "Ça marche tout seul". Le système fonctionne très bien mais de manière **opaque**, avec très peu de contrôle ou de compréhension de la part de l'utilisateur. Dépendance à un service technique (On s'occupe de ça pour vous). 
- Constitution d'une **base utilisateur conséquente** et sa **rétention** (acquisition & retention). Utilisation de stratagèmes d'ingénierie sociale : cooptation, rareté, bénéfices aux pionniers, beta tests fermés sur tirage au sort.
- **Sollicitations intempestives**: qualité dégradée des applications en mode web afin de favoriser l'expérience avec une App (incluant trackers et notifications) : en passant par une application, le système peut vous notifier en permanence et monitorer votre activité si vous n'êtes pas assez actif.
- **Difficultés a se détacher** / quitter le service : suppression de compte difficile, culpabilisation (ce que vous allez perdre). Délai de suppression et e-mails intempestifs)
- **Systèmes non interopérables, concurrents et non transférables** :  Attention toutefois aux systèmes interopérables et ouverts (stratégie du embrace, extend, extinguish)
- **Multiplication des services** et écosystèmes complexes : Par exemple, le trio Facebook, Instagram, Whatsapp de meta. Partage des données entre les services. Complexification et difficulté à garder ces données contenues sur un seul service.
- **Dégradation lente du service** : limitation, suppression ou mise en premium de features. Atteintes progressives aux libertés,  rachat et/ou prise de contrôle par des actionnaires prédateurs (Twitter par Elon Musk)

On pourrait faire le parallèle avec le "Techno-Cocon" si justement qualifié par Alain Damasio, une douce et agréable prison qui se renforce à mesure que l'on y reste.

### Signes et phrases-type d'utilisateurs domestiqués

> Heureusement qu'ils sont là pour s'occuper de ça pour nous, on y comprend rien à l'informatique

> Oh, qu'ils nous prennent nos données personnelles, de toute façon on a rien à cacher

> C'est vraiment pratique, ils s'occupent de tout tout seul, on a même toutes les photos directement sur internet.

> Ah je sais pas, j'ai cliqué sur le bouton parce qu'ils me disaient de le faire.

# Se ré-emanciper, se ré-ensauvager

![Steven Erixon - Unsplash](/images/steven-erixon-ZgGQCCPBKag-unsplash-cropped-scaled.jpg)

On ne va pas se mentir, déjà la prise de conscience est compliquée à vivre et à intégrer, mais sortir de cet état d'aliénation l'est encore plus. 

Franchir le pas, c'est un peu comme arrêter de fumer. Un bienfait a travers un effort, mais un effort heureux.

### Sortir de l'impuissance (et des préjugés)

La première étape pour se ré-ensauvager est de sortir de l'impuissance. Tout comme l'on est persuadé que se sevrer du tabac est un acte incommensurable et très difficile ; toutefois il n'en est rien, pour peu que nous soyons entourés des bonnes personnes et accompagné dans la démarche.

La phrase la plus typique de l'Impuissance est "Mais pour utiliser quoi d'autre ? Je vais pas retourner à l'âge de pierre !"

Quitter facebook, windows, ou bien tout autre système privateur ne signifie en rien que nous allons revenir à la bougie. Il existe de nombreux autres services qui peuvent aisément les remplacer. L'important est déjà d'en prendre conscience.

Une autre erreur est souvent que nous nous sentons pris au piège car bouger, c'est s'éloigner de sa zone de confort, mais aussi de ses relations sociales. Pour un réseau social, c'est en effet problématique, car celà reviendrait à convaincre nos amis de nous suivre sur un nouveau réseau. Toutefois, c'est quelque chose qui peut être discuté, avec certains de vos proches, de l'envie de changer. L'essentiel est de se souvenir : si personne n'en parle ou fait le premier pas, personne ne va nulle part.

Pour ce qui est de changements de logiciels qui n'impliquent pas vos relations sociales, c'est en revanche une tâche bien plus aisée : le seul frein au changement, est principalement la peur de l'inconnu. Et puis qu'est-ce que nous avons à perdre à essayer autre chose ?  On n'est pas obligé de tout plaquer. Il existe plein de manières de tester avant de franchir le pas. Après tout, on quitte un monde toxique pour un monde qui nous laisse bien plus de liberté.

### Savoir lâcher prise : quitter un service toxique.

Une fois décidé à franchir le pas, la première des choses est de se dire que nous ne quittons pas un service toxique de manière irrévocable, mais nous allons nous efforcer d'aller vers des services et logiciels plus vertueux.

Tout en gardant à l'esprit ce qui nous enfermait, pourquoi et comment nous étions prisonniers du système, il devient alors plus aisé de transitionner vers quelque chose de différent, pas fondamentalement, car les gens du libre sont des gens de sens : les outils, logiciels et services qui sont développés sont pour la plupart très similaires à leurs alternatives privées privatrices : en résumé, **vous ne serez pas dépaysé**.

La grande différence entre l'avant et l'après, est que la plupart de ces nouveaux services que vous allez rencontrer, n'ont pas d'attentes particulières de votre part. Ici, pas de rétention, de notifications intempestives, ou encore de course à votre attention : le but est "juste un service" pour vous. Si vous souhaitez partir, vous avez des outils rapides et faciles pour exporter vos données et les utiliser ailleurs.

# Le retour à la liberté

![Shane Rounce - Unsplash](/images/shane-rounce-DNkoNXQti3c-unsplash-cropped-scaled.jpg)

Une fois le pas franchi, nous voici en route vers la liberté, le réensauvagement. Revenir à la liberté ne se fait pas non plus sans un changement des attitudes et de notre état d'esprit de consommateur.

Souvenez-vous : nous souhaitons reprendre le contrôle, et par celà nous avons un changement de paradigme à opérer : Nous ne sommes plus des consommateurs d'un service, mais usagers d'un bien commun. On ne parle plus à des entreprises, mais à des humains, désireux de mettre en commun leur passion à travers des outils utiles et non basés sur le profit.

Les enjeux ne sont alors plus les mêmes : le libre n'a que faire de se faire de l'argent sur votre dos ; on est plus dans une relation associative, où les dons et les contributions servent à faire évoluer le projet, et non des profits. On assiste à un changement radical dans la relation : fini le client, je deviens utilisateur et pourquoi pas contributeur.

On pourra toujours formuler un certain nombre de critiques des logiciels libres:

- L'expérience utilisateur est légèrement en deçà des solutions propriétaires (pas du tout les mêmes moyens financiers, et de temps d'investissement). Toutefois, elle est en évolution permanente, et les équipes de design de logiciel libres sont très ouvertes à la discussion / amélioration : Le Développement collaboratif est de mise et on y voit même les contributions de nombreux professionels des interfaces graphiques.
- Le changement de paradigme nous rend acteur, et nous demande une moindre passivité face à nos usages. Nous ne sommes plus de simples consommateurs : Après tout, on l'a bien voulu ce ré-ensauvagement?

# Conclusion

Nous venons de voir différents aspects de la domestication numérique, tant dans l'approche que les acteurs de la technologie ont vis à vis du consommateur, que dans nos pratiques, biais cognitifs et freins qui nous enferment dans cette même domestication. 

Cette boucle de rétroaction ne mène à rien de bon et s'alimente d'autant plus que la technologie se complexifie (par exemple, aujourd'hui avec l'avènement des modèles de deep learning, aussi qualifiés d'intelligence artificielle), elle cherche à nous maintenir dans un état de dépendance, et d'impuissance : la technologie avance, mais nous n'y comprenons toujours rien... car on ne nous laisse même pas chercher à la comprendre.

Toutefois, il existe d'autres chemins, qui demandent une libération. Ces chemins sont pavés par les acteurs du libre et de la technologie ouverte. Décriés par les gros acteurs car "trop complexes" ou "en retard sur la technologie", ils ont une image très négative auprès du public ; image qui, lorsqu'on essaie un peu les logiciels libres, retrouve de sa superbe, et ne correspond pas aux critiques "amish" et "pour barbus" que certains veulent bien lui prêter.

Enfin, la libération de cette domestication ne se fait pas sans un changement d'attitude face à la technologie. Il existe tout un pan de la technologie dont les intentions ne sont pas notre asservissement, ni un intérête de croissance de la consommation. Le monde du libre est un monde qui s'est libéré de sa domestication (ou l'a t'il seulement été?) : il est composé de personnes qui oeuvrent aux communs et investissent un peu de temps, à hauteur de ce qu'ils savent. 

Se ré-ensauvager ne nécessite pas de devenir programmeur linux, bien au contraire, nous restons des utilisateurs, mais des utilisateurs maitres de leur technologie, qui savent dépanner, améliorer, discuter et même aider les autres. Une grande communauté d'entraide, au service du commun.
