# Les grands enjeux du libre emploi

![person using computer on brown wooden table - Robert Bye - unsplash ](/images/banner-robert-bye-BY34glOW7wA-unsplash-cropped-scaled.jpg)



Le fait de pouvoir **employer librement** nos appareils numériques est essentiel pour de nombreuses raisons : notamment, il permet de **réduire notre dépendance** aux constructeurs de matériel et développeurs logiciels dont les objectifs ne sont clairement pas les mêmes que les notres :

| Consommateurs                                                | Développeurs                                                 | Constructeurs                                                |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| - Fiabilité et résistance aux pannes<br />- Performance durable<br />- Compatibilité des périphériques<br />- Mises à jour et support<br /> | - Vente de licences en flux continu (abonnements)<br />- Nouvelles Fonctionalités, nouveaux produits<br />- Adaptation au(x) nouveau(x) matériel(s) | - Renouvellement périodique des machines<br />- Déprécation et nouvelles normes<br />- Mise à jour du design : modes<br /> |

On ne va pas se voiler la face : les constructeurs de matériel, ainsi que les développeurs de logiciels privateurs, ont pour principal objectif **la rentabilité de leur modèle économique** : 

- Il faut que leurs produits soient **attractifs**, mais laissent suffisamment la place à de la nouveauté, pour pouvoir proposer régulièrement de nouveaux produits.
- Le modèle économique de ces entreprises est basé sur la **croissance**, ce qui implique **vendre toujours plus** chaque année.

Afin de pouvoir vendre de nouveaux produits, il leur faut :

- soit trouver de **nouveaux clients**, qui n'ont pas encore l'usage, en créant un besoin ou via la publicité.
- soit pousser les clients déjà équippés à **renouveler leur matériel**  
- soit encore pousser les consommateurs à **multiplier les appareils numériques** en leur possession, en créant des nouveaux usages ou en déplaçant un usage vers un matériel dédié.


Afin de pousser les consommateurs à renouveler ou multiplier leur matériel, plusieurs [stratagèmes privateurs]() sont mis en place au fil du temps et de l'évolution du marché : 

- **Rendre les applications plus lourdes** afin de les rendre plus lentes sur du matériel plus ancien. On constate sur le temps que les programmes utilisent plus de ressources pour rendre le même service ([loi de Wirth](https://fr.wikipedia.org/wiki/Loi_de_Wirth))
- **Favoriser l'Obsolescence logicielle** : Déclarer arbitrairement la fin d'un support logiciel sur des appareils anciens : ceci passe souvent par des incompatibilités et des messages intempestifs ou autres injonctions à mettre à jour votre appareil.
- **Assumer l'Obsolescence matérielle** accrue par l'utilisation de composants peu durables et non remplaçables : on rend difficile, voir impossible la possibilité d'ouvrir le matériel, remplacer des pièces en panne, les batteries usées, ou même encore commander des pièces de rechange.

Enfin, on note un glissement de l'usage avec les nouveaux logiciels et systèmes d'exploitation qui mènent à une dépossession numérique:

- Logiciels payants en **abonnement** : créent de la dépendance, car **s'arrêter de payer** c'est souvent **tout perdre**.

- Logiciels **gratuits** truffés de **publicité et trackers** : perturbent l'usage et sont conçus pour vous garder un maximum attentifs aux publicités.

- Logiciels "**dans le cloud**", requérant une connexion internet, et ne permettant pas de maitriser le stockage de ses propres données.

Ces trois caractéristiques sont même souvent entremêlées, rendant l'expérience utilisateur plus aliénante, et alimentent un **système techno-féodal**.

![broken display glass - Julia Joppien - Unsplash](/images/julia-joppien-XFUqd0u5U7w-unsplash-cropped-scaled.jpg)

## Obsolescence et Écologie

On entend souvent que l'impact écologique du numérique est lié à la consommation énergétique lors de l'uage de nos appareils et qu'il est préférable de racheter du matériel récent, car plus efficace énergétiquement. Si l'efficacité du matériel récent est vraie, le fait de changer de matériel pose, en lui même un problème écologique **majeur**.

 Les principaux problèmes écologiques des appareils numériques sont liés au **remplacement du matériel** :

- **Génération de déchets électrioniques (DEEE)** : la plupart sont conservés chez les gens (1), recyclés en déchetterie, ou encore même détruits. Toutefois, beaucoup de DEEE non fonctionnels finissent dans des décharges des pays sub-sahariens, où [des enfants sont employés à trier à la main des produits hautement toxiques.](https://reporterre.net/Au-Senegal-le-fructueux-business-des-dechets-electroniques-occidentaux)
- **Le remplacement par de nouveaux équipements** produits à partir de matières premières issues de la métallurgie. De surcroit, ces nouveaux équipements évoluent dans le temps en utilisant des procédés de plus en plus complexes, ce qui leur confère une durée de vie encore plus courte, et un réemploi très compliqué : Impossibilité à réparer, mettre à jour, ou à démonter pour en tirer des pièces de rechange. 

On peut considérer que chaque produit électronique qui finit inutilisé, ou jeté, ouvre la porte à la production d'un nouveau produit, qui aura une vie probablement plus courte et une fin de vie plus difficile que son prédécesseur. Un cycle interminable qui n'augure rien de bon à [notre ère de la raréfaction des ressources métalliques](https://www.youtube.com/watch?v=7bh3Z78e68Q) et [les impacts majeurs de la mine sur l'environnement](https://reporterre.net/Les-ravages-ignores-de-l-activite-miniere).

On comprend donc vite que **l'obsolescence programmée** des appareils pose un problème majeur de **consommation de resources métalliques**, et par extension de **pollution des sols**, raréfaction des **resources minières**, consommation d'eau et d'énergie pour produire, et acheminer.

### Et le recyclage dans tout ça?

En France, on a une bonne filière de recyclage de nos DEEE, avec l'ADEME qui [affiche un taux de 70%](https://librairie.ademe.fr/dechets-economie-circulaire/5191-equipements-electriques-et-electroniques-donnees-2020.html) de recyclage, toutefois à relativiser car il comprend bien plus que simplement le numérique. Il reste difficile de trouver des chiffres exacts correspondants aux déchéts du numérique. 

De plus, lors du recyclage des appareils numériques, seule une petite partie des composants peuvent être recyclés : on va se concentrer sur certains métaux "purs", faciles à extraire, le reste est "valorisé" (comprendre brûlé pour produire de l'énergie, et de la pollution), ou encore stocké (comme les écrans LCD) [en attente de recherche pour pouvoir le recycler](https://www.nature.com/articles/s41893-019-0320-4). 

Dans le monde, [seuls 17% des déchets électroniques sont recyclés](https://theroundup.org/global-e-waste-statistics/), la plupart finissent exportés illégalement dans des déchetteries en Afrique, où travaillent souvent des enfants qui sont exposés à la toxicité de ces déchets.

> Plus d'informations (en anglais) dans ce [rapport très complet de l'ONU](https://ewastemonitor.info/wp-content/uploads/2020/11/GEM_2020_def_july1_low.pdf).

Néanmoins, la filière du recyclage opère également sur la **remise en état** et le **reconditionnement** des appareils, ce qui présente **l'intérêt écologique principal** ; Les produits du recyclage n'étant pas ré-employés (ou très peu) dans la production de nouveaux appareils numériques (par les pays asiatiques).

## Performances, optimisation logicielle, et loi de moore

La [loi de moore](https://fr.wikipedia.org/wiki/Loi_de_Moore) considère que le matériel informatique double ses performances et sa complexité tous les 18 mois : alors que cette loi est relativement vérifiable pour le matériel haut de gamme, elle l'est moins pour le matériel bas de gamme, ce dernier faisant souvent appel à des technologies plus anciennes, très peu coûteuses à produire, mais également **rapidement obsolètes**.

Si l'on prend par exemple un matériel [bas de gamme acheté en 2022](https://www.laptopspirit.fr/324917/lenovo-ideapad-1-14igl7-82v6001cfr-pc-portable-pas-cher-compact-14-windows-11-fin-et-leger-avec-usb-c.html), on pourrait constater que ses performances sont équivalentes (voir parfois inférieures) à un [produit de milieu de gamme](https://www.notebookcheck.biz/Critique-complete-de-l-Ultrabook-Lenovo-ThinkPad-X250.141390.0.html), acheté entre 5 et 10 ans auparavant. De plus certains matériels bas de gamme (mais pas que) récents font l'économie de pouvoir mettre à jour le matériel (par exemple en changeant la RAM, ou le stockage).

La performance de l'appareil va alors dépendre des logiciels qui sont exécutés dessus, et de son [système d'exploitation](#). Au fur et à mesure du temps, on se rend compte que les développeurs (de manière générale) adaptent leurs technologies et leur optimisation (référence?) aux performances "courantes" du matériel informatique, sans prendre en compte le matériel plus ancien.

La tendance est donc à une consommation croissante des ressources, afin de réaliser la même tâche :

- Utilisation de frameworks coûteux en ressources, tels que [electron](#)
- Augmentation des [ressources utilisées pour l'affichage de pages web](https://mgearon.com/performance/the-web-is-bloated/) (notamment liées aux analytics, et aux contenus image et vidéo croissants)
- Ajout de [nombreux services d'arrière-plan](https://lecrabeinfo.net/thisiswin11-supprimer-les-bloatwares-de-windows-11.html) pas toujours utiles, et qui ralentissent l'ordinateur.

## Efficacité énergétique vs. Impact de la production

On parle souvent d'une amélioration de l'efficacité énergétique sur les produits électroniques récents, et c'est un argument irréfutable : les équipements numériques modernes bénéficient d'un progrès technique qui les rend plus efficaces en termes de calculs par Watt consommé.

Toutefois, ne tenir compte que de l'efficacité énergétique réduit le problème à sa seule utilisation : on ignore sciemment les enjeux énergétiques et de ressources liés à la fabrication, le packaging, le transport.  [L'ADEME considère](https://infos.ademe.fr/magazine-avril-2022/faits-et-chiffres/numerique-quel-impact-environnemental/) que l'impact de la fabrication des produits numériques est responsable à 78% de l'impact écologique, son usage étant réduit à 21% seulement.

Le rapport est vite fait: jeter un produit utilisable pour un produit équivalent, mais légérement plus efficace énergétiquement représente un gachis.

## L'Intimité Numérique face à la Collecte de données

Un des enjeux cachés du libre emploi, est le respect à la vie privée. Il est aujourd'hui devenu très difficile pour le grand public d'échapper à la captation permanente et omniprésente de données. La plupart du temps non-consentie, ou non-consciente, celle-ci conduit bien souvent à une impuissance, ou encore des biais tels que ["Je n'ai rien à cacher"](https://usbeketrica.com/fr/article/pourquoi-n-avoir-rien-a-cacher-n-est-pas-une-raison-pour-accepter-la-surveillance-de-masse)

Le manque d'informations à ce propos, et l'opacité dans laquelle est entretenue le modèle économique livre un public à toujours plus de publicité ciblée, employant des méthodes de manipulation, constituent [un réel danger d'aliénation](https://reflets.info/articles/transparence-numerique-et-totalitarisme-algorithmique-l-alienation-des-masses-en-question) pour les citoyens : ces données sont utilisées autant pour du profilage dans un but commercial, mais également [d'influence politique](https://www.lemonde.fr/pixels/article/2018/03/22/ce-qu-il-faut-savoir-sur-cambridge-analytica-la-societe-au-c-ur-du-scandale-facebook_5274804_4408996.html). 

De plus, l'arrivée de l'**Intelligence artificielle** marque un tournant décisif dans le travail des publicitaires pour forger des messages hyper-convaincants : l'utilisation des données captées permet de générer des argumentaires sur mesure d'après des profils très précis, [forgés sur les données collectées](https://www.youtube.com/watch?v=YKh0crS9pdQ) de nos habitudes, opinions, humeurs.

Enfin, le glissement sécuritaire -tous suspects, tous surveillés- auquel on assiste depuis une vingtaine d'années entretient un système avec de moins en moins d'alternatives : Il devient difficle de [préserver son intimité numérique](https://framablog.org/2018/10/23/intimite-numerique-le-truc-a-fait-mouche/) : ont été récemment [rendues suspectes](https://www.laquadrature.net/2023/06/05/affaire-du-8-decembre-le-chiffrement-des-communications-assimile-a-un-comportement-terroriste/) des pratiques (saines) de chiffrement et d'usage de logiciels respectueux de la vie privée, par le [contrôle administratif des réseaux sociaux](https://www.france24.com/fr/france/20230705-en-france-le-gouvernement-envisage-des-restrictions-des-r%C3%A9seaux-sociaux-en-cas-d-%C3%A9meutes), ainsi que la [criminalisation des luttes]() pour les droits humains et écologiques.  

## Dépossession Numérique

Quand on parle de dépossession numérique, on parle du fait qu'un produit crée une dépendance à un service payant, dont l'achat n'est pas pérenne dans le temps. 

Récemment, beaucoup de logiciels sont passés sur un modèle économique de license par abonnement (mensuel ou annuel). Alors qu'il y a encore une dizaine d'années, le modèle était à du *pay to own* (payer pour posséder), le modèle économique a glissé vers du *pay to use* (payer pour utiliser) : une utilisation temporaire où l'on se retrouve captif d'une application, car obligé de maintenir un paiement régulier pour pouvoir continuer à l'utiliser dans le temps. 

Cette dépossession numérique a pour conséquence que ce que nous considérons comme des achats, ne sont que des "licences d'utilisation d'un service". Or, nous savons que commencer à utiliser un produit, crée une dépendance : celle-ci augmente proportionellement avec le temps, l'argent, et la quantité de données qui y sont investies. 

### Dépendance aux produits "dans le cloud"

Un autre exemple de dépossession est la dépendance d'un produit "dans le cloud" à un abonnement, ou encore à un service dépendant du constructeur. Prenons l'exemple d'un *smart object* : un lecteur de musique "dans le cloud", basé sur un abonnement ou sur l'"achat" de produits numériques, exclusivement utilisables sur cet appareil. 

Qu'en est il lorsque l'abonnement s'arrête ? Si le constructeur décide arbitrairement de fermer votre compte? Si la société qui opère le service peut plus exercer son service dans votre pays pour des raisons légales? ou bien encore fait faillite ? 

Nous nous retrouvons alors avec un appareil qui ne sert plus à rien, car il est déconnecté de son service en ligne essentiel à son fonctionnement : c'est une des formes les plus déplorables de déchets électroniques. De par leur matériel et logiciel privateur, le cycle est entièrement rompu. Une des dernières solutions pour redonner à cet objet un dernier usage serait d'utiliser de la [rétro ingénierie (retroengineering en anglais)](https://fr.wikipedia.org/wiki/R%C3%A9tro-ing%C3%A9nierie) afin de modifier le [micrologiciel (firmware)](https://fr.wikipedia.org/wiki/Firmware) de l'appareil désormais inutile et le remplacer par une alternative libre.

## Réemploi

Aux yeux du public, les enjeux liés à nos usages numériques englobent généralement deux grandes catégories : les coûts énergétiques liés à l'usage, et les déchets électroniques. 

Un point capital concernant les déchets électroniques est leur cycle de vie. On pense souvent au recyclage, qui est un bon moyen de ne pas gaspiller de ressources minérales (métaux, terres rares), et de les remettre en circuit dans de nouveaux produits. Pourtant, une question capitale reste en suspens : que considère t'on comme un déchet électronique?

On retrouve souvent dans cette catégorie des produits endommagés, défecteux, et irréparables, mais également des produits difficiles à réparer, devenus obsolètes car non supportés, ou encore désuets mais encore fonctionnels.

Ces dernières catégories doivent nous faire prendre conscience de l'emploi de nos appareils numériques : remplacer un appareil en fonctionnement, qui peut être réparé, ou mis à jour, par un neuf, va produire deux choses:

- La génération d'un déchet électronique

- La production d'un nouveau matériel

Le meilleur produit est généralement celui de nous n'avons pas besoin d'acheter pour  en remplacer un plus ancien. 

## Recyclage : le dernier recours

L'action de recycler devrait être l'acte ultime d'accompagenement de nos fidèles appareils électroniques, après la réparation et le réemploi. Pour une consommation électronique responsable, il devrait intervenir au bout du bout de la chaîne de la vie de l'appareil. Des entreprises en france comme [ecosystem](https://www.ecosystem.eco/) organisent sous contrat d'état :

- la collecte des DEEE
- l'organisation d'opérations telles que [je donne mon téléphone](https://www.jedonnemontelephone.fr/) en association avec [emmaüs]() et [les ateliers du bocage](https://ateliers-du-bocage.fr/)

Une fois collectés, les déchets électroniques sont testés, désassemblés et ont généralement deux destins bien distincts:

- Les équipements en état de marche, ou les pièces utiles sont réemployées dans des opérations de réparation, réassemblage en vue d'un réemploi. De nouveaux appareils sont assemblés, reconditionnés et remis sur le marché à des prix plus attractifs.
- Les équipements inutilisables (défectueux, cassés, irréparables) sont démantelés autant que possible et leurs composants triés en vue du recyclage des métaux qui les composent.

Toutefois, le recyclage des appareils électroniques ne représente pas un 100% de la réutilisation de leurs métaux. En raison de la [très grande complexité métallique](https://www.systext.org/sites/all/animationreveal/mtxsmp/#/) de ceux-ci, on considère que seules les pièces en métaux purs peuvent être recyclées.



## Lectures:

https://www.laquadrature.net/2023/06/15/tribune-attaches-aux-libertes-fondamentales-dans-lespace-numerique-nous-defendons-le-droit-au-chiffrement-de-nos-communications/

