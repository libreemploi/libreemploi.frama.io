---
template: blog.html
---

![Transcript - Jancovici à propos des impacts du numérique (conférence chez Orange)](blog/Transcript - Jancovici à propos des impacts du numérique (conférence chez Orange)/../images/header-jancovici-860x200.jpg)

# [Transcript - Jancovici à propos des impacts du numérique (conférence chez Orange)](2023-10-08-transcript-jancovici-a-propos-des-impacts-du-numerique)
écrit par _Thomas Iché_ le 2023-10-08


> Transcript adapté et commenté de la vidéo youtube : [Jancovici démonte les avantages du numérique chez Orange](https://www.youtube.com/watch?v=njtez3Md0bs) de la chaine [Ethique et Tac](https://www.youtube.com/@ethiqueettac) du 2023-10-08 (abonnez vous, elle est top!)

**Sources et Ressources:**

- Plan de transformation de l'économie française : https://ilnousfautunplan.fr/le-plan/
- Travail du shift sur le numérique : https://theshiftproject.org/category/thematiques/numerique/
- Atelier de sensibilisation - La fresque du numérique: https://www.fresquedunumerique.org/

> **Ce n'est pas tous les jours qu'on a des vidéos sur youtube qui collent parfaitement aux enjeux du libre emploi, et ce soir était un soir un peu particulier. J'aime bien Jean-Marci Jancovici pour son franc-parler et c'est un de ceux qui a réussi à me faire mettre un pied à l'étrier sur toutes ces problématiques. **
>
> **Après, il faut dire que le bougre sait s'y prendre pour parler à des capitalistes, même si bon, passé son côté taquin, je crains fort qu'il ne soit rien de plus qu'un token. Au final, son intervention ici chez Orange, à propos du numérique résume bien la panade dans laquelle on est et les efforts à fournir. Il présente, au delà des enjeux du CO2 et l'énergie (son domaine de prédilection), les enjeux de ressources métalliques liées à la fabrication et au recyclage.**
>
> **Son interlocutrice, laisse également soupirer le conflit d'intérêt RSE*, tout bonnement insoluble, auquel est confronté l'entreprise.**

Note: j'ai raccourci le transcript pour me focaliser sur les réponses de Jean-Marc Jancovici et moins sur son interaction.



[lire en entier](2023-10-08-transcript-jancovici-a-propos-des-impacts-du-numerique)

---

![Nos produits électroniques ont changé en 20 ans - Les systèmes pour gérer les déchets électoniques n'arrivent plus à suivre](blog/Nos produits électroniques ont changé en 20 ans - Les systèmes pour gérer les déchets électoniques n'arrivent plus à suivre/../images/header-blog-default-2.jpg)

# [Nos produits électroniques ont changé en 20 ans - Les systèmes pour gérer les déchets électoniques n'arrivent plus à suivre](2023-06-17-nos-produits-electroniques-ont-change-en-20-ans)
écrit par _Thomas Iché_ le 2023-06-17


> Traduction de l'article : [Consumer electronics have changed a lot in 20 years – systems for managing e-waste aren’t keeping up](https://theconversation.com/consumer-electronics-have-changed-a-lot-in-20-years-systems-for-managing-e-waste-arent-keeping-up-147972) sur [The Conversation](https://theconversation.com/) du 2021-01-11

Auteurs originaux : 

- Callie Babbitt - Associate Professor of Sustainability, Rochester Institute of Technology
- Shahana Althaf - Postdoctoral associate, Yale University

(Sources et liens en anglais, non traduits)



[lire en entier](2023-06-17-nos-produits-electroniques-ont-change-en-20-ans)

---

![Premier article de blog!](blog/Premier article de blog!/../images/header-blog-default-1.jpg)

# [Premier article de blog!](2023-06-15-bienvenue-sur-le-blog)
écrit par _Thomas Iché_ le 2023-06-15


Bienvenue à tous! 

Ceci est le premier post du blog du libre emploi! Ce blog recensera les actualités du site. Nous publierons des articles régulièrement sur ce blog pour vous présenter les nouveaux projets, outils et expérimentations.



[lire en entier](2023-06-15-bienvenue-sur-le-blog)

---
