# Remettre en état un Ordinateur



Remettre en état un ordinateur est une tâche *relativement facile* pour peu qu'on s'intéresse à ce qui pose problème, et *trouver des solutions* pour refaire une jeunesse notre machine.

On pense souvent qu'un ordinateur trop lent a fait sa vie et qu'il est temps d'en changer. Mais généralement, il s'agit souvent d'un problème logiciel, ou d'un composant que l'on peut mettre à jour.

![Remettre en état un ordinateur](/images/lukasz-adam-monitor-illustration-2.svg)

Racheter une machine complète est généralement **contre-productif**, surtout si votre ordinateur a **moins de 10 ans** (en moyenne). Avant de changer de machine, il peut être important de considérer d'essayer de lui refaire une jeunesse :

- Pour **le portefeuille** : on considère qu'à performance ressentie égale, le prix d'une machine a tendance à augementer notamment en raison de la [Loi de Wirth](https://fr.wikipedia.org/wiki/Loi_de_Wirth)
- Pour **la planète** : jeter ou mettre au rebut un ordinateur a des conséquences. Produire un nouvel ordinateur a également d'autres conséquences. Sachant que [75% de l'empreinte écologique](https://librairie.ademe.fr/cadic/6836/transcription-infographie-impacts-environnementaux-numerique.pdf) d'un ordinateur réside dans sa fabrication.
- Pour **rester libre** : apprendre à réparer, mettre à jour, et maitriser son propre matériel permet d'être moins dépendant économiquement des géants du numérique et de garder la main sur nos libertés. Le matériel devenant de plus en plus fermé, opaque et privateur de liberté, nos bons vieux appareils sont souvents plus pratiques à maintenir que les nouveautés pleines d'obsolescence programmée.

---

## Pourquoi mon ordinateur est lent?

Un ordinateur peut être devenu très lent pour tout un tas de raisons : ce sont des appareils qu'on utilise tous les jours, et à force de les utiliser, on les remplit de données, d'applications, de mises à jour, et ils finissent petit à petit par devenir très lents.



Si on devait comparer avec un autre objet, on pourrait dire qu'il devient "sale" plutôt que "usé". On peut noter 2 grandes causes à cette "saleté" qui rend nos machines lentes:

- **L'emcombrement logiciel** : des logiciels toujours plus lents et adaptés aux performances croissantes du nouveau matériel.
- Dans une moindre mesure, **L'usure du matériel** peut être facilement évitée avec un peu de connaissances et d'envie de faire la poussière.

### Encombrement Logiciel

Un des principaux problèmes des logiciels et des apps sur un ordinateur c'est qu'ils ont une facheuse tendance à devenir plus lourds, et plus lents à mesure que le temps avance. 

Les développeurs ont souvent cette facheuse tendance à programmer leurs applications pour du matériel récent, sans tenir compte des utilisateurs du matériel ancien. Non pas qu'ils soient machiavéliques, leur travail consiste souvent à utiliser des librairies de programmation qui sont mises à disposition par des géants du numérique et qui intègrent plus la facilité de programmation que l'optimisation du code logiciel.

On en arrive donc à des applications avec des interfaces hyper stylées, mais à taille gargantuesque. Ou même à des sites web qui demandent de plus en plus de mémoire, et de bande passante, des Vidéos Ultra HD par défaut qui se jouent toutes seules.

#### La vitesse de la mémoire RAM et la mémoire de Stockage

Les principaux composants qui sont impactés par cette obésité logicielle sont la Mémoire RAM, et la mémoire de stockage :

- La RAM se retrouve vite saturée : si vous aviez 4 Giga-Octets sur votre ordinateur, désormais, lorsque vous ouvrez trop de logiciels trop gros (ou de pages web trop grosses), votre ordinateur va déplacer des bouts de cette mémoire sur votre mémoire de stockage, et plus votre disque est lent, plus le processus va ralentir votre ordinateur.
- Lorsque vous ouvrez une application, elle se copie toujours dans la mémoire RAM depuis le stockage afin de pouvoir s'exécuter très rapidemment. Toutefois la mémoire de stocakge peut être lente, voir très lente, et un programme qui grossit au fil du temps, prendra toujours plus de temps à s'ouvrir.

#### La vitesse du Processeur et du processeur graphique

Dans une moindre mesure, le processeur de l'ordinateur est sollicité afin de réaliser des calculs. Depuis 2010, on considère que les processeurs d'ordinateur n'ont pas tant évolué sur leur vélocité. 

En revanche, si ces processeurs n'ont pas augmenté leur vitesse, ils se sont parallélisés : au sein d'une même puce, on a vu le nombre de coeurs se multiplier, permettant à différentes applications de tourner en parallèle. 

Pire, on a vu depuis une dizaine d'années, une explosion des services s'exécutant en arrière plan : gestionnaires de mise à jour, applications en sommeil, gestionaires de télémétrie, anti-spywares. La liste est longue, mais tous ces programmes "invisibles" pèsent sur votre ordinateur sans s'en rendre compte.

Bien heureusement, il est possible de retirer ces programmes du démarrage de notre ordinateur si nous n'en avons pas besoin.

### L'usure Matérielle

L'usure matérielle d'un ordinateur est moins présente, car les composants électroniques ne sont pas soumis aux mêmes aléas du temps que les composants mécaniques. Toutefois, on peut compter parmi les pièces pouvant s'user au fil du temps:

- **L'encrassement** et **l'empoussièrement** des **ventilateurs** et radiateurs : indispensables composants permettant à nos machines de dissiper la chaleur dégagée par les processeurs, la mémoire RAM, les disques, ou les cartes graphiques, ils ont une facheuse tendance à attirer la poussière et s'encrasser. Afin de les nettoyer, des précautions sont de mise, car utiliser un aspirateur ou une bombe d'air sec sur un ventilateur ne se fait pas sans un minimum de connaissances pratiques.
- **L'usure naturelle des batteries** d'ordinateurs portables est également une des causes principales de l'usure matérielle des ordinateurs. Et si certains ordinateurs ont une batterie amovible (donc remplaçable), d'autres sont plus retors afin de procéder au remplacement de la batterie cachée à l'intérieur de l'ordinateur: Dans ces cas là, il est très important de prendre des précautions pour changer la batterie sans risque de feu. 
- **La déterioration des disques durs mécaniques :** ces disques, utilisés globalement jusqu'au milieu des années 2010 ont pour particularité d'avoir une de série de plateaux de disque tournant à plusieurs milliers de tours par minute, sur lesquels vient lire et écrire une aiguille magnétique très fine. Ils sont extrêmements sensibles aux chocs, aux vibrations, au magnétisme ainsi qu'à la chaleur. Leur durée de vie est très aléatoire en fonction des marques, et des conditions d'utilisation. Leur principal problème est que leur défaillance est généralement fatale. Parfois, les conditions d'utilisation se dégradent rapidement, mais passé un certain point de déterioration, il devient impossible de récupérer les données.
- **L'usure mécanique des touches de clavier, boutons de souris** est également un facteur de déterioration très frustrante de l'expérience de l'ordinateur. Bien heureusement, même les claviers d'ordinateurs portables sont interchangeables.

### Les chocs et accidents

Il arrive parfois qu'un ordinateur tombe ou reçoive un coup violent, le rendant inopérant partiellement ou totalement hors service. Ces cas bien malheureux arrivent et il devient alors très hasardeux d'arriver à réparer un ordinateur. Les chocs peuvent être physiques (votre portable qui tombe de la table), électriques (la foudre tombe tout près et crée une surtension), ou encore de plein d'autres ordres (eau renversée). Dans tous ces cas là, la prudence est de mise afin d'essayer de sauver les meubles.

Parfois, on ne sauve qu'une partie de l'ordinateur, et il nous faudra procéder au remplacement des parties hors service.

---

## Diagnostiquer les problèmes

Afin de diagnostiquer les problèmes on va établir une liste des choses qui marchent, celles qui ne marchent que partiellement (ou sont lentes), et celles qui ne marchent pas du tout. Une bonne manière de procéder est d'établir une petite checklist comme ci-dessous.

| Item                                                         | Fonctionne Bien | Lenteurs | Hors Service |
| :----------------------------------------------------------- | :-------------: | :------: | :----------: |
| Démarrage de l'ordinateur en moins d'1 minute                |                 |    x     |              |
| Démarrage de la session utilisateur                          |                 |    x     |              |
| Ecran/Affichage                                              |        x        |          |              |
| Son                                                          |                 |          |      x       |
| Webcam                                                       |        x        |          |              |
| Clavier                                                      |                 |          |              |
| Souris/Touchpad                                              |                 |          |              |
| Démarrage d'un navigateur                                    |                 |    x     |              |
| Démarrage d'applications                                     |                 |    x     |              |
| Lecture Audio (spotify)                                      |        x        |          |              |
| Lecture Vidéo (youtube)                                      |        x        |          |              |
| Ouverture de documents textes                                |        x        |          |              |
| Travail avec plusieurs applications ouvertes (navigateur avec plusieurs tabs, office, etc.) |                 |    x     |              |

En regardant item par item on peut commencer à déceler des problèmes récurrents. En l'occurence sur notre checklist ci-dessus, on peut noter les problèmes suivants :

- Notre ordinateur est lent à démarrer, et à démarrer des programmes
- Le son ne marche pas, il faudra regarder et investiguer
- Après un moment d'utilisation, l'ordinateur devient très lent

---

## Nettoyage logiciel de l'ordinateur

La première chose à effectuer, avant de se lancer dans des changements matériels, est de commencer par décrasser un peu les données et les applications présentes sur l'ordinateur. Afin de réaliser ce nettoyage nous allons nous intéresser aux choses suivantes :

Quels programmes :

- sont lancés au démarrage
- sont installés

Quelle est la quantité de mémoire RAM :

- Disponible sur ma machine
- Utilisée après le démarrage de la machine
- Utilisée en démarrant mes applications les plus courantes

Quel est l'espace disque disponible dans la mémoire de stockage

- Taille totale
- Système d'exploitation
- Programmes et Apps
- Documents personnels´

![](/images/lukasz-adam-cat-desk.svg)

### Un régime pour la mémoire RAM ?

La mémoire RAM est la mémoire qui accueille les applications lorsqu'elles sont le principal problème, **lancées** et en cours d'exécution, lorsqu'on ouvre un document dans une application (texte, image, video, ou page web), le document est également envoyé dans la mémoire RAM pour pouvoir être modifié, et ensuite sauvegardées sur la mémoire de stockage. 

- Elle accueille également le **système d'exploitation** et tous ses **services** qui permettent de faire fonctionner l'ordinateur (son, réseau, imprimantes, etc).

- La mémoire RAM se remplit et se vide en fonction de l'usage. Fermez une application et la mémoire RAM qu'elle occupait sera libérée.

Les problèmes arrivent généralement **lorsque la mémoire RAM est saturée** : si toute la mémoire est occupée et l'on veut ouvrir une nouvelle application, le système d'exploitation va essayer de faire du ménage et déplacer la mémoire RAM d'une application non utilisée (mais toujours ouverte) sur le disque de stockage, puis libérer la place en RAM pour ouvrir la nouvelle application : on appelle cette opération le **Swap** (remplacement). Or, déplacer de la mémoire vers le disque de stockage, plus lent, prend du temps, et rend l'ordinateur plus lent pendant l'opération. Pire, si l'on veut passer d'une application à une autre lorsque la mémoire est saturée, l'ordinateur va **swapper** la mémoire à plusieurs reprises : c'est lent, énervant et c'est là **une des principales sources de frustration de l'usage d'un ordinateur**.

> **IMPORTANT** : Afin d'éviter le swap intempestif, il est nécessaire d'avoir un système avec suffisamment de mémoire disponible pour pouvoir accueillir un nombre raisonnable d'applications en parallèle.

**Diagnostic :** Pour déterminer la mémoire disponible et nécessaire à une utilisation *confortable*, on va mesurer la mémoire RAM à plusieurs moments afin de déterminer la charge mémoire totale au fil du temps:

- La mémoire nécessaire au système d'exploitation (assez difficile à réduire)
- La mémoire utilisée par les **programmes au démarrage de l'ordinateur**, généralement ceux lancés après l'écran de connexion
- La mémoire utilisée par les **applications courantes**

**Quelques Solutions :**

- La première des solutions est de **retirer du démarrage**, tout programme lancé automatiquement qui n'empêche pas le fonctionnement de l'ordinateur.

- **Certaines applications** (comme telegram, discord, spotify) peuvent être r**emplacées par des versions web** (tournant dans le navigateur web) et consommant moins de ressources RAM. En général on considère que chacune de ces apps embarque avec elle le code d'un navigateur web, ce qui peut peser lourd dans la balance face à des versions qui tournent dans un navigateur web unique.

- **Fermer les onglets/fenêtres/apps que nous n'utilisons pas**. Attention cependant, certaines applications ne se ferment pas réellement lorsque nous fermons leur fenêtre, et il faut les quitter manuellement via leur icônes dans la barre des tâches.

- **Utiliser des logiciels libres** au lieu de logiciels propriétaires, préférer des logiciels simples pour des **usages simples**.

<u>Si toutefois l'équation et notre régime était insoluble : il nous faudra penser à d'autres solutions:</u>

- Utiliser un **système d'exploitation** moins gourmand en ressources mémoire, comme **Linux** : certaines distributions sont bien plus légères que d'autres.
- **Ajouter une barrette de RAM** ou **remplacer** la/les barrettes existantes par des modèles de plus grande capacité.

### Faire de la place sur le Stockage SSD

Une autre source de lenteur des PC est liée à la saturation de la mémoire de stockage sur les anciens disques SSD : pour des [raisons liées à la conception des SSD](https://pureinfotech.com/why-solid-state-drive-ssd-performance-slows-down/), on considère que pour fonctionner à sa pleine performance, un disque SSD ne devrait pas dépasser une utilisation de sa capacité de 70%.

### Défragmenter les vieux Disques Durs mécaniques

Les disques durs mécaniques, en plus d'être fragiles et lents, sont sujets à un problème qu'eux seuls connaissaient : la fragmentation. A force d'être utilisés, des fichiers sont créés, et supprimés sur le disque, chaque fichier a une position physique sur le plateau magnétique, toutefois, a force de créer et d'en supprimer, des espaces minuscules se créent et un fichier qui prenait une place conséquente va devoir trouver de la place ailleurs, jusqu'à même devoir être fragmenté et positionné dans plusieurs trous disponibles. 

Ce phénomène est à l'origine de lenteurs extrêmement désagréables pour des disques qui sont généralement déjà très lents. C'est pourquoi il recommandé d'effectuer de la défragmentation des disques durs mécaniques de manière régulière, afin de les maintenir en ordre et éviter la fragmentation. Ces opérations peuvent être réalisées à l'aide de logiciels comme [myDefrag](http://www.mydefrag.fr/)

---

## Mise à jour des composants matériels

Une fois testé après un ménage logiciel, on peut maintenant penser à mettre à jour certains composants de notre ordinateur.

En fonction du matériel que nous utilisons, certains composants peuvent être remplacés par des pièces plus modernes, sans avoir à remplacer l'intégralité de l'ordinateur. Au niveau pécunier tant que écologique, c'est une très bonne solution, d'autant plus que certains composants que nous remplaçons pourront être réutilisés et réemployés à bon escient.

![](/images/lukasz-adam-settings-illustration.svg)

### Quelles pièces remplacer ou mettre à jour?

Que l'on utilise un PC fixe ou un portable, les pièces remplaçables ne sont pas les mêmes, et il arrive même que certaines machines ne puissent pas être modifiées du tout !

Les PC Fixes sont souvent les plus flexibles car ils sont l'[assemblage de composants](/guides-pc/dans-mon-ordinateur.ms) individuels qui peuvent être remplacés. Sous certaines conditions cependant, car ils emploient des normes et des standards avancés qui nécessitent parfois de changer plus d'une pièce.

Les PC portables, eux, sont souvent composés d'une seule carte mère, sur laquelle sont connectés:

- Une batterie (interne ou externe)
- La mémoire interne (Disque SSD SATA ou NVME, ou disque dur SATA)
- Un ou plusieurs emplacements pour mémoire RAM, occupés par une ou plusieurs barrettes de RAM.

La plupart du temps, la mise à jour d'un portable (et même d'un PC Fixe) consiste en :

- Remplacer le disque de stockage par un plus gros / plus rapide
- Remplacer la mémoire RAM ou ajouter une barrette de RAM si un emplacement est disponible

### Remplacer le disque dur par un SSD

Le cas le plus courant d'obsolescence dans un ordinateur moderne est la lenteur du stockage externe si celui-ci est un disque dur mécanique. Les disques mécaniques ont été généralement utilisés comme stockage principal des ordinateurs jusqu'au début des années 2010, cependant leurs vitesses de lecture, d'écriture et temps d'accès aux fichiers sont bien plus lents que les disques à mémoire flash (aussi appelés SSD, ou solid state drive), qui remplacent désormais ces disques mécaniques. De plus, les ordinateurs récents ont désormais un nouveau format de SSD, encore plus véloce, qui permet d'atteindre des vitesses encore plus rapides. Toutefois, l'usage de SSD simples est généralement très satisfaisant dans la mise à jour des composants d'un ordinateur

En termes de vitesse, ce tableau résume les ordres de grandeur des vitesses et les gains pour chacun des types de disques

| Nom                   | Temps de démarrage (Windows 10) | Vitesse de Copie (Gros fichiers) | Vitesse de Copie (Petits fichiers) | Temps d'accès |
| --------------------- | ------------------------------- | -------------------------------- | ---------------------------------- | ------------- |
| Disque dur HDD (SATA) | 54s                             | 100MB/s                          | 1MB/s                              | 0.1ms         |
| SSD (SATA)            | 11s (4x)                        | 500 MB/s (5x)                    | 40MB/s (40x)                       | 0.03ms (3x)   |
| SSD (NVME)            | 11s (4x)                        | 3000 MB/s (30x)                  | 100MB/s (100x)                     | 0.02ms (5x)   |

On voit en général que le gain principal se fait lors du changement du Disque Dur vers un SSD SATA, c'est le cas le plus courant et il permet d'améliorer grandement :

- La vitesse de démarrage de l'ordinateur
- La vitesse de démarrage des applications
- La copie de fichiers

Si l'ordinateur a un port NVME inutilisé (ce qui est improbable mais peut arriver), considérer utiliser un disque NVME peut être un bon investissement sur le temps, bien que plus onéreux.

> **TRÈS IMPORTANT : ** il arrive que certains ordinateurs portables soient équipés, ni de disque dur, ni de SSD, mais utilisent une mémoire interne (souvent appelée eMMC). Cette mémoire, bien plus lente qu'un SSD et un peu plus rapide qu'un disque dur, a la facheuse tendance à être la plupart du temps soudée sur la carte mère, ce qui rend impossible sa mise à jour.
>
> De [tels appareils](https://www.amazon.fr/Lenovo-IdeaPad-14IGL7-Ordinateur-Portable/dp/B0B88B3PPW) (souvent bas de gamme) sont à considérer comme des parangons d'obsolescence programmée, et dont l'achat est à proscrire : il est bien plus intéressant d'acheter un ordinateur portable, même vieux de 5 ans sur [leboncoin.fr](https://leboncoin.fr)

#### Que faire d'un Disque dur après l'avoir remplacé?

La plupart du temps, une fois le disque dur remplacé par un SSD, on se retrouve avec un disque sur les bras qu'on imagine souvent mettre au rebut. Lorsqu'on parle de jeter un disque dur, les gens ont cette image d'épinal de devoir le *détruire à coups de masse et de le jeter*.

**<center>!!! NE FAITES SURTOUT PAS ÇA !!!</center>**

Les disques durs, aussi lents qu'ils soient, sont **toujours très utiles**, seulement ils peuvent servir à tout autre chose qu'à être utilisés en permanence. Les disques durs sont fait de métal précieux et un disque en état de marche que l'on détruit c'est "perdu pour perdu".

<u>Quelques idées de réemploi des disques durs :</u> 

- Achetez un [boitier SATA/USB](https://www.ldlc.com/informatique/pieces-informatique/boitier-disque-dur/c4676/) et vous pouvez transformer votre ancien disque dur en disque dur externe que vous pouvez utiliser pour du stockage supplémentaire. 

- Mieux encore, vous pouvez ré-employer ce disque **en le transformant en archive de sauvegarde** pour l'excellent [Pika Backup](https://apps.gnome.org/PikaBackup/) si vous êtes sous Linux, ou encore [Duplicati](https://www.duplicati.com/) si vous êtes sous Windows.

Dans tous les cas, transformer un disque qui tournait en permanence en un disque utilisé ponctuellement permettra de **prolonger sa durée de vie** et de lui conférer un usage **plus adapté à ses performances** et son âge avancé. 

### Augmenter la mémoire RAM de l'ordinateur

Lorsque l'on se retrouve bloqués avec un ordinateur trop lent lorsque trop d'applications tournent en même temps, l'augmentation de la mémoire RAM est souvent une bonne idée. 

La première chose est de s'assurer que la mémoire de notre ordinateur peut être augmentée. Si nous avons un PC fixe, la réponse est très souvent oui (sauf si le constructeur s'est amusé à souder la mémoire sur la carte mère). Dans le cas d'un ordinateur portable ou d'un Mini-PC, c'est plus compliqué, il nous faut déterminer s'il est possible d'étendre la mémoire, et comment réaliser l'augmentation de mémoire :

- Soit en **remplaçant** la barrette actuelle de mémoire par une barrette équivalente de plus grande capacité, si l'ordinateur n'est équipé que d'un seul **slot** de mémoire
- Soit en **ajoutant** une barrette supplémentaire, équivalente, si l'ordinateur est équipé d'un slot libre.
- Soit en **remplaçant et ajoutant** de la mémoire équivalente.

le principal problème, 

**Équivalence :** pour sa compatibilité, la mémoire RAM est catégorisée par plusieurs facteurs : 

- La **taille** et la **forme** (aussi appelé **form-factor**) : les barrettes de mémoire PC de bureau et celles pour les ordinateurs portables / miniPC n'ont pas la même taille. On trouve [deux grandes catégories](https://itigic.com/fr/dimm-vs-so-dimm-characteristics-definition-and-differences/) : DIMM pour les ordinateurs fixes et SO-DIMM pour les portables.

- Le **type de mémoire** : c'est un peu la génération de mémoire qui dépend de l'âge de l'ordinateur. En fonction de celui ci, il faudra opter pour de la SDR, DDR, DDR2, DDR3 ou DDR4. 

- La **fréquence de fonctionnement** : celle ci est généralement déterminée par le processeur et doit être scrupuleusement identique sur toutes les barrettes. Si l'une d'entre elles est plus lente, alors les autres s'adapteront à la vitesse la plus lente. Afin de déterminer la vitesse actuelle de la mémoire (ainsi que son type), il existe sous windows l'outil [CPU-Z](https://www.cpuid.com/) et sous Linux la commande `sudo lshw -c memory`

**Connaitre les limitations de son PC :** chaque PC (carte mère) possède une limite de mémoire acceptée par le système. Si cette limite est dépassée, certains PC n'utiliseront que la capacité qu'ils savent gérer, tandis que d'autres refuseront la barrette mémoire ou ne démarreront pas du tout.



**Où se procurer de la Mémoire RAM ?**  Après avoir identifié quel type de RAM doit être utilisé, le principal problème est de savoir si ce modèle est encore en vente. Si le modèle est encore un peu récent, Il est souvent possible d'en acquérir sur les sites marchands (materiel.net, LDLC, amazon). 

Autrement il va falloir se tourner vers les sites de vente **d'occasion**, tels que eBay ou leboncoin pour espérer trouver notre précieuse barrette de mémoire.
