# Choisir un Navigateur Web

> **Aujourd'hui l'offre des navigateurs web est pléthore et beaucoup mettent en avant le fait d'être installés par défaut sur les ordinateurs ou bien téléphones. Toutefois, la solution par défaut n'est pas toujours la meilleure en terme de bilan écologique, de performance et de durabilité pour votre ordinateur/smartphone. Également, le respect de la vie privée et les enjeux sociétaux peuvent être des critères déterminants dans nos choix.**
>
> Il est important de bien choisir son navigateur web, car nous passons beaucoup de temps dessus et il est important d'être au courant de leurs caractéristiques, tant au niveau de leurs performances, que des risques liés à votre vie privée auxquels ils vous exposent. Cet article présente les principaux navigateurs, leurs points faibles et leurs points forts, ainsi que des questions que l'on pourrait se poser quant au choix d'un navigateur.

![](../images/web-browsers.jpg)

## Vue d'ensemble

On classe les navigateurs généralement en deux catégories : ceux basés sur **Chromium**, la base open-source du navigateur **Google Chrome** et ceux basés sur d'autres architectures, tels que **Firefox**, **Safari**, ou **Opera**.

Aujourd'hui, **Google Chrome** possède plus de 65% de part de marché, suivi par **Safari** (17%) et **Microsoft Edge** (6%). Ces trois navigateurs profitent d'une situation de monopole de fait respectivement sur **Android**, **IOS/macOS** et **Windows** car ils sont souvent installés par défaut. De plus **Google Chrome** profite de l'hégémonie de ses services pour [dénigrer la concurrence](), [abuse de sa position dominate](https://cms.law/fr/fra/publication/google-une-nouvelle-fois-condamne-a-une-amende-en-milliards-d-euros) pour forcer l'installation de chrome sur les machines des constructeurs, et [implémente des contrefeux](https://www.tomshardware.com/news/youtube-responds-to-delayed-loading-in-rival-browser-complaints) afin de ralentir les navigateurs concurrents.

### Problème hégémonique et Chromium

Le principal problème lié aux navigateurs basés sur le projet Chromium est qu'ils sont **dépendants de ce projet**, certes open-source, mais à la gouvernance liée à Google. Car, oui, Chromium est un navigateur **open-source** certes, mais celà ne veut en aucun cas dire que sa gouvernance est libre et démocratique. 

**Actualité :** Le problème principal est lié aux choix technologiques et à l'orientation philosophique de chrome, réside dans ses choix d'évolution, et notamment son système d'extensions. A court terme, Google [va mettre en place des limitations](https://www.forbes.com/sites/kateoflahertyuk/2022/10/08/google-chrome-users-this-is-how-long-you-have-to-switch-to-firefox/) permettant d'interdire ou de limiter l'action de certaines extensions, sous couvert de sécurité. Certes louable dans l'intention, ceci a pour objectif de limiter, voir rendre obsolètes, les bloqueurs de publicité, qui sont actuellement dans le viseur de la firme car elles porteraient atteinte à ses objectifs financiers.

Le principal problème est que ce changement, va également se répercuter sur tous les autres navigateurs basés sur chromium : **Brave**, **Vivaldi**, ou encore **Microsoft Edge** devront également utiliser ce nouveau système d'extensions qui devient *de facto*, plus dépendant de Google.

### Maintenir une concurrence saine

Pour ces raisons, il est sain de maintenir une concurrence entre les navigateurs, car elle seule permet de prévenir tout abus de position dominante. Aujourd'hui, **Firefox** est en perte de vitesse du fait de la politique aggressive de Google envers les navigateurs basés sur autre chose que Chromium. Et il est capital que celui-ci ne disparaisse pas, car il représente, (également avec WebKit, la base de **Safari** et **GNOME Web**) l'alternative pour résister à un monopole qui est de plus en plus pressant, et qui menace déjà les libertés, notamment celle du choix de refuser la publicté et le ciblage.

## Fonctionnalités en Trompe-l'oeil

Lorsqu'on est à la recherche d'un navigateur web, on est souvent enclin à choisir un navigateur rapide, sûr et si possible sans publicité. C'est d'ailleurs ce que mettent en avant tous les navigateurs : Qui souhaiterait le contraire?

Pourtant, certains navigateurs proposent des fonctionnalités qui les mettent en avant par rapport à leurs concurrents. Voyons ensemble rapidement ce qui se cache derrière.

### Affichage Instantané des pages (pre-fetching et pre-rendering)

Mis en avant par Google Chrome, Microsoft Edge et Brave, [l'affichage instantané](https://en.wikipedia.org/wiki/Link_prefetching) des pages met en place des mécanismes de probabilité de cliquer sur des liens, afin de charger ces pages avant même d'avoir cliqué dessus. Cela a pour effet de mettre en place des systèmes de suveillance et de profilage afin de déterminer, via votre comportement, la meilleure probabilité d'ouvrir une page. De plus, ce système, aussi performant qu'il vous soit présenté, alimente un système qui va consommer du trafic réseau inutilement, au cas où vous cliqueriez sur un lien, en chargeant la page en avance, même si vous n'y accédez pas.

### Bloqueurs de Publicité et conflits d'intérêt

Les bloqueurs de publicité sont devenus depuis une vingtaine d'année des alliés redoutables afin de conserver un semblant de navigation calme et à l'abri de sollicitations intempestives de la plupart des sites web courants. Vus d'un très mauvais oeil par les éditeurs de contenu, ainsi que les régies publicitaires, ces extensions n'ont cessé d'être au centre de batailles techniques et juridiques. 

Pourtant, certaines d'entre elles se sont vues accorder plus de clémence de la part des régies publicitaires, car elles ont mis en place un système de *liste blanche* laissant passer la publicité d'annonceurs qui montraient patte blanche, tout en contribuant à leur développement. C'est le cas par exemple de [AdBlockPlus](https://adblockplus.org/fr/), autrefois extension favorite qui a vu son modèle économique biaisé, et est désormais en situation de conflit d'intérêt.

D'autres bloqueurs de publicité, tels que [uBlock](https://github.com/gorhill/uBlock) militent et prônent un blocage de publicité neutre et total, et c'est aujourd'hui, la seule extension libre et fiable concernant le blocage publicitaire, ayant suffisamment de renom et de support communautaire pour être efficace.

## Comparatif de Navigateurs

Voici un petit comparatif des principaux navigateurs avec une analyse basée sur la **performance**, le respect de la **vie privée**, ainsi que certaines fonctionnalités telles que le **blocage de publicités**.

### Mozzila Firefox

**Performance :** ⭐⭐⭐ <br/>
**Mémoire :**  ⭐⭐<br/>
**Vie Privée :**⭐⭐⭐⭐

Firefox, historiquement développé depuis 2002 par la fondation Mozilla a pour but d'être le navigateur respectueux de la vie privée. Il est disponible sous Android, Windows, Mac et Linux, et est généralement installé par défaut sur la plupart des distributions Linux. Son gros point fort est son respect de la vie privée (discutable parfois sur des détails, en raison de contenu sponsorisé qui sert à financer son développement), en revanche, ses performances et sa consommation mémoire sont un peu moins optimales que d'autres navigateurs, notamment ceux basés sur Chrome (Edge, Brave et Chromium).

La survie de Firefox est importante pour l'écosystème web afin d'éviter le monopole que constituerait les navigateurs basés sur Chrome.

**Verdict :** Même si ce n'est pas le meilleur, on l'aime beaucoup pour tout ce qu'il représente. 💕💕💕

### GNOME Web (Linux)

**Performance :** ⭐⭐⭐<br/>
**Mémoire :**  ⭐⭐⭐⭐⭐<br/>
**Vie Privée :**⭐⭐⭐⭐⭐

GNOME Web est un navigateur développé pour le bureau GNOME de Linux et basé sur Firefox, ainsi que le moteur d'affichage de Safari (WebKit), ce n'est pas le plus performant à l'affichage, mais il consomme moins de ressources mémoire que Firefox ou même Chrome.

Au niveau du respect de la vie privée, c'est l'un des navigateurs les plus éthiques existants : il est configuré avec un bloqueur de publicités (), et une sécurité accrue par défaut. Malheureusement, il a parfois quelques soucis de compatibilité, notamment avec les sites de Google, ainsi que la lecture de contenus avec DRM* (Google [aurait refusé](https://gitlab.gnome.org/GNOME/epiphany/-/issues/1200) au projet de leur concéder une licence de leur API)

** les contenus DRM sont par exemple des musiques et/ou vidéos ne pouvant être jouées que si vous avez un abonnement.*

**Verdict :** Très sous-estimé, léger et rapide, il possède des fonctionnalités permettant d'améliorer la vie privée. Toutefois, son manque de support DRM ainsi que le manque d'extensions peuvent manquer à certains. 🌍💗

### Google Chrome

**Performance :** ⭐⭐⭐⭐<br/>
**Mémoire :**  ⭐⭐⭐⭐<br/>
**Vie Privée :**⭐ 

Développé par Google, Chrome est le navigateur le plus utilisé au monde (+ de 80% de parts de marché) : grâce à cette hégémonie il possède la plus grande compatibilité, et de bonnes performances. Toutefois, il est truffé de code qui envoie à Google de nombreuses informations glanées sur notre navigation : ces données (analytics) servent à du profilage marketing et à cibler de la publicité. 

Il est disponible sur tous les systèmes : Android, Windows, Linux et MacOS

**Verdict :** Le plus populaire, plein de fonctionnalités et le moins respectueux de la vie privée. 💩

### Chromium

**Performance :** ⭐⭐⭐⭐<br/>
**Mémoire :** ⭐⭐⭐⭐⭐<br/>
**Vie Privée :**⭐⭐⭐ (Certaines versions comme ungoogled-chromium sont 5 ⭐)

Chromium se définit comme la **partie libre du navigateur Chrome** de Google (en réalité, Chrome est un navigateur Chromium truffé de code peu éthique accédant aux services de Google). Il possède encore quelques appels aux services de Google, que certains libristes ont retiré dans une variante appelée **Ungoogled Chromium**.

Toutefois, retirer des services de Google en font un navigateur un peu plus complexe à utiliser : Par exemple l'accès aux extensions demande un peu de bidouille.

**Verdict :** Dans tous les cas, une meilleure alternative à Chrome pour ce qui est de la vie privée. Un peu plus léger, mais aussi destiné à un public un peu plus bidouilleur.🔧😎🛠

### Microsoft Edge

**Performance :** ⭐⭐⭐⭐<br/>
**Mémoire :** ⭐⭐⭐⭐⭐<br/>
**Vie Privée :**⭐

Développé par Microsoft, Edge est le navigateur remplaçant Internet Explorer par défaut depuis Windows 10. Il est basé sur Chromium, et possède également de la télémétrie et des analytics qui collectent beaucoup de données de navigation, dans un but de profilage marketing. Au niveau de l'éthique et du respect de la vie privée, il n'est donc pas meilleur que Chrome et reste un des pires élèves à ce jour.

Au niveau des performances en revanche, il s'avère encore meilleur que Chrome, et notamment au niveau de l'utilisation mémoire, ce qui en fait l'un des navigateurs les plus performants du marché. 

On peut l'installer sur Windows, Linux et MacOS.

**Verdict : ** Pas grand chose de plus à dire que Google Chrome. Rapide et plein de fonctionnalités qui se feront un régal de prendre vos données personnelles. 💩

### Opera

**Performance :** ⭐⭐⭐⭐⭐<br/>
**Mémoire :** ⭐⭐⭐<br/>
**Vie Privée :**⭐ ?

Opera a toujours été le troisième compétiteur, et a toujours été innovant dans sa manière d'apporter des curiosités de navigation. Il est aujourd'hui une bonne alternative à Google Chrome et Microsoft Edge, notamment car il embarque un bloqueur de publicité et un VPN. Il est, comme Microsoft Edge, basé sur Chromium.

Toutefois, son code étant propriétaire, on ne peut pas vraiment présager des données qu'il collecte à notre insu. Depuis son rachat en 2015 par la société chinoise Golden Brick, et la législation chinoise en matière de respect des données personnelles, la question reste en suspens.

De plus il promeut dès l'installation des services peu respectueux de la vie privée, comme facebook, whatsapp...

**Verdict :** Opera reste une bonne alternative à celle des 2 autres géants, mais avec de grosses interrogations sur la collecte de données et l'éthique. 🤷‍♂️

### Brave

**Performance :** ⭐⭐⭐<br/>
**Mémoire :** ⭐⭐⭐⭐<br/>
**Vie Privée :**⭐⭐⭐ (débatable)

Encore une variante basée sur Chromium, ce navigateur au code source libre promeut la vie privée des utilisateurs de manière contestable. Il intègre un bloqueur de publicité qui en réalité remplace les publicités par d'autres publicités, au profit de la société qui développe le navigateur.

Il propose un système de rétribution des utilisateurs (Brave Rewards) sous forme de crypto-monnaies (BAT) lorsque l'on active le remplacement des publicités, notamment par des publicités pour des services de crypto-actifs et de finance. Son double-discours ne nous inspire que très peu de confiance, car il avance des arguments contradictoires. 

Au niveau des performances, on est grosso-modo sur les mêmes performances que les autres compétiteurs à Google Chrome, à savoir un navigateur relativement rapide.

**Verdict : ** Si vous détestez Google, Microsoft et que vous préférez détourner l'argent de la publicité au profit des développeurs de Brave, ainsi que vous faire de l'argent, c'est pour vous. Un produit douteux. 🤷‍♀️ 🤔🤨

### Apple Safari (Mac)

**Performance :** ⭐⭐⭐<br/>
**Mémoire :** ⭐⭐⭐⭐<br/>
**Vie Privée :**⭐⭐⭐

Navigateur par défaut sur Mac OS faisant partie du système d'exploitation, Safari est, à l'image des autres applications de ce système, simple et efficace. A la différence de beaucoup d'autres navigateurs, il utilise son propre moteur de rendu (WebKit, aussi utilisé dans GNOME Web). 

Propriétaire, son code ne permet pas d'être audité et de savoir quelles informations sont collectées. On suppose que l'activation des publicités ciblées dans MacOS entraine la collecte d'usage sur Safari, mais on ne peut que le supposer.

Il n'intègre pas de bloqueur de publicité, mais on peut y installer AdBlockPlus. uBlock Origin peut être installé mais seulement sur les versions antérieurs à la version 13 (2020, Big Sur)

Au niveau performance, il est relativement léger et rapide, mais toutefois sa compatibilité avec certains sites (comme les services Google) est parfois hasardeuse.

**Verdict** : Le choix par défaut sur Mac, fidèle aux valeurs de Apple, des promesses de respect de la vie privée ne pouvant être vérifiées. 😎🍏🤨

## Conclusion : Notre avis

Il est complexe aujourd'hui de se prononcer sur un navigateur qui aurait tous les atouts de son côté, car la plupart des solutions commerciales sont aujourd'hui bien plus performantes par rapport aux initiatives libres et ouvertes, comme Firefox. Toutefois, il est important de mettre dans la balance que ces solutions, la plupart basées sur le moteur de rendu de Google Chrome créent une uniformisation, et de fait un monopole de la part de Google, qui lui permettent de mener des politiques restrictives en implémentant des fonctionnalités scélérates, comme des DRM par page.

**Firefox** reste selon nous, la meilleure alternative en termes de respect de la vie privée, et des enjeux sociétaux, mais sa consommation mémoire peut être limitante à l'utilisation sur des machines assez anciennes. 

**GNOME Web**, si vous êtes sous Linux est très prometteur, à l'heure où nous écrivons, sa version 45 a fait d'énormes progrès et offre un confort de navigation et une éthique très agréable, malgré encore quelques rugosités.

**Chromium**, la version libre de Google Chrome se défend très bien elle aussi surtout sa variante **Ungoogled**, qui permet les performances de Chrome sans ses atteintes à la vie privée. Toutefois, l'utiliser permet de conforter Google dans son hégémonie.

Le navigateur qui s'en sort le mieux au niveau de la consommation mémoire reste **Microsoft Edge**, basé sur Chrome, et qui a pour renom de capter beaucoup d'informations personnelles. Pour cette raison nous ne le conseillons que très peu.

