# Installer une distribution Linux

**Une fois décidé à franchir le pas, généralement après un [essai de linux via une distribution "Live"](/guides-pc/live-usb), on peut passer à l'installation de linux sur notre ordinateur. Cependant, il est important de comprendre ce que nous allons faire et se préparer à l'installation.** 

Au préalable, nous allons faire quelques vérifications : choisir quel type d'installation nous souhaitons, sauvegarder nos données importantes, et s'assurer que nos données ne seront pas perdues, le tout afin que nous puissions installer Linux l'esprit tranquille.

![Clé USB - Brina Blum (Unsplash)](/images/brina-blum-Bb_X4JgSqIM-unsplash-cropped-scaled.png)

## Se préparer à accueillir Linux

Avant d'accueillir Linux, nous allons procéder à quelques dernières vérifications, mettre de côté les données importantes et choisir comment nous allons installer linux. Nous allons rassembler les éléments suivants :

- Une Clé USB sur laquelle [installer une version Live de notre distribution](guides-pc/live-usb)
- Si besoin de sauvegarder nos données : Un disque externe USB ou bien une Clé USB avec suffisamment d'espace pour la sauvegarde
- Un peu de temps devant nous, si l'installation peut prendre une trentaine de minutes, prendre son temps pour sauvegarder nos données est primordial.
- Si besoin de changer un disque dur mécanique pour un SSD, notre nouveau disque, préalablement acheté, et un peu de temps pour le remplacer. (Prévoyez également un kit avec des tournevis d'électronique, tel qu'un de ceux vendus par le site [I Fix It](https://fr.ifixit.com/News/48672/quel-est-le-kit-de-tournevis-ifixit-quil-vous-faut))

### Choisir quel type d'installation

Lorsque nous installons linux sur un ordinateur nous pouvons l'installer de deux manières différentes :

- Soit de manière **exclusive** : auquel cas le système d'exploitation actuel (par exemple, Windows) sera **remplacé**.
- Soit en **cohabitation** avec l'autre système d'exploitation : dans ce cas là l'espace libre de notre disque sera **partagé** en deux, avec une partie réservée à Windows, et l'autre pour Linux. Ce mode s'appelle également **dual-boot**, et permet de démarrer l'ordinateur soit sur un système, ou sur un autre.

> A moins que nous aiyons une très bonne raison de garder les deux systèmes (par exemple, un logiciel, indispensable n'existant pas sous Linux), il est **beaucoup plus simple de ne garder qu'un seul système d'exploitation** sur son ordinateur. De plus, l'espace disque n'est pas divisé pour chacun des systèmes et on peut bénéficier son l'entièreté.

### Changer son disque dur?

Lors de l'installation de Linux, nous allons remplacer les données (et au moins partiellement si nous optons pour une installation en dual-boot). Si **l'installation** est faite de manière **exclusive**, il peut être opportun de procéder au remplacement de la mémoire de stockage pour un support plus rapide et/ou de plus de grande taille de stockage (surtout si notre ordinateur utilise un disque dur mécanique) : 

> ... par exemple, remplacer un disque dur SATA 250GB par un SSD SATA de 500GB.

Cette opération demande souvent un peu de bricolage, que l'on soit sur un ordinateur de bureau ou un portable. Le matériel ancien est généralement plus facile à ouvrir et mettre à jour que le matériel récent.

![Remplacer le Disque dur d'un laptop par un SSD](/images/laptop-replace-ssd.png)

**Pour plus d'informations voir :**

- [Remettre en état son ordinateur](guides-pc/remettre-en-etat)
- [I Fix It](https://fr.ifixit.com) : contient de très nombreux guides pour ouvrir et remplacer du matériel, en français, mais aussi en anglais (plus fourni).

> **Sauvegarde des données : ** Si notre disque dur actuel n'était pas chiffré (par exemple avec **bitlocker**), ses données resteront accessibles, car nous allons l'extraire de l'ordinateur pour le remplacer par un nouveau disque, et il sera alors possible de le connecter à nouveau à l'ordinateur (via un [cable SATA/USB](), ou en le mettant dans un [Boitier externe USB pour disque dur]()).

> **Réemploi du disque dur : ** De plus, nos anciens disques peuvent devenir de fidèles alliés, par exemple en les transformant en [Supports de Sauvegarde à Froid (cold storage backup)]()

### Sauvegarder ses données

Puisque nous allons effacer nos données de notre disque de stockage actuel, il est important d'en sauvegarder les données. Pour ce faire il est conseillé d'effectuer une copie de nos données personnelles :

- Sur un disque externe ou une clé USB
- Sur un service de données dans le cloud (Nextcloud, Google Drive, Dropbox, ...)

![Un disque externe utilisé pour sauvegarder des données](/images/backup-external-disk.png)

<u>Généralement, deux types de données sont présentes sur notre ordinateur :</u> 

- Nos fichiers personnels, souvent diponibles dans des Dossiers personnels par type (Bureau, Mes Documents, Mes Images, Ma Musique, Mes Vidéos, Téléchargements) : On peut prendre ces fichiers et les copier coller sur un disque externe (ou clé USB) dans des dossiers équivalents.
- Des fichiers personnels stockés ailleurs sur le disque : celà arrive également, si votre machine est équipée de plusieurs disques (ou partitions), que des fichiers soient stockés sur un autre disque (par exemple un disque D: sous Windows)
- Les données de nos applications, par exemple nos navigateurs web (Firefox, Chrome, Edge), messagerie (Thunderbird, Outlook). Celles-ci sont plus complexes à exporter, mais heureusement, la plupart des navigateurs permettent désormais, soit de synchroniser les informations dans le cloud, soit de [sauvegarder et restaurer](https://support.mozilla.org/fr/kb/sauvegarder-restaurer-informations-profils-firefox) votre profil utilisateur.

---

## Installer Linux de manière Exclusive

Installer linux de manière exclusive est relativement simple, car nous allons effacer totalement notre disque interne de notre ordinateur pour le remplacer par un système linux fonctionnel. La procédure est relativement **simple et peu risquée**, pour peu que nous aiyons bien pris le temps d'avoir sauvegardé nos données personnelles sur un autre disque.

> **Ce qui pourrait mal tourner ?** Au pire des cas, un problème dans l'installation de notre distribution nous demanderait de relancer l'installation.

![Un portable démarrant Fedora Linux](/images/laptop-running-fedora.png)

Afin d'installer une version de linux voici divers liens qui vous amèneront vers des guides dédiés à des distributions différentes:

- [Linux Mint 21.2 Cinnamon](/guides-pc/install-linux-mint-21-2-cinnamon)
- [Ubuntu 22.04](/guides-pc/install-ubuntu-22-04)

> **Ma distribution n'est pas listée** : Ce n'est généralement pas un problème en soi, car les installeurs des principales distributions se ressemblent, car ils sont généralement basés sur des distributions similaires : par exemple, on voit beaucoup de similarités dans les installeurs de Linux Mint et Ubuntu, car la première distribution est basée sur la seconde.

---

## Installer Linux aux cotés d'un autre système (Dual Boot)

Installer Linux en mode **dual boot** est légèrement plus complexe qu'en mode exclusif, et il nécessite une bonne compréhension des disques et notamment des partitions. 

![](/images/linux-dual-boot.png)

Le guide est actuellement planifié pour être rédigé, et sera bientôt disponible. En attendant, un très bon article sur le site de [LeCrabeInfo](https://lecrabeinfo.net/installer-ubuntu-20-04-lts-dual-boot-windows-10.html).

---

## Et après ?

Une fois Linux Installé, vous pouvez profiter pleinement de son utilisation. S'il est installé en dual boot, son chargeur de démarrage **Grub2** vous demandera à chacun des démarrages de l'ordinateur sur quel système vous souhaitez démarrer.

Une fois installé, vous pouvez ôter la clé USB de l'image Live, du port de votre ordinateur et redémarrer. Nos données ayant été sauvegardées sur un disque externe, n**ous pouvons maintenant les copier à nouveau sur notre nouvelle installation**.

Bienvenue dans l'univers Linux !



