# Démarrage d'un ordinateur

S'il existe une phase que toutes nos machines ont en commun, c'est la manière dont ils démarrent : 

Avant d'être utilisables, nos ordinateurs passent par une **phase de démarrage**, plus ou moins longue en fonction de l'âge et de la puissance de la machine. Cette phase de démarrage s'ensuite de la phase de **démarrage du système d'exploitation**.

![Schéma de démarrage](/images/boot-simple.svg)

Durant ce laps de temps de quelques secondes à une poignée de minutes (pour les moins chanceux) qui séparent l'appui sur le bouton ON du moment où l'on peut enfin utiliser notre ordinateur, beaucoup de choses importantes se passent :

1) L'ordinateur démarre et fait des tests de bon fonctionnement du matériel

2) Le micrologiciel cherche un disque sur lequel démarrer

3) Le micrologiciel démarre le système d'exploitation depuis un disque capable de démarrer

## Phase 1 : Tests préalables (POST)

On s'en rend compte assez souvent, les premières secondes de l'allumage de l'ordinateur ont quelque chose de mystique : rien ne s'affiche, l'écran s'allume mais on a un suspens de quelques secondes avant que le démarrage ne commence réellement. Cette phase (relativement rapide) est appelée POST : "Power-On Self Test" qui se traduit en *Tests préalables à l'allumage*. Durant cette phase, l'ordinateur sonde ses composants pour vérifier s'ils sont fonctionnels et prêts à être démarrés. 

Les principaux éléments qui sont testés sont :

- Les contrôleurs présents sur la carte mère, qui permettent aux différents composants de parler entre eux (CPU, RAM, etc), l'alimentation électrique, ainsi que l'intégrité du micrologiciel.

- Le microprocesseur (CPU)

- La mémoire RAM

- Éventuellement : La carte graphique

D'autres éléments, non essentiels, comme les cartes réseau, ne sont pas testés durant cette phase, mais celà ne veut pas dire qu'ils peuvent poser problème par la suite.

> **Dépannage**: Souvent, lorsque rien ne s'affiche au démarrage et que l'ordinateur se bloque sur un écran noir, il s'agit d'un problème de matériel : soit la mémoire, le processeur, la carte graphique, ou la carte mère de l'ordinateur ont un problème.

### Affichage de l'écran de POST

![](/images/boot-screens-bios.png)

Une fois les tests d'alimentation passés, un premier écran s'affiche avec des informations sur le matériel: l'ordinateur exécute son Micrologiciel (aussi appelé firmware). Cet écran est différent d'une machine à une autre : il peut autant afficher juste le logo du constructeur du matériel, que des informations détaillées sur le démarrage du micrologiciel :

- Quantité de mémoire disponible

- Les disques détectés (pour pouvoir être éventuellement démarrés) 

- Éventuellement, des messages d'erreur comme un clavier non détecté, ou une notification si le dernier démarrage ne s'est pas bien passé.

Contrairement au système d'exploitation qui est installé sur un disque dur ou un SSD, le micrologiciel est présent directement dans une puce présente sur la carte mère, ce qui le rend indépendant des disques, imaginez si votre disque dur venait à tomber en panne, c'est tout l'ordinateur qui serait bon pour la casse ! Heureusement la puce contenant le micrologiciel est généralement plus fiable.

### Différentes générations de micrologiciels

Le micrologiciel présent sur les ordinateurs peut être de deux types : sur les ordinateurs anciens (généralement avant 2010), on trouve un BIOS (Basic Input Output System), et sur les systèmes plus récents, on trouve un UEFI (Universal Extended Firmware Interface), la différence entre les deux est généralement que l'UEFI embarque plus de fonctionnalités que le BIOS, mais aussi qu'il possède un mode "privateur" appelé *secure boot* qui permet aux constructeurs d'interdire, ou de limiter l'installation de systèmes d'exploitation non autorisés. Bien heureusement, le secure boot peut être désactivé sur beaucoup de systèmes sans poser trop de problèmes.

Pour plus d'informations voir nos rubriques [BIOS ou UEFI](#) et [Secure Boot](#)

### Séquence de démarrage et détection des disques

Lorsqu'il démarre, le micrologiciel passe en revue l'ensemble du matériel présent sur l'ordinateur : claviers, souris, cartes son, réseau, WiFi, bluetooth, et les disques actuellement connectés.

Lors du démarrage, le firmware va charger sa configuration (vitesse du processeur, tensions utilisées pour le processeur, vitesse de la mémoire), activer ou désactiver des périphériques internes, puis, énumérer les disques qu'il détecte comme **connectés** et **démarrables**

### Accéder à la configuration du BIOS/UEFI (Setup)

Lors de l'affichage de l'écran de démarrage (ou même avant), nous pouvons accéder à un outil de configuration ("Setup") du matériel de notre ordinateur. Pour y accéder, il faut appuyer sur une touche du clavier ou la laisser enfoncée au bon moment. La touche à utiliser est différente d'une machine à une autre, parfois Echap, F2, F8, F10, F12 ou même Suppr. Cette touche est souvent indiquée parmi toutes les informations affichées au démarrage.

Cet outil de configuration permet de définir comment notre ordinateur démarre, de configurer finement le matériel, ou de résoudre des problèmes plus complexes. Par exemple, lorsque l'ordinateur ne démarre plus du tout.

Le setup permet également de régler sur quel disque par défaut va démarrer l'ordinateur.

Pour plus d'informations, voir la page [BIOS/UEFI Setup](bios-uefi-setup.md)

## Etape 2 : Démarrage sur un disque (Boot Sequence)

A la fin de l'exécution du firmware, ce dernier va donner la main à un **disque de démarrage**, afin de lancer un **système d'exploitation**. 

> S'il ne trouve pas de disque sur lequel démarrer, l'ordinateur se bloque sur un écran indiquant qu'il ne peut pas démarrer.

![](/images/boot-disque-priorite.svg)

Les disques détectés par le firmware peuvent être des disques internes (SSD ou disque durs), des lecteurs DVD/BluRay, des clés USB ou encore des disques externes branchés en USB, ou encore des cartes SD si ces périphériques externes sont insérés avant le démarrage de l'ordinateur.

#### Priorité de démarrage

Dans le firmware est stockée une **liste de priorité** des disques, qui permet de choisir sur quel disque démarrer en priorité, elle peut être modifiée dans l'interface de configuration du firmware (Setup). Parfois encore, une touche permet lors du démarrage choisir temporairement sur quel disque démarrer (par exemple une clé USB d'installation) 

![](/images/boot-menu.png)

#### Types de disques de démarrage

- Sur les disques internes à l'ordinateur, le démarrage d'un **système d'exploitation** déjà installé (Windows/Linux)

- Sur les supports externes (DVD, Clés USB, Cartes SD) le démarrage de l'**installeur** pour un système d'exploitation, ou bien une version dite "**Live**" d'un système d'exploitation (version en lecture seule destinée à tester le système d'exploitation sans l'installer)

- Très rarement, on peut également démarrer sur le réseau (net boot / PXE), celà arrive parfois dans certaines entreprises. Au lieu de démarrer sur un disque, l'ordinateur va chercher les données sur un autre ordinateur connecté au réseau

## Etape 3 : Démarrage du système d'exploitation

Que l'on utilise Linux, macOS ou Windows, le principe du chargement du système d'exploitation est relativement simple : durant le démarrage le firmware passe en revue tous les disques qu'il détecte. 

Aussitôt qu'il trouve un disque "bootable", c'est à dire possédant les données d'un [chargeur de démarrage (bootloader)](), il l'ajoute à la liste des disques démarrables.

Puis, en suivant l'ordre de priorité défini dans le firmware, il va choisir le premier disque démarrable, et tenter de lancer son **bootloader**. 

Le **bootloader** est un petit bout de logiciel présent cette fois-ci sur le disque : Prenant le relais du firmware, il est capable de charger un **système d'exploitation** complet (ou même plusieurs, dans le cas du chargeur [grub](#) pour linux)

> **Important** : Alors que le fonctionnement est relativement similaire, les détails du démarrage sont légèrements différents lorsque l'on démarre depuis un BIOS ou un UEFI. Pour plus d'informations voir la section  [BIOS et UEFI](#)

Un disque vierge et/ou neuf, ne possède aucune donnée, il est donc impossible de démarrer dessus. L'installatoin du chargeur de démarrage se fait généralement lorsque l'on installe un système d'exploitation depuis un support externe.

Pour plus d'informations voir notre article [bootloader, partitions et systèmes d'exploitation](#)

Sur des ordinateurs modernes, la séquence de démarrage (POST) ainsi que le chargement du système d'exploitation sont quasiment imperceptibles à l'écran. 

## Extinction et Redémarrage de l'ordinateur

Lorsque l'on éteint ou redémarre notre ordinateur, le système d'exploitation est déchargé de la mémoire et l'ordinateur peut ensuite s'éteindre ou redémarrer. 

Lors d'un redémarrage, l'ordinateur repasse par la phase de POST et l'exécution du firmware, avant de re-charger à nouveau le système d'exploitation.

> **📑 Hibernation :** Parfois, il arrive qu'un système d'exploitation "hiberne" : c'est un mode où l'ordinateur s'éteint en sauvegardant l'état dans lequel il se trouve (documents ouverts, programmes, etc) . Lors d'un redémarrage après une hibernation, au lieu de démarrer normalement, l'ordinateur va charger depuis le disque l'état exact dans lequel était le système d'exploitation avant d'entrer en hibernation.

## Différence entre Veille, Hibernation et Extinction

Il existe 3 états distincts lorsque nous ne nous servons pas de notre ordinateur:

- La **veille** : l'ordinateur éteint la plupart de ses périphériques (écran, son, clavier) et n'alimente que le minimum pour pouvoir rester allumé, sans consommer beaucoup d'énergie. C'est souvent ce qui se passe lorsque vous refermez le clapet de votre ordinateur portable, ou que vous appuyez sur le bouton de votre smartphone pour éteindre l'écran. Dans cet état, l'ordinateur est toujours allumé, et est disponible en une ou quelques secondes pour travailler à nouveau. Sortir de veille ne repasse pas par le démarrage de l'ordinateur, car nous ne l'avons tout simplement pas éteint.
- **L'extinction** met l'ordinateur ou le téléphone hors tension, ce qui fait qu'il va devoir repasser par une phase plus longue de démarrage avant d'être à nouveau disponible. Lors d'une vraie extinction, le redémarrage va relancer les programmes "proprement", mais si vous aviez éteint l'ordinateur dans un certain état (documents et navigateurs web ouverts, etc), vous ne retrouverez pas cet état.
- **L'hibernation** est similaire à la phase d'extinction, mais à la différence près que l'ordinateur sauvegarde son état, permettant un démarrage un peu plus rapide au prochain démarrage.

## Dépanner le démarrage d'un ordinateur

Il peut être compliqué de comprendre pourquoi notre ordinateur ne démarre plus, et ce en raison d'un très grand nombre de raisons:

**Une ou plusieurs défaillances matérielles majeures :**

* Problème général d'alimentation : soit une défaillance de l'alimentation, soit un problème lié à une alimentation insuffisante à la suite de l'ajout d'un matériel.
* Défaillance de la carte mère
* Défaillance du Processeur
* Défaiillance de la Mémoire RAM
* Défaillance de Carte Graphique

**Des problèmes de branchements des éléments sur la carte mère:**

- Barettes de RAM mal insérées
- Processeur mal inséré, ou ventilateur mal monté
- Carte graphique mal insérée dans le port PCI-Express

**Un problème de configuration dans le firmware :**

- Mauvaise Configuration du voltage du processeur
- Mauvaise Configuration de la vitesse de la RAM

