# Dans mon Ordinateur

Nos ordinateurs sont généralement composés des mêmes éléments, qu'ils soient des PC de bureau, des laptops, ou même des hybrides portables et tablettes. Même si la taille et la forme changent, ces éléments se retrouvent toujours connectés ensemble, avec plus ou moins de miniaturisation.

![](/images/dans-mon-pc.svg)

## L'alimentation électrique

L'alimentation de notre ordinateur a pour tâche principale de prendre le courant électrique domestique (220 Volts) et le convertir en courant compatible avec l'ordinateur, que ce soit un PC de bureau ou un portable, le courant qui passe dans l'ordinateur est très faible. Sur un ordinateur **portable**, l'Alimentation est également responsable de **charger la batterie** et d'alimenter l'ordinateur sans elle si l'ordinateur est branché sur le courant secteur. L'alimentation est généralement connectée à la *carte mère* de l'ordinateur.

## La carte Mère

La carte mère de l'ordinateur est l'épine dorsale sur laquelle sont **interconnectés** tous ses **composants** et ses **périphériques**. On y trouve des éléments soient connectés directement dessus, soit connectés via des câbles.

C'est un composant très complexe et important, car elle orchestre la communication entre tous les éléments. Elle possède un grand nombre de composants électroniques, le plus important d'entre eux étant le **firmware**, permettant à l'ordinateur de démarrer.

## Le Microprocesseur 

Aussi appelé Processeur, ou CPU, il est le composant qui réalise les calculs principaux de l'ordinateur et est indispensable au fonctionnement de celui-ci. Il est généralement composé d'un ou plusieurs **cores** (coeurs) qui peuvent calculer chacun indépendamment des autres, afin de réaliser plusieurs taches en parallèle. Il est généralement monté sur la carte mère et accompagné d'un ventilateur/radiateur afin de dissiper l'énorme chaleur qu'il dégage.

## La Mémoire Vive

Aussi appelée RAM, elle accompagne le processeur car c'est là que sont stockées les données en cours d'utilisation (par exemple, le document de traitement de texte que vous n'avez pas encore sauvegardé sur le disque). Elle est très rapide, mais malheureusement très volatile : une coupure de courant et tout est perdu ! C'est pour celà qu'elle échange souvent avec la **mémoire de stockage** (disques durs, ssd, clés usb) afin de conserver les informations lorsque l'ordinateur est éteint.

La RAM est souvent présente sous forme de **barrettes** amovibles que l'on peut remplacer ou bien ajouter à l'intérieur de l'ordinateur. C'est un composant que l'on utilise souvent pour améliorer les performances de l'ordinateur.

## La Mémoire de Stockage

La **Mémoire de Stockage** garde les informations sauvegardées sur l'ordinateur sur le long terme. C'est là que résident nos documents, nos programmes installés et notre système d'exploitation. On en trouve sous une grande variété de types, chacun ayant ses spécificités. Généralement, il est possible de remplacer la mémoire de stockage d'un ordinateur afin d'augmenter sa taille, mais également sa vitesse. C'est l'un des composants qui peut faire une grande différence lorsque l'on décide de donner une seconde vie à un ordinateur.

On la trouve sous différentes formes en tant que **stockage interne** aux ordinateurs:

![Image de disques et cables SATA]()

- Des **Disques Internes** de type **Disque Dur (HDD)** ou **Flash (SSD)**, ces derniers sont généralement connectés à la carte mère via des cables SATA. Les disques durs sont composés de plateaux magnétiques tournant à haute vitesse sur laquelle une tête de lecture se déplace, et sont généralement plus lents, et plus fragiles que les SSD. Ces derniers sont composés de puces électroniques contenant de la mémoire flash, bien plus rapide et résistante aux chocs.

![Image de disque NVMe]()

- Des **Disques Internes M.2 (NVMe ou SATA)** : Ils sont connectés directement sur la carte mère via un petit connecteur appelé M.2. Ce connecteur est légèrement différent et présente un détrompeur afin de ne pas les confondre. Les disques NVMe sont bien plus rapides que les disques SATA (environ 5 à 10x) et sont aujourd'hui la technologie la plus rapide en termes de stockage.
- Plus rarement, de la mémoire fixe **eMMC** sert de stockage bon marché sur des ordinateurs bas de gamme, elle est très souvent soudée sur la carte mère et offre des performances similaires à un disque dur.

La mémoire de stockage peut également se trouver sour forme  **externe** à l'ordinateur, pour y être branché ponctuellement, ou être partagé entre plusieurs ordinateur. On la trouve sous les formes suivantes :

![Images de disques externes USB]

- Des **Disques Externes USB** sous forme de Clés USB, Disques miniatures ou encore de boitiers USB dans lesquels l'on place un disque SATA ou NVMe, leur vitesse est variable et dépend à la fois de la vitesse du disque, ainsi que de la vitesse du port USB sur lequel ils sont branchés. La mémoire des clés USB est généralement moins rapide que celle des SSD, et assez équivalente à celle d'un disque dur.
- Des **Disques optiques** tels que des CD, DVD ou Blu-Ray. Ceux-ci ont généralement la particularité d'être en *lecture seule*
- Des **Cartes Mémoires** à insérer dans un lecteur SDCard. Celles ci sont relativement lentes et servent généralement à stocker des fichiers (photos, documents, vidéos)
- Des **Serveurs de stockage réseau** (NAS) accessibles via Wi-Fi ou le réseau filaire ethernet.

## La Carte Graphique (GPU)

Ce composant est généralement utilisé afin d'améliorer les performances graphiques d'un ordinateur afin de pouvoir y faire tourner des Jeux Vidéos, ou encore travailler sur des logiciels de Montage Vidéo, Photographie ou encore de Modélisation et Rendu 3D. Les cartes graphiques sont présentes de deux manières distinctes dans un ordinateur:

* Soit de manière **Intégrée au CPU** dans une partie de la puce de celui-ci, on parlera de carte graphique intégrée ou IGP, c'est souvent le cas des processeurs Intel

- Soit en tant que périphérique dédié sous forme de **Carte Graphique** connectée à la carte mère d'un PC de bureau (via un port PCI-Express), ou présente sur la carte mère d'un ordinateur portable.

Les cartes graphiques présentent quasiment toutes une ou plusieurs **sorties vidéos**, sous forme de port HDMI (ou DisplayPort), permettant de brancher un ou plusieurs écrans. Les ordinateurs portables ont leur écran également connecté à la carte graphique via un connecteur réduit dédié interne.

Les cartes graphiques possèdent des performances très variées en fonction de l'usage auquel elles sont destinées : 

- Les **cartes graphiques intégrées**, ainsi que les cartes dédiées bas de gamme sont généralement destinées à la lecture et l'encodage de vidéos. Elles consomment peu d'énergie et par conséquent permettent aux ordinateurs portables d'avoir une plus grande autonomie.
- Les **cartes graphiques dédiées**, de milieu et haut de gamme offrent des performances de calcul pouvant être très impressionnantes, mais également une consommation énergétique allant de pair avec ces performances. Lorsqu'elles sont utilisées sur des ordinateurs portables, la chaleur dégagée ainsi que la puissance de calcul réduisent grandement l'autonomie de la batterie, il est d'ailleurs souvent conseillé de brancher son ordinateur portable avant de les utiliser.

Certains ordinateurs portables possèdent à la fois une carte graphique intégrée, ainsi qu'une carte graphique dédiée : cette combinaison de deux cartes graphiques dites **hybrides** peuvent être allumées et éteintes en fonction des besoins, afin de pouvoir avoir une grande flexibilité entre autonomie et performance.



## Les Interfaces et Périphériques

Au delà de la carte mère et des composants internes, peuvent être connectés une myriade d'appareils à notre ordinateur. Ces appareils sont appelés périphériques car ils gravitent autour de l'ordinateur. On y trouve par exemple des **claviers**, **souris**, **manettes de jeu**, **webcams**, ou encore des **disques durs externes**. La plupart de ces périphériques sont connectés via des ports USB : ces ports sont de taille et de formes différentes en fonction de leurs normes.

![Images de Ports et câbles USB]()



Il existe également des ports de **sorties Vidéo** permettant de connecter un ou plusieurs écrans à l'ordinateur, ces ports sont souvent de type HDMI ou Display Port. Plus anciennement, on trouvait des ports DVI et VGA, mais ces ports sont moins, ou plus du tout utilisés désormais.

![Images de ports Vidéo]()



D'autres périphériques peuvent aussi être connectés via le réseau, soit filaire, via des **câbles ethernet**, soit sans fil, en les connectant à un réseau Wi-Fi. Par exemple on peut trouver des imprimantes, ou des serveurs de stockage réseau (NAS).

![Images de ports Ethernet]()



