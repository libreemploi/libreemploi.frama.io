# Installer Ubuntu 22.04

Installer le système Ubuntu est probablement la manière la plus simple d'installer linux à l'heure actuelle, non seulement parce que son installeur est extrêmement simple, mais également parce que sa compatibilité avec le matériel en fait l'un des systèmes linux les plus fiables.

Ubuntu 22.04 est sortie en Avril (04) 2022 et est estampillée **LTS** (Long Term Support) ce qui en fait une version supportée durant plusieurs années. Elle ne présente pas des logiciels le plus à jour possible, mais au contraire s'assure que le système est stable et fonctionne avec le moins de bugs possibles.

Elle sera supplantée en Avril 2024 par Ubuntu 24.04, la nouvelle version LTS de Ubuntu, dès lors que cette version sera disponible, il sera possible de faire une **mise à niveau** de votre version 22.04 vers la nouvelle version.

## Pré-Requis

Ubuntu 22.04 est un système 64bits, qui fonctionne sur la plupart des processeurs dits "modernes" introduits à partir de 2003 : [Processeurs Intel 64bits](https://fr.wikipedia.org/wiki/Intel_64) ou [Processeurs AMD 64bits](https://fr.wikipedia.org/wiki/AMD64)

Ubuntu requiert un minimum de 2GB de Mémoire RAM pour fonctionner convenablement, pourtant 4GB permettent de fonctionner de manière plus agréable. Un environnement de travail photo et/ou vidéo peut demander encore plus de mémoire.

L'image Live d'Ubuntu pèse 3.7 GB, elle nécessite donc une clé USB d'au moins 4GB pour être installée.

Le système de base, une fois installé, requiert environ 8GB d'espace disque. Toutefois, les mises à jour et les applications installées ultérieurement peuvent faire grossir cette taille.

## Démarrer depuis une Clé USB Live

Afin d'installer Ubuntu 22.04, il faut tout d'abord créer une [Clé USB "Live"](/guides-pc/live-usb) de la version actuelle de Ubuntu 22.04. Son image ISO peut être téléchargée à cette adresse :

https://releases.ubuntu.com/22.04/ubuntu-22.04.3-desktop-amd64.iso

Si votre système le supporte, l'image peut démarrer en mode UEFI.

### Première Partie : Démarrage sur la clé Live

Lorsque l'on démarre sur clé Live de Ubuntu, on arrive sur un écran de démarrage (en anglais) qui nous permet de choisir parmi plusisurs options. Généralement, on choisit **Try or Install Ubuntu**. 

> Il arrive que nous aiyons à choisir **Ubuntu (Safe Graphics)** si jamais le démarrage n'affichait rien. C'est très rare, mais celà peut arriver.

![Démarrage depuis l'image Live USB](/images/install-ubuntu-22-04-01-boot-usb.png)

L'installeur graphique démarre, et va vous présenter plusieurs pages, pour chacune d'entre elles vous pouvez valider en cliquant sur **Continuer**, ou revenir en arrière en cliquant sur **Précédent**

---

Une fois sélectionné avec les flèches, on appuie sur entrée, et on démarre Ubuntu. Celà peut prendre environ 1 minute en fonction de la vitesse de votre clé USB.

![Chargement de Ubuntu Live](/images/install-ubuntu-22-04-02-live-loading.png)

---

Une fois chargé, on arrive dans l'interface graphique de ubuntu. Et l'application d'installation va démarrer dans quelques secondes.

![Démarrage de l'interface de Ubuntu](/images/install-ubuntu-22-04-03-live-start.png)

---

### Passer en mode installation

La fenêtre d'installation s'ouvre, et l'on peut choisir notre langue (Français), puis choisir si nous voulons [essayer ubuntu](/guides-pc/live-usb) ou l'installer. On choisit **Installer Ubuntu**

![Ecran de démarrage : choisir la langue, et Installer](/images/install-ubuntu-22-04-04-select-install.png)

---

**Choisir le clavier** : La première étape est de bien configurer le clavier. Si nous avons choisi la langue française, le programme va choisir un clavier AZERTY compatible (mais on peut l'ajuster si besoin)

![Choisir le clavier](/images/install-ubuntu-22-04-05-select-keyboard.png)

---

**Configurer le Wi-Fi (Si présent)** : Si une carte Wi-fi est détectée, vous pouvez vous connecter à un réseau Wi-fi, il sera utilisé pour télécharger les mises à jour au cours de l'installation.

![Choisir le clavier](/images/install-ubuntu-22-04-05b1-select-wifi.png)

![Choisir le clavier](/images/install-ubuntu-22-04-05b1-wifi-password.png)

---

**Choisir son type d'installation :** L'étape suivante consiste à choisir le **type d'installation** et les logiciels présents après l'installation : 

- Installation Normale : installe Firefox, Libre Office, ainsi qu'un lecteur de musique, image, vidéo et quelques jeux
- Installation Minimale : n'installe que firefox ainsi que quelques utilitaires

Vous pouvez cocher "Télécharger les mises à jour pendant l'installation" pour avoir un système à jour dès le premier démarrage. Sinon vous pourrez mettre à jour ultérieurement.

On vous conseille d'activer "Installer un logiciel tiers pour le matériel graphique, Wifi et des formats supplémentaires". Ce ne sont pas des logiciels libres, mais ils restent gratuits et permettent une plus grande compatibilité du système.

![Quel type d'installation](/images/install-ubuntu-22-04-06-install-type.png)

---

### Partitionnement du Disque

**Partitionnement du disque** : Cette étape est cruciale pour installer Ubuntu. Vous pouvez choisir différentes options :

- **Installer Ubuntu à coté des autres** : ceci est l'option à choisir pour effectuer une installation en **dual-boot** , par exemple pour conserver Windows.
- **Effacer le disque et installer Ubuntu** : ceci est l'option à choisir pour n'installer que Ubuntu, les autres systèmes d'exploitations du disque seront effacés (pensez donc à sauvegarder vos données)
- **Autre chose** : ceci ouvre un menu avancé de configuration de disque, pour utilisateurs confirmés.

![Comment partitionner le disque](/images/install-ubuntu-22-04-07-disk-partition.png)

---

### Démarrage de l'installation sur votre ordinateur

Une fois sélectionné, l'installeur va vous demander de **Confirmer** votre sélection.

> **IMPORTANT** : C'est le moment où l'installeur va procéder à l'effacement de données sur le disque afin d'installer Ubuntu. Une fois validée, cette étape est irréversible. Pensez bien à avoir sauvegardé vos données personnelles sur un disque externe ou une clé USB.

![Confirmer le partitionnement](/images/install-ubuntu-22-04-08-disk-confirm.png)

Une fois validé, l'installation va démarrer et nous allons pouvoir terminer de configurer le système pendant qu'il s'installe.

---

**Choix du fuseau Horaire** : A partir de votre choix de langue et du clavier, un fuseau horaire est choisi par défaut, mais vous pouvez le modifier.

![Choisir le fuseau horaire](/images/install-ubuntu-22-04-09-choose-timezone.png)

---

**Nom de la machine, et de l'utilisateur principal** : Sur cet écran, nous allons configurer le nom de notre machine, ainsi qu'un utilisateur principal qui pourra effectuer de la maintenance* :

- **Votre nom** : Le nom complet de votre utilisateur, uniquement utilisé pour l'affichage
- **Le nom de votre ordinateur** : Le nom de l'ordinateur tel qu'il apparaitra aux autres machines du réseau local (si besoin). On choisit généralement un seul mot, sans espaces et sans ponctuation
- **Choisir un nom d'utilisateur** : il correspond au **login**, c'est le nom utilisé par le système pour vous identifier, en général on choisit une version courte de notre nom complet, par exemple notre prénom. Il doit être sans espace, ni ponctuation
- **Mot de passe (et confirmation)** : Il est important d'avoir un mot de passe assez sécurisé, l'installeur vous indiquera la force de votre mot de passe.

Des options vous permettent également de choisir si vous **souhaitez arriver sur le bureau sans avoir à taper de mot de passe au démarrage**, ou bien si vous souhaitez protéger votre ordinateur avec votre mot de passe. Dans tous les cas, votre mot de passe sera demandé à la sortie de veille de l'ordinateur. 

Nous vous conseillons de choisir l'option **Demander mon mot de passe pour ouvrir une session**.

> L'option Active directory est réservée à une configuration particulière, telle que les réseaux d'entreprise.

> (*) L'utilisateur principal a les droits **super-utilisateur** qui lui permettent d'ajouter et supprimer des logiciels et composants sur la machine, ainsi que d'effectuer des tâches de maintenance avancées (ajouter/supprimer des utilisateurs)

![Nommer la machine et l'utilisateur](/images/install-ubuntu-22-04-10-user.png)

---

L'installeur continue ensuite la copie des fichiers la configuration et la mise à jour.

![Installation en cours](/images/install-ubuntu-22-04-11-install-1.png)
![Installation en cours](/images/install-ubuntu-22-04-12-install-2.png)
![Installation en cours](/images/install-ubuntu-22-04-13-install-3.png)

---

**Installation terminée** : Une fois terminé, le programme d'installation vous demande de redémarrer. 

![Installation terminée](/images/install-ubuntu-22-04-14-install-complete.png)

---

Une fois prêt à redémarrer, pensez à retirer votre clé USB de la machine, puis appuyez sur Entrée

![Fin de l'installation](/images/install-ubuntu-22-04-15-reboot-unplug-usb.png)

---

## Redémarrage et Premier démarrage de votre système

Votre ordinateur va redémarrer et lancer le système Ubuntu que vous venez d'installer :

![Premier démarrage](/images/install-ubuntu-22-04-01-boot-usb.png)

Au démarrage vous arrivez sur le choix de l'utilisateur, vous pouvez le sélectionner...

![Premier démarrage : login](/images/install-ubuntu-22-04-16-firstboot-login.png)

... puis taper votre mot de passe.

![Premier démarrage : mot de passe](/images/install-ubuntu-22-04-17-firstboot-password.png)

---

**Outil de configuration** Votre session utilisateur va démarrer, et lancer l'outil de configuration initial.

Sur la première page, vous pouvez vous connecter à vos comptes Ubuntu, Google, Nextcloud ou encore Microsoft

![Premier démarrage : Configuration 1](/images/install-ubuntu-22-04-18-firstboot-wizard-1.png)

Sur la seconde page, vous pouvez choisir d'activer la télémétrie de Ubuntu si vous le souhaitez. Par défaut cette option est désactivée, pour protéger votre vie privée.

![Premier démarrage : Configuration 2](/images/install-ubuntu-22-04-19-firstboot-wizard-2.png)

Sur cet écran, vous pouvez choisir d'activer des services de géolocalisation. Ces services peuvent être désactivés par la suite dans les paramètres système.

![Premier démarrage : Configuration 3](/images/install-ubuntu-22-04-20-firstboot-wizard-3.png)

Le dernier écran vous confirme que tout est prêt pour démarrer.

![Premier démarrage : Configuration 4](/images/install-ubuntu-22-04-21-firstboot-wizard-4.png)

## Installation Terminée !

Vous voici maintenant sur le bureau de Ubuntu, vous avez accès à vos applications sur la barre de gauche, avec les favoris, et l'ensemble des applications installées dans l'icone en bas à gauche.

![Installation terminée](/images/install-ubuntu-22-04-22-firstboot-complete.png)

