# Essayer Linux à l'aide d'une Clé USB "Live"

![Une clé USB Live Fedora 39](../images/live-usb.png)

L'idée, de quitter un système d'exploitation Windows ou macOS, et se lancer dans une aventure Linux peut faire peur, car changer quelques habitudes est OK, mais devoir réinstaller son ordinateur est une toute autre affaire !

Afin de pouvoir essayer une distribution Linux, il existe depuis environ 20 ans, un système dit "Live", qui permet de lancer un système d'exploitation Linux depuis une clé USB, et si on en est satisfait, l'installer.

## Quels avantages ?

Le principal avantage d'une clé Live USB est de pouvoir **essayer Linux** sans franchir le pas de l'installer définitivement. On aura une expérience, certes légèrement différente d'une vraie installation mais elle nous permettra de tester les choses suivantes :

- On peut **vérifier que le matériel de notre ordinateur est compatible**, et fonctionne correctement avec la distribution Linux de notre choix. C'est généralement le cas pour la plus grande partie du matériel, mais c'est toujours bon de vérifier.
- Cela nous permet d'**essayer différents bureaux graphiques** et choisir celui dont l'ergonomie nous convient le mieux.
- Cela nous permet de rester sur notre système d'exploitation actuel, au cas où on ne serait pas encore satisfait.

![Diverses Clés USB](../images/usb-keys.png)

## Quels inconvénients à utiliser un Live USB?

Il faut savoir qu'utiliser une distribution Live de Linux, que l'on démarre sur une clé USB n'est pas sans inconvénients, et présente des différences avec une installation classique sur le disque interne de votre ordinateur:

- **Le système Live est toujours plus lent** car il doit être lu depuis la clé USB et décompressé en mémoire vive. Votre distribution Linux sera plus rapide si elle est installée sur votre disque.
- **Les distributions Live ne stockent aucun changement sur la clé USB** : celà revient à perdre toutes les données ou les changements que vous auriez fait dès lors que vous éteignez votre ordinateur. Toutefois, Il est possible de créer une **partition persistante** contenant vos données personnelles, pour celà, consultez le chapitre [Utiliser Rufus pour créer une partition persistante](#utiliser-rufus-pour-creer-une-partition-persistante) à la fin de ce guide.
- **On ne peut pas mettre à jour un système Live USB**, à part en mode persistant, toutefois il faut faire attention car certaines mises à jour sont plus complexes et peuvent échouer.
- **Niveau sécurité**, l'utilisateur Live a tous les droits de super-utilisateur, sans mot de passe, ce qui peut poser des problèmes de sécurité pour un usage régulier.

> **Quelle langue ?** Les versions d'essai de Linux en mode Live sont très souvent localisées en **anglais** . Toutefois, certaines permettent de sélectionner la langue au démarrage, ou dans les options. Si vous modifiez la langue de votre Live USB et souhaitez la conserver à chaque démarrage, c'est possible à condition d'avoir créé votre clé USB avec une partition de **persistance**

## Bien débuter

Afin de se lancer dans l'essai de différentes distributions Linux il va nous falloir réunir les éléments suivants:

- **Une Clé USB** sur laquelle copier l'image Live de Linux : Cette clé USB doit avoir une **capacité plus grande** que l'image que nous voulons tester (généralement **4GB ou 8GB** suffisent). Cette clé USB s**era effacée durant le processus** : prenez donc garde à bien **sauvegarder** les données présentes au préalable.
- Un ordinateur sous **Windows**, **macOS** ou **Linux** afin de télécharger une image Live de Linux, et la copier sur la clé.
- Un **logiciel de copie d'image disque**, tel que [Etcher](https://etcher.balena.io/) (conseillé aux débutants), [Rufus](https://rufus.ie/fr/), ou encore [Ventoy](https://www.ventoy.net/en/download.html) (cas un peu spécial, voir la section dédiée)

> #### Attends... c'est quoi une Image ?
>
> Dans notre cas, une Image (ou Image disque), n'est pas à proprement parler une photo ou un dessin, mais plutôt un fichier qui va contenir les données exactes à copier sur notre clé USB pour pouvoir la transformer et la rendre **démarrable** (bootable) par notre ordinateur.
>
> Les fichiers image sont généralement au format **.iso** ou parfois **.img**

### Choisir et télécharger une image Live Linux

La première chose avant de copier une image Linux sur notre clé, va être de trouver une image à installer dessus. Les images live sont des fichiers généralement avec l'extension **.iso** , qui correspondent à une image disque CD ou DVD mais qui peuvent également être installées sur une clé USB.

> **Pas si vite !** Lisez bien la suite, car il ne suffit pas de copier le fichier .iso sur votre clé USB

Il existe souvent des images ISO différentes pour une même distribution Linux, ces images ont pour différences :

**Le Type de processeur supporté** : 

- x86_64/amd64 pour les processeurs 64 bits récents depuis 2008 environ.
- x86 pour les anciens processeurs 32 bits. Ils sont aussi compatibles avec les processeurs 64bits mais ne supportent pas plus de 4 Go de RAM
- arm64 pour les processeurs ARM (par exemple présents sur certains chromebooks)

**Les variantes des distributions linux**

- Souvent, **le bureau graphique** qui sera utilisé et installé par défaut (Par exemple Linux Mint propose différentes versions avec les bureaux *Cinnamon*, *Maté*, ou encore une variante appelée *LMDE*)
- Le type de **support à long terme (LTS)** : certaines versions sont conçues pour ne pas être mises à niveau durant un long moment, ces versions sont plus stables, mais n'ont pas toutes les dernières mises à jour de fonctionnalités.
- Les versions dites **nightly** ou **testing**, servent généralement à tester les nouveautés mais peuvent contenir des bugs, à éviter si celà ne vous concerne pas.

**Le type d'image d'installation :**

- **Live** : Contient le système démarrable et testable depuis la clé USB, ainsi que le programme d'installation si l'on souhaite l'installer définitivement sur l'ordinateur.
- **Installer** : contient souvent juste le programme installation définitive sans environnement d'essai
- **Netboot** : comme la version Minimale, contient juste le minimum pour installer depuis le Réseau, nécessitera une connexion internet.

> **Dans notre cas, on va choisir la version Live**, qui nous permettra de démarrer et de tester avant de pouvoir l'installer

### Quelle distribution choisir?

Sur la table suivante, vous trouverez les liens de téléchargement vers les principales distributions Linux populaires, qui proposent des images Live.

| Nom et Lien                                                  | Niveau d'utilisation                       | Prévu pour une machine                 | Bureau Graphique                              |
| ------------------------------------------------------------ | ------------------------------------------ | -------------------------------------- | --------------------------------------------- |
| [Ubuntu](https://ubuntu.com/download/desktop)                | Débutant / Intermédiaire                   | Moyenne / 4GB RAM                      | Gnome (Modifié)                               |
| [Debian](https://www.debian.org/CD/live/)                    | Intermédiaire                              | Varié : dépend du bureau graphique (*) | Gnome, KDE, LXQT, Maté, XFCE, Cinnamon        |
| [Linux Mint](https://linuxmint.com/download.php)<br/><br/>[Linux Mint Debian Edition](https://linuxmint.com/download_lmde.php) | Débutant <br/><br/>Débutant/ Intermédiaire | Moyenne / Ancienne                     | Cinnamon, Maté, XFCE                          |
| [Fedora](https://fedoraproject.org/fr/)                      | Intermédiaire                              | Varié : dépend du bureau graphique (*) | Gnome, KDE, Cinnamon, XFCE, LXDE, LXQT, Maté  |
| [Manjaro](https://manjaro.org/download/)                     | Intermédiaire/Avancé                       | Varié : dépend du bureau graphique (*) | KDE, XFCE, Gnome, Budgie, Cinnamon, Maté, ... |

> (*) Les principaux bureaux graphiques de Linux et leurs requis en performance:
>
> - **Gourmands et pleins de fonctionnalités** : GNOME et KDE nécessitent au minimum 3GB de mémoire RAM (2GB est trop limite), 8GB pour une utilisation multimédia avancée.
> - **Optimisés pour le plus grand nombre** : Cinnamon et XFCE nécessitent généralement que 2GB de mémoire RAM mais vont être plus à l'aise avec 4GB de RAM
> - **Conçus pour faire durer du matériel ancien** : LXQT (ou LXDE), et Maté peuvent tourner sur des ordinateurs équipés de seulemen 1GB de mémoire RAM



<u>Conseils pour les débutants :</u> 

1) Une distribution très populaire pour ceux qui veulent débuter sous Linux, est **Linux Mint** dans sa version **Cinnamon**. Sa simplicité d'utilisation du bureau est proche de celle d'un PC Windows, et la documentation ainsi que la communauté francophone en font un choix particulièrement attrayant.

2. Si votre machine est plus ancienne, vous pouvez également tester **Linux Mint** dans sa version avec le bureau **MATE**.

3. Si votre machine est un peu plus puissante, récente ou nécessite une meilleure compatibilité, nous vous conseillons **Ubuntu**, car elle est une des distributions les plus populaires.

> Dans la plupart des cas, même les distributions dites "Gourmandes" le sont moins qu'un Windows 10, ou Windows 11, qui nécessitent au moins 4GB

---

## Copier l'image linux sur une clé USB

Afin d'installer notre image linux sur une clé USB, nous allons utiliser un petit logiciel qui va **lire le fichier ISO** et, de manière correcte et sécurisée, **transférer ses données sur la clé USB**. Il existe plusieurs utilitaires permettant de réaliser cette opération. Nous verrons ici les méthodes suivantes:

- Etcher (recommandé pour sa simplicité)
- Ventoy (un peu plus complexe, mais simplifie l'ajout d'images ISO par la suite)

![Etcher Télécharger](../images/etcher-download.png)

### Utiliser Etcher

Etcher est un programme très simple qui a pour but de copier le contenu d'une image ISO sur une clé USB. On peut le télécharger à l'adresse suivante : https://etcher.balena.io/#download-etcher. On peut y trouver les téléchargements pour Windows, Linux ou macOS.

> **Note :** la version linux de Etcher est une application de type **.appimage** qui nécessite qu'on autorise son exécution en tant que programme. Pour ce faire, cocher la case correspondante dans les propriétés du fichier.

Une fois téléchargé, installé et lancé, on arrive sur l'écran suivant :

![Etcher](../images/etcher-screen.png)

La première étape est de cliquer sur **Flash from File** : Ce bouton ouvre une fenêtre de sélection de fichier qui va nous permettre de sélectionner le fichier ISO que l'on vient de télécharger, et que l'on souhaite flasher (copier) sur notre clé USB.

![Etcher Sélection de fichier](../images/etcher-iso-selection.png)

Ensuite, nous allons insérer notre clé USB dans un port USB libre de notre ordinateur. Puis, cliquer sur le bouton **Select Target** afin de sélectionner notre clé USB.

> **IMPORTANT** : le contenu de la clé USB va être supprimé, si son contenu est important, veillez à ce qu'il ne soit pas perdu, par exemple en copiant les fichiers sur votre ordinateur.

![](../images/etcher-select-target-button.png)

En cliquant sur le bouton, nous pouvons sélectionner notre clé USB dans la liste, en cochant sa case, puis cliquer sur **Select 1** (car nous en avons sélectionné une seule.)

![](../images/etcher-select-target.png)

Enfin, si tout est en ordre pour être écrit sur la clé USB, nous pouvons cliquer sur **Flash!** afin de lancer l'écriture de l'image ISO sur la clé USB.

![](../images/etcher-click-flash.png)

Maintenant, l'image est en cours d'écriture sur la clé USB, il ne nous reste plus qu'à attendre que le processus soit terminé.

![](../images/etcher-flash-progress.png)

Une fois terminé, Etcher nous affiche un rapport si l'écriture sur la clé USB s'est bien déroulée. Nous pouvons maintenant retirer la Clé USB de son port.

![](../images/etcher-flash-complete.png)

---

## Démarrer l'ordinateur sur la clé USB

Afin de démarrer sur une clé USB, il va nous falloir redémarrer notre ordinateur. La meilleure manière (celle qui nous laisse du temps), est **d'éteindre** d'abord l'ordinateur, **insérer la clé USB** dans un port, puis enfin **rallumer** l'ordinateur.

Afin de démarrer sur la clé USB, il va nous falloir accéder au menu de démarrage : pour ce faire il faut laisser appuyé sur une touche, avant le démarrage du système d'exploitation. 

![](/images/uefi-boot-menu-key.png)

Attention c'est une opération à réaliser durant les premières secondes du démarrage de l'ordinateur, et il est facile de rater la fenêtre d'opportunité pour appuyer. La touche à utiliser change en fonction du type de machine et sa marque.

| Marque  | Touche                |
| ------- | --------------------- |
| Acer    | Suppr ou F2           |
| ASUS    | F9 ou Echap           |
| Dell    | F12                   |
| HP      | F10 ou Echap          |
| Lenovo  | F1 ou F2              |
| Samsung | F2                    |
| mac     | Option(Alt)           |
| Autres  | Echap (Généralement*) |

(*) L'information est généralement présente dans la documentation de votre ordinateur

> **Aller plus loin** (en anglais) : https://www.wikihow.com/Get-to-the-Boot-Menu-on-Windows

Une fois dans le menu, nous pouvons enfin sélectionner notre clé USB pour démarrer dessus :

![UEFI Boot Menu](../images/uefi-boot-menu.png)

> **Note:** sur un mac, en utilisant la touche option (alt), on obtient le même menu, plus graphique, suivant.
>
> ![](../images/uefi-boot-menu-mac.png) 

Une fois sélectionnée et démarrée, Ventoy donne la main au démarrage de l'image Live. La plupart du temps, nous avons un menu supplémentaire qui nous permet de démarrer en mode Live ou bien en mode installation : 

![](../images/uefi-boot-linuxmint.png)

# Pour aller plus loin... 

Il existe également d'autres outils permettant de créer une Clé USB Live pour Linux. Cette section présente ces outils et certaines de leurs fonctionnalités qui pourraient vous intéresser.

## Utiliser Rufus pour créer une partition persistante

[Rufus](https://rufus.ie) est un outil particulièrement pratique sous Windows, permettant de copier une image disque sur une clé USB et qui présente des options très intéressantes, notamment celle de créer une **partition persistante** sur notre clé USB. Une partition persistante permet (entre autres) de :

- Enregistrer des modifications et des préférences (thèmes, mots de passe Wi-Fi)
- Stocker des fichiers pour les retrouver entre chaque redémarrage
- Ajouter d'autres utilisateurs
- Installer certaines applications, et les mettre à jour

Après avoir téléchargé Rufus et lancé le programme on se retrouve avec la fenêtre suivante :

![](../images/rufus.png)

Dans la section **Drive Properties** :

- On Sélectionne notre clé USB dans la liste déroulante *Device*
- Puis on vérifie que l'on a l'option *Boot Selection* définie sur *Disk or ISO image (please select)*
- Ensuite, on peut cliquer sur le bouton **SELECT**, et utiliser la fenêtre qui s'ouvre pour sélectionner notre fichier .iso, puis valider en cliquant sur le bouton **Open** 

![](../images/rufus-explore-iso.png)

Ensuite, nous devons ajuster les paramètres suivants :

- **Persistent partition size** : ce paramètre permet de choisir quel espace disque on souhaite allouer aux données persistantes. Par défaut il est désactivé. Si l'on souhaite l'activer, il est intéressant d'utiliser la taille maximum.
- **Partition Scheme / Target System** : tout va dépendre de l'âge de votre ordinateur. Généralement, depuis 2008, les ordinateurs utilisent le système de démarrage de UEFI, tandis qu'auparavant ils utilisaient le système MBR. Ainsi, on choisira une option GPT + UEFI (non CSM) pour une clé USB devan démarrer sur un système récent. et une option MBR /BIOS pour un ordinateur ancien.



![](../images/rufus-configure-persistent.png)

Enfin, on peut régler les derniers paramètres dans la section **Format Options**, il est généralement conseillé de laisser les valeurs par défaut.

![](../images/rufus-format-options.png)

Puis, une fois réglé, on peut cliquer sur le bouton **START** en bas de la fenêtre pour démarrer l'écriture sur la clé USB. Le processus d'écriture peut être plus long si vous avez sélectionné une partition persistante.

## Installer Ventoy sur une clé USB

**Ventoy** est un outil très intéressant dès lors que nous souhaitons **essayer plusieurs distributions linux depuis la même clé USB**, sans avoir à flasher une image à chaque fois à l'aide de Etcher. Ce que Ventoy propose, c'est un menu de démarrage de l'ordinateur, permettant de **sélectionner une image live de linux, parmi une liste**. 

Là où le système ventoy est intéressant, c'est que les images ISO ont simplement à être copiées via l'explorateur de fichiers, et que l'opération d'installation de ventoy n'est réalisée qu'une seule fois.

L'installation est légèrement plus complexe qu'avec Etcher, mais l'utilisation à terme est bien plus simple.

### Téléchargement et Installation de Ventoy

Ventoy peut être installé sur une clé usb en téléchargeant son **programme d'installation** depuis le site : https://www.ventoy.net/en/download.html

Il existe des installeurs pour **Windows** et **Linux**, (nous ne nous intéresserons pas au liveCD). L'outil d'installation est relativement le même entre Windows et Linux, nous ne présenterons que la version windows.

En ayant téléchargé et extrait le fichier ZIP, nous pouvons lancer le programme **Ventoy2Disk.exe** situé dans le dossier où nous avons extrait les fichiers. Nous allons sélectionner dans la liste notre clé USB. Si rien n'est installé encore, nous devrions voir un espace vide dans la section "Ventoy In Device"

![](https://www.ventoy.net/static/img/ventoy2disk_en.png)

En cliquant sur "Install", nous allons remplacer entièrement le contenu de la clé par un système Ventoy, toutes les données vont être supprimées.

Une fois installé, on peut voir la version de ventoy installée sur la clé USB:

![img](https://www.ventoy.net/static/img/ventoy2disk2_en.png)

Le disque Ventoy est maintenant visible dans l'explorateur Windows

![](/images/ventoy-windows-drive.png)

Nous pouvons maintenant copier les fichiers ISO qui nous intéressent sur la clé USB.

>  Pour plus d'informations, consulter la [documentation officielle (en anglais)](https://www.ventoy.net/en/doc_start.html)

### Démarrer sur un système Ventoy

Tout comme pour un démarrage sur une clé USB, ventoy démarre via le boot UEFI de l'ordinateur (en utilisant soit le menu de démarrage, soit si la clé usb est en haute priorité de boot dans le BIOS). 

Une liste d'images apparait, correspondant aux fichiers iso placés sur la clé USB dans le disque nommé VENTOY.

Afin de démarrer sur une entrée, il suffit de **sélectionner une entrée** et avec les **flèches directionnelles** du clavier, et d'appuyer sur **Entrée**.

![Démarrage de ventoy](https://www.ventoy.net/static/img/screen/screen_uefi.png)

