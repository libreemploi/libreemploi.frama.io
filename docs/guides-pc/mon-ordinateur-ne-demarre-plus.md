# Mon ordinateur ne démarre plus !

> **Pas de panique ! Avoir des problèmes de démarrage d'un ordinateur est relativement courant et les problèmes graves sont rares (mais ça arrive tout de même). Ce que l'on appelle démarrage de l'ordinateur est constitué de nombreuses phases et différents problèmes peuvent survenir à différents moments.**

![An HP-branded Laptop PC, that is located in the Rotterdammer neighbourhood of 110-Morgen, Hillegersberg-Schiebroek - Donald Trung Quoc Don (Chữ Hán: 徵國單) - Wikimedia Commons - © CC BY-SA 4.0 International.](https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/HP_Stream_Laptop_PC_Model14-ax010nd%2C_Hillegersberg%2C_Rotterdam_%282021%29_14.jpg/800px-HP_Stream_Laptop_PC_Model14-ax010nd%2C_Hillegersberg%2C_Rotterdam_%282021%29_14.jpg)

Sur cette page, vous trouverez différents cas de figure qui peuvent se présenter, et pour chacun d'entre eux, une **méthode de diagnostic** ainsi qu'une ou plusieurs **solutions** pour tenter de faire fonctionner votre machine à nouveau.

<u>Pour chacun des cas, on va passer en revue un certain nombre de vérifications:</u>

1) Essayer de déterminer ce qui ne marche pas
2) Identifier le(s) problème(s)
3) Tenter de Corriger le(s) problème(s) nous même, si possible.

> L'objectif premier, quand on est dans ce cas là est de travailler à **trouver le point bloquant du démarrage** afin de permettre à l'ordinateur de démarrer à nouveau. Pour ce faire, on va procéder par étapes. Cet article présente les problèmes un par un, chronologiquement, si vous n'êtes pas concerné par un paragraphe, vous pouvez passer au suivant.

Certains **indices** pourront vous aiguiller tout au long de ce guide, il est important de noter:

- Si votre ordinateur a reçu un **choc**, une **chute** ou a été **déplacé** (déménagement)
- Si du **liquide a été renversé** dessus.
- Si le problème survient après une **coupure de courant**, une **surtension** ou une **baisse de tension** (par exemple durant un orage).
- Si l'ordinateur s'est **éteint brusquement** pendant que l'on s'en servait.

## Avant toute chose, un peu de sécurité

![](/images/kilian-seiler-phone-repair-unsplash-cropped.png)

Investiguer un ordinateur en panne passe souvent par un peu d'exploration dans ses entrailles. Nos machines sont alimentées par de l'électricité, et même si une seule partie est alimentée en 230 volts, il est important d'agir avec sécurité, autant pour vous que pour votre ordinateur.

Nous autres, humains, avons la faculté d'accumuler de **l'électricité statique**, et celle-ci a la fâcheuse tendance à vouloir s'échapper vers les appareils électroniques, leur causant **parfois de gros dommages**.

<u>Il faut donc penser à, lors de nos manipulations :</u>

- Toujours **éteindre, puis débrancher l'ordinateur** lorsque nous allons enlever ou remettre des pièces. Sur un ordinateur fixe celà peut se faire en éteignant le bouton (0/1) du bloc d'alimentation généralement situé à l'arrière, en débranchant le câble d'alimentation ou même en coupant l'alimentation de la multi-prise, si celui-ci est branché sur une multi prise à interrupteur.

- Déconnecter la batterie du portable** lorsque nous allons l'ouvrir, afin de décharger l'électricité résiduelle. Parfois, ceci est uniquement possible après avoir retiré le capot arrière.
- Toujours **Attendre environ 30 secondes** après avoir déconnecté l'électricité, pour s'assurer que les composants sont déchargés. Souvent, une diode présente sur la carte mère va alors s'éteindre.
- Si possible, [**Porter des gants** de type ESD](https://store.ifixit.fr/products/esd-gloves) **fins**, ceux ci sont **anti électrostatiques** et permettent d'éviter toute décharge qui endommagerait le matériel.
- Régulièrement, **se décharger de notre électricité statique** : cela est souvent possible en **touchant un élément de la maison en métal** (radiateur en fonte, fenêtre).

Bien, maintenant qu'on est au courant de ces mesures de sécurité pour nous et notre matériel, on va pouvoir commencer à investiguer.

## 1- Aucun signe de vie !

C'est souvent le problème le plus embêtant, lorsque rien ne se passe. On appuie sur le bouton ON/OFF et l'ordinateur ne réagit pas. Généralement on peut entendre les ventilateurs, ou les disques durs se mettre en marche.

Lorsqu'on est dans cet état, c'est très souvent un **défaut d'alimentation**, qui fait que l'ordinateur n'est pas alimenté, ou bien que l'alimentation n'arrive pas à la carte mère.

<u>On va vérifier les choses suivantes :</u>

**Dans le cas d'un Ordinateur Fixe**

1) Est-ce que l'ordinateur est bien branché? Oui, je sais ça peut paraître évident, mais parfois, un pied qui traîne, qui éteint une multiprise, ou bien débranche à moitié le câble d'alimentation. Vérifions donc que notre ordinateur est bien branché, que la prise ou la multiprise est bien alimentée, que le disjoncteur n'a pas sauté (parfois, le tableau électrique ne saute pas en entier)
2) Il arrive parfois que par mégarde le bouton On/Off situé [sous la prise d'alimentation](https://fr.wikipedia.org/wiki/Bloc_d%27alimentation#/media/Fichier:PSU-Open1.jpg) soit remis sur 0. Il suffit alors de le remettre sur 1 pour remettre l'alimentation. Certaines alimentations ont une diode lumineuse qui s'allume lorsqu'elles sont correctement alimentées, mais pas encore en marche.
3) Il peut arriver qu'une odeur de brûlé provienne de l'alimentation, c'est notamment possible s'il y a eu une coupure électrique, une surtension, ou du liquide a coulé. Dans ce cas là, l'alimentation peut avoir eu un court-circuit, la mettant Hors-Service : Il faudra dans ce cas la remplacer. Plus rarement (mais également possible), d'autres composants de l'ordinateur, comme la carte mère, peuvent avoir été atteints, dans ce cas il faudra aussi les remplacer.
4) Si l'ordinateur a été manipulé, il est possible que certaines connexions d'alimentation à la carte mère se soient détachées : par exemple le [connecteur ATX](https://commons.wikimedia.org/wiki/File:2023_Z%C5%82%C4%85cze_zasilania_ATX.jpg) présent sur la carte mère a peut être sa [prise ATX](https://commons.wikimedia.org/wiki/File:ATX_PS_ATX_connector.jpg) débranchée : ce connecteur sert à alimenter la carte mère en électricité.
5) Il arrive également que le bouton d'allumage de l'ordinateur ne soit plus correctement relié à la carte mère. Ce bouton, situé sur le boîtier, est relié à la carte mère via 2 fils torsadés ensemble et reliés à des connecteurs via un **connecteur jumper**. Ces connecteurs sont généralement appelés [front panel module (anglais)](https://pcguide101.com/motherboard/what-are-front-panel-connectors/) et permettent de connecter différents éléments présents sur le boîtier, comme les boutons d'allumage, reset, la diode d'activité de disque, etc. Il convient de vérifier que la torsade de fils soit correctement reliée à la carte mère.

**Dans le cas d'un ordinateur portable**

1) Il est possible que la batterie soit tellement déchargée que même brancher le chargeur ne permet pas de démarrer tout de suite (et dans ce cas là il faut recharger quelques minutes avant de démarrer). 
2) Il est possible que la **Batterie soit HS et que l'alimentation serve uniquement à alimenter la batterie**. Sur des ordinateurs portables un peu anciens, la batterie est amovible, et l'ordinateur peut alors démarrer sans batterie, via son chargeur. Si l'ordinateur est plus récent et sa batterie non amovible, c'est une affaire plus compliquée.
3) Il est également possible que le **Chargeur soit HS**. Il peut être difficile de s'en assurer, mais généralement lorsque le chargeur est branché et alimenté, une diode s'allume, soit sur le chargeur, soit l'ordinateur, afin d'indiquer le bon fonctionnement de la charge.
4) Le cas le plus problématique est une **surcharge électrique qui aurait endommagé la carte mère** (ou d'autres éléments internes) : cela peut arriver si l'ordinateur portable subit un choc durant son fonctionnement, ou reçoit du liquide. Dans ce cas là il devient compliqué de réparer soi-même. Des personnes compétentes en électronique peuvent vous aider à diagnostiquer ce problème, mais généralement il faut penser à remplacer la carte mère de l'ordinateur.

## 2 - L'ordinateur s'allume mais reste sur un écran noir (ou éteint)

Un ordinateur qui démarre mais qui reste bloqué sur un écran noir, ou un moniteur qui ne s'allume pas présente souvent les caractéristiques d'un **défaut de matériel** interne. Toutefois, il peut également s'agir également d'un **problème d'écran**.

### Cas #1 (Ordinateur Fixe) : Pas d'image mais activité du disque

Il arrive que ce ne soit qu'un **problème d'écran débranché, ou mal branché** : dans ce cas là, on peut généralement constater que l'ordinateur démarre, n'affiche rien, mais que la diode d'activité du disque montre qu'il se passe des choses. 

![Diode d'activité du disque](https://upload.wikimedia.org/wikipedia/commons/3/3e/Laptop_Computer_HDD_Disk_Activity_LED.gif)

Dans ce cas-ci, il va nous falloir **trouver pourquoi on n'a plus d'image**. Le problème peut avoir plusieurs causes :

1) **Un câble vidéo débranché, ou un port vidéo défectueux**. Les câbles vidéo sont généralement branchés à l'arrière de l'ordinateur, soit [sur la carte graphique]() soit sur le [panel arrière de la carte mère]() si des ports vidéo sont présents : essayer de rebrancher, ou de brancher sur un port différent.
2) **Un défaut d'écran** : vérifier que celui-ci est bien alimenté et fonctionne correctement. Certains écrans peuvent se bloquer (à cause de bugs), dans ce cas, on peut forcer l'écran à s'éteindre complètement, en débranchant tous ses câbles (Vidéo et Alimentation électrique), en attendant environ 1 minute que tous les composants électroniques se déchargent, puis en rebranchant à l'identique.
3) **Un câble vidéo débranché sur l'écran, ou une mauvaise source vidéo sélectionnée** : vérifier que le câble vidéo est bien branché à votre écran et que la source (input) sélectionnée sur l'écran correspond à l'entrée dans laquelle est branchée le câble.

> En fonction de l'âge de votre ordinateur, le câble vidéo peut être [VGA](https://commons.wikimedia.org/wiki/File:VGA-Buchse%2BStecker.JPG), [DVI](https://commons.wikimedia.org/wiki/File:DVI-Buchse%2BStecker.JPG), [HDMI](https://commons.wikimedia.org/wiki/File:HDMI_Connector_Types.png), ou [DisplayPort](https://fr.wikipedia.org/wiki/DisplayPort)

Dans le cas d'une image qui n'apparait toujours pas, les trois causes les plus probables sont : **un câble vidéo défectueux**, **un port vidéo défectueux** (soit sur la carte graphique/carte mère, soit sur l'écran), ou encore un **moniteur défectueux**. 

> Le fait d'avoir une **carte graphique défectueuse** est **moins probable**, car les cartes graphiques défectueuses empêchent souvent le démarrage de l'ordinateur : on ne constaterait donc pas d'activité de la diode du disque.

<u>Afin de tenter de résoudre le problème, voici plusieurs pistes à essayer successivement :</u>

1) Tenter de brancher ce même câble vidéo sur d'autres ports de la carte graphique, ou de l'écran. Si rien n'y fait, il est possible que le câble soit défectueux.
2) Tenter d'utiliser un autre câble vidéo de même type sur les différents ports de la carte graphique, et/ou de l'écran. Si le problème persiste, il est possible que le problème ne vienne pas du câble, mais de l'écran.
3) Tenter de brancher un autre ordinateur / appareil vidéo sur l'écran à l'aide de son câble, ou même d'un autre câble. Tenter de brancher l'ordinateur sur un autre écran.

En déduisant ce qui marche et ce qui ne marche pas, on peut ensuite remplacer les câbles incriminés, ou bien chercher à réparer l'écran si c'est celui-ci qui est en panne. 

### Cas #1 (Ordinateur Portable) : Pas d'image mais activité du disque

Si le problème arrive sur un ordinateur portable, c'est déjà un peu plus embêtant. Les écrans d'ordinateurs portables ne sont pas connectés à la carte mère via un câble, mais généralement via un [connecteur spécifique](https://fr.ifixit.com/Tutoriel/Dell+XPS+15+9570+Display+Replacement/130788?lang=en#s256614).

![](../images/laptop-screen-connector-motherboard.png)

Si votre ordinateur portable a **reçu un choc** ou a été malmené, **il est possible que ce connecteur se soit détaché**. Dans ce cas, il doit être possible de le replacer en suivant un guide du site [I Fix It](https://fr.ifixit.com/Device/PC_Laptop).

Toutefois, il est également possible que l'écran en lui-même soit endommagé : Remplacer un écran d'ordinateur portable n'est pas une chose facile, mais le site [I Fix It](https://fr.ifixit.com/Device/PC_Laptop) propose de nombreux guides pour différents modèles que vous pouvez suivre. Il vous faudra pour cela, trouver un écran de remplacement, et vous armer de patience et d'un peu de courage pour pouvoir ouvrir votre ordinater et procéder au remplacement.

### Cas #2 : Quelques signes de vie, mais pas d'écran, ni d'activité de disque.

Il est possible que votre ordinateur démarre mais se bloque au tout début de sa [phase de démarrage (POST)](demarrage.md) . Si rien ne s'affiche, il s'agit probablement d'un problème majeur empêchant le firmware de s'initialiser. Il est même possible que parfois l'ordinateur émette de longs bips, signalant ce problème.

> En fonction de la marque de votre ordinateur, plus ou moins d'informations sont disponibles, certaines cartes mères ont même un [affichage indiquant un code d'erreur](https://www.asus.com/support/FAQ/1043948) permettant d'identifier le problème.

La plupart du temps, nous allons devoir investiguer "en aveugle" afin de trouver si un élément amovible bloque le démarrage de l'ordinateur.

1. On commence par les **précautions d'usage**  : éteindre l'alimentation, débrancher la prise électrique, et attendre 30 secondes, la décharge des composants.

2. La première étape consiste à **débrancher tout périphérique externe qui n'est pas utile au démarrage**. On ne garde que le clavier et la souris. Si l'ordinateur démarre, on teste de rebrancher un à un jusqu'à identifier le périphérique qui pose problème.

   > Si même avec le clavier et souris débranché, rien ne se passe, on peut rebrancher le clavier et passer à l'étape suivante.

3. Si l'ordinateur ne démarre toujours pas avec juste le clavier et la souris, on va essayer de chercher **une barette de RAM défectueuse**. Pour ce faire, nous allons **ouvrir l'unité centrale de l'ordinateur** pour accéder à la **carte mère** : Certains ordinateurs de bureau 2 barrettes de RAM installées sur cette dernière : en retirant l'une ou l'autre, et en testant dans d'autres ports mémoire (il y en a souvent 4 sur une carte mère), on peut tenter de déterminer si c'est une barrette de RAM défectueuse qui empêche le démarrage. 

   > Attention toutefois : l'ordinateur ne démarre pas si aucune barrette de RAM n'est installée.

4. On peut également **vérifier la carte graphique**, s'il y en a une qui est présente : d'abord vérifier que ses connecteurs d'alimentation sont bien branchés, puis vérifier que la carte est toujours bien connectée au port PCI Express (on peut tenter de l'enlever et la replacer correctement, attention à bien débrancher les câbles d'alimentation et vidéo lors de la manipulation). Enfin, si rien ne fonctionne, on va essayer de totalement retirer la carte graphique. Une fois retirée, il nous faudra brancher notre écran sur la sortie vidéo de la carte mère, située sur le panneau arrière.

> **Pour aller plus loin :** Pour plein d'autres raisons, certaines cartes mères peuvent émettre plusieurs bips, le site **vulgarisation informatique** a fait un bon [résumé des pannes](https://www.vulgarisation-informatique.com/bips-bios.php) liées à ces bips.

## L'ordinateur s'allume et reste bloqué sur un écran

La plupart du temps, l'ordinateur peut se bloquer durant deux grandes phases (voir notre fiche sur le [démarrage de l'ordinateur](demarrage.md)) :

- La phase de démarrage du Firmware
- Le chargement du Système d'exploitation

### L'ordinateur démarre mais affiche une erreur durant la phase de POST (firmware)

L'affichage d'un message d'erreur durant la phase de démarrage de l'ordinateur est souvent source de stress, car elle empêche d'accéder au système d'exploitation et fait souvent penser à un problème matériel. Toutefois, ces messages sont souvent explicites et permettent d'identifier rapidement le problème. En voici quelques uns que vous pourriez rencontrer.

> Note : cette section est inspirée de [cet article de SilentPC.com](https://silentpc.com/articles/bios-problems-and-solutions)

#### BIOS ERROR : Failed to Overclock

 Cette erreur arrive lorsque le firmware essaie d'appliquer des paramètres d'overclocking (une méthode pour faire tourner le processeur ou la RAM à des vitesses supérieures à celles prévues), toutefois, il est possible que l'erreur soit liée à autre chose:

- **Si vous avez déplacé votre ordinateur (fixe)**, il est possible que le **ventilateur du processeur se soit détaché et que le processeur surchauffe**. Pour ce faire il est important de vérifier que le ventilateur soit toujours bien attaché, et éventuellement le reconnecter. En fonction du modèle de processeur, les ventilateurs sont différents, et la manière dont ils s'attachent à la carte mère peut varier. Voici [un exemple sur le site I Fix It](https://fr.ifixit.com/Tutoriel/Remplacement+du+processeur+Gigabyte+A320M-S2H/140613#s280627)
- **Un problème de Pile de BIOS** : Cette pile au lithium, présente sur beaucoup d'ordinateurs anciens, permet de garder en mémoire les paramètres du firmware. Cette pile se recharge lorsque l'ordinateur est sous tension, mais parfois elle devient défaillante et ne se recharge plus. Ou alors elle se décharge lorsque l'ordinateur est mis hors tension depuis longtemps. Afin de garder les paramètres en mémoire, il nous faut une pile qui soit utilisable, sous peine de voir certains paramètres être constamment réinitialisés. Voici un [guide pour remplacer la pile CMOS](https://www.howtogeek.com/893809/how-to-test-and-replace-your-cmos-battery/) (en anglais)
- **Un problème électrique** : Si votre alimentation est défaillante ou a reçu une surcharge électrique, il est possible que les voltages appliqués aux composants soient incorrects, générant une trop forte chaleur, ou un voltage insuffisant. Dans ce cas, tenter d'abord de réinitialiser les paramètres du BIOS, et vérifier si le problème persiste.
- **Un réel problème d'overclocking** : Si vous avez joué avec les paramètres de voltage, multiplicateurs et fréquences du processeur ou de la RAM dans les paramètres du BIOS, vous pouvez tenter de réinitialiser le BIOS à ses valeurs d'origine.

#### BIOS Error : Failed Device

Cette erreur apparaît lorsqu'un périphérique connecté à l'ordinateur a une défaillance, en général matérielle. Pour éviter d'endommager la carte mère, il est important de ne pas aller plus loin et d'éteindre l'ordinateur.

Dans un premier temps on va débrancher tout périphérique USB et redémarrer, si le problème persiste, sur un PC fixe, on va également essayer de débrancher :

- les disques durs SATA
- les ports USB en façade, connectés sur la carte mère

Puis, lorsque on arrive à ne plus avoir l'erreur, on va **redémarrer** plusieurs fois et à chaque fois, **rebrancher un périphérique, jusqu'à identifier le coupable**.

Une fois le coupable identifié, il sera intéressant de trouver pourquoi il ne marche plus et éventuellement le réparer, ou le remplacer.

### BIOS Error : CPU Fan Error

Cette erreur peut arriver lorsque la carte mère détecte un ventilateur en interne qui tourne à une vitesse anormalement basse : la carte mère est capable de moduler et de lire la vitesse de rotation des ventilateurs des ordinateurs, afin de pouvoir rafraîchir les composants lorsqu'ils chauffent, mais éviter du bruit inutile lorsque l'ordinateur est au repos.

Les ventilateurs d'un ordinateur sont des pièces fragiles qui sont assez susceptibles de s'encrasser avec de la poussière. Cette dernière peut amener les ventilateurs à tourner lentement et déclencher cette erreur.

Il est également possible que lors du transport de l'ordinateur ou d'un changement de pièce, un ventilateur se soit déconnecté de la carte mère.

<u>Afin de résoudre le problème :</u>

- En préventif, s'occuper régulièrement de son ordinateur et nettoyer la poussière.
- Vérifier et nettoyer la poussière encrassée sur les éventuels ventilateurs. 
- Si un ventilateur a été endommagé par trop de poussière (celui ci fait du bruit et est désaxé), changer le ventilateur. Attention, les ventilateurs de carte graphiques ne sont pas toujours changeables !

#### Comment Réinitialiser les paramètres du BIOS ?

Afin de réinitialiser les paramètres du BIOS, il va nous falloir entrer dans le [demarrage.md#acceder-a-la-configuration-du-biosuefi-setup] de notre firmware.

Dans l'outil, en fonction des constructeurs, il nous faudra trouver une option permettant de réinitialiser les paramètres. Cette option peut s'appeler:

- Load Setup Defaults
- Load Optimized Defaults
- Load BIOS Defaults

> Pour une liste un peu plus exhaustive des messages d'erreur visibles pendant la phase de POST, le site SOSPC20 les résume ici: https://www.sospc20.com/depannage_informatique/messages-erreur-pc.php

### L'ordinateur démarre mais affiche l'erreur : `No bootable devices found`

Cette erreur indique que le firmware n'arrive pas à démarrer sur un disque dur, un SSD ou quelque autre disque que ce soit. Si vous n'avez pas déplacé votre ordinateur ou ouvert pour changer une pièce, il est possible que votre disque dur, ou SSD soit défaillant.

Plusieurs cas sont possibles :

- Si vous avez déplacé votre ordinateur fixe, ou ouvert ouvert pour changer une pièce, il est possible que la connexion entre le disque et la carte mère ne soit plus assurée. Si votre disque est SATA, il convient de vérifier si le cable SATA reliant le disque à la carte mère est toujours bien branché, et que le disque est correctement relié à l'alimentation du PC. Un disque seulement connecté au port SATA ne sera pas détecté s'il n'est pas alimenté.
- **Une panne matérielle**, fait que le disque n'est plus détecté : c'est la panne la plus embêtante, car on peut généralement considérer que le disque est mort.
- **Une panne logicielle**, fait que la table des partitions ou les secteurs de démarrage du disque ont été endommagés. Il est possible, mais complexe, de récupérer les données en restaurant le secteur de démarrage : https://www.diskpart.com/fr/articles/reparer-secteur-de-boot.html
- **Une erreur de manipulation** lors de l'installation de Linux, ou un redémarrage précoce (avant la fin de l'installation) a empêché le chargeur de démarrage GRUB de s'installer correctement. En général, la manière la plus facile est de réinstaller Linux.

## L'ordinateur démarre mais reste bloqué sur le chargement du système d'exploitation

Il peut arriver que l'ordinateur se bloque assez tard dans le démarrage, pendant le démarrage du système d'exploitation. En fonction du système (windows, linux ou macOS), l'écran de démarrage peut prendre plusieurs formes :

1) Pour windows, un cercle de petits points tournent sur eux mêmes vers le milieu-bas de l'écran.
2) Les écrans de chargement de linux sont très variés, ils affichent souvent le logo de la distribution linux installée et un indicateur de chargement (petit cercle qui tourne)
3) macOS démarre le plus souvent en affichant le logo Apple, et une barre de progression.

> Note : Certains PC modernes affichent leur logo constructeur à la fois avant le chargement du système d'exploitation, et pendant le chargement de ce dernier. Le témoin de chargement apparaît à un certain moment, indiquant qu'il a commencé à charger. 

La plupart du temps, un système d'exploitation qui bloque a pour raison un **problème logiciel**. Mais il peut arriver qu'un périphérique ou un composant endommagé bloque le chargement logiciel.

Il peut arriver également que le système se débloque au bout de **plusieurs minutes**, pour ce faire, on va vérifier si ceci arrive seulement une fois, ou persiste. Si au bout de plusieurs dizaines de minutes l'ordinateur est toujours en chargement on va considérer qu'il est bloqué et le forcer à s'éteindre : pour ce faire, on laisse appuyé sur son bouton d'allumage plusieurs secondes, et ce jusqu'à ce qu'il s'éteigne.

Afin de chercher l'origine du problème, on va d'abord se poser les questions suivantes:

1) Est-ce que le problème est survenu à la suite d'une mise à jour?
2) Est-ce que le problème est survenu à la suite du changement d'un composant ? ou l'ajout d'un nouveau périphérique ? On peut essayer de redémarrer en retirant le nouveau composant (et éventuellement remettant l'ancien) 

#### Windows 10/11 : Utiliser la réparation au démarrage

Si votre ordinateur bloque au démarrage de Windows 10, vous pouvez l'éteindre (appuyer 10s sur le bouton ON) puis le rallumer. Si jamais deux démarrages successifs bloquent, le troisième démarrage va lancer l'outil de réparation du système :

![](https://support.content.office.net/fr-fr/media/72fa96b3-adab-4261-a78e-4ef47eaa96ea.png)

Dans ce mode, vous pouvez accéder à différents outils afin de tenter de résoudre les problèmes de démarrage, désinstaller des options, ou encore utiliser la restauration système :

![](https://support.content.office.net/fr-fr/media/0d827ef0-a433-4765-a366-f0b39109f35e.png)

#### Windows 10/11 : Utiliser la restauration système

Afin de diagnostiquer son démarrage et de corriger d'éventuels problèmes, Microsoft a mis à disposition un outil de Restauration du Système :

https://support.microsoft.com/fr-fr/windows/utiliser-l-outil-restauration-du-syst%C3%A8me-a5ae3ed9-07c4-fd56-45ee-096777ecd14e

Cet outil permet de remettre le système à un état antérieur où il a été sauvegardé, si les points de restauration système sont activés sur la machine. Il peut être opportun de les activer lorsque le système est en bon état de marche, afin de pouvoir avoir un point de retour à utiliser en cas de pépin. 

**Si Windows ne démarre pas, l'outil de restauration système se trouve dans les options avancées de l'outil de récupération**

#### Utiliser une Clé USB live Linux pour diagnostiquer, sans toucher au système

> Attention, cette méthode ne fonctionne pour un Windows 10/11 que si le chiffrement **BitLocker** est **désactivé** sur votre machine. Il existe néanmoins [une méthode (anglais)](https://www.maartendekeizer.nl/blog/detail/recover-files-from-bitlocker-with-linux-live-usb) ou encore [une autre sur la distribution Kali Linux](https://www.kali.org/tools/dislocker/) toutes les deux complexes, afin de pouvoir récupérer les données sur un disque chiffré par BitLocker.

#### Le pire des cas : devoir réinstaller

Ça peut arriver, et ce n'est pas vraiment le plus grave de nos problèmes, mais réinstaller notre système d'exploitation est souvent la solution qui va nous permettre de sortir du problème en dernier recours.

Pour ce faire, il nous faut nous y préparer, car un système bloqué, sur lequel on a toujours nos données, doit être sauvegardé avant la réinstallation (car celle ci entraîne souvent la perte de toutes les données)

Si vous étiez sous Windows, on peut également se poser la question de passer sous Linux.

## Être prêt à panne la plus embêtante

OK, on vient de voir plein de pannes possibles qui vont du simple problème logiciel à la panne matérielle embêtante, voir à l'ordinateur Hors Service ! Le pire des cas, malgré ce que l'on pense, n'est pas forcément un ordinateur hors service. 

La panne la plus embêtante, est la **perte de données** sans espoir de les recouvrer. 

Car si l'ordinateur est hors-service, on peut toujours parfois récupérer les données qu'il contenait, en extrayant son disque dur pour en récupérer les. On peut ensuite remplacer les pièces défectueuses. C'est une perte d'argent et de temps, mais très souvent, la perte de données est bien plus problématique.

C'est pour cela qu'il existe une règle d'or, au dessus de n'importe quelle autre :

> **Il faut considérer, en toutes circonstances, que notre matériel informatique peut disparaître du jour au lendemain et toutes ses donnés avec.**

Afin de se préserver de cela, il est important de préserver ses données. L'informatique a ceci de pratique qu'un fichier peut être copié à l'infini, et ce presque gratuitement. Il est donc primordial de :

-  Conserver ses données critiques sur sa machine, mais également les **synchroniser sur un stockage de données en ligne**. Il existe pour celà plusieurs hébergeurs, certains peu recommandables au niveau de la vie privée, comme [Dropbox](https://www.dropbox.com/), [Microsoft OneDrive](https://onedrive.live.com/), ou encore [Google Drive](https://www.google.com/drive/).  Apple permet également à ses utilisateurs de synchroniser ses fichiers sur [iCloud](https://www.icloud.com/). Parallèlement à celà, il existe des centaines de petits hébergeurs associatifs tels que la fédération [CHATONS](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=271&field_is_shared_value=All&title=), la plupart utilisent [Nextcloud](https://nextcloud.com/fr/) comme solution d'hébergement de fichiers, et 
- **Utiliser un système de sauvegarde** (backup) et faire des sauvegardes régulières de vos données sur un support physique. Ce n'est pas incompatible avec avoir un stockage en ligne, et c'est même bien, en cas de mauvaise manipulation et d'effacement de données sur ce stockage. Il existe beaucoup d'outils de sauvegarde, gratuits et puissants, et même utiliser un vieux disque dur externe que l'on laisse sur l'étagère fait office de bonne sécurité pour les données.

<u>Quelques logiciels de sauvegarde:</u>

- [Pika Backup](https://apps.gnome.org/PikaBackup/) sous Linux, notre préféré <3, rapide et facile à utiliser et peut même se synchroniser à des sauvegardes en ligne.
- On trouvera aussi sous Linux, [Déjà Dup Backups](https://apps.gnome.org/DejaDup/) qui a peu ou prou les mêmes fonctionnalités, mais utilise un système de sauvegarde différent (duplicity)
- [Duplicati](https://www.duplicati.com/download) est une très bonne alternative également, fonctionnant sous Windows, Mac et Linux, il utilise (à priori) son propre système de sauvegarde. Il fonctionne à travers une interface web, ce qui en fait aussi un bon outil pour sauvegarder ses données si vous faites un serveur à la maison.

## Prendre soin

Au delà de savoir réparer un ordinateur lorsqu'une panne survient, il est impératif de prévenir tout dommage qui pourrait advenir au matériel informatique : **prendre soin** apparaît comme une évidence, mais c'est surtout une nécessité, afin de préserver notre matériel et le faire durer le plus possibles. Quelques gestes qui peuvent aider:

- Une fois le matériel éteint, (PC Fixes), éteindre les multiprises, ou utiliser des prises équipées de parasurtenseurs. Toujours brancher un ordinateur sur une prise équipée de la terre.

- Pour du matériel critique (Ordinateurs de travail) : utiliser un [onduleur](https://www.materiel.net/onduleur/l515/), qui permettra à votre ordinateur de bureau de s'éteindre proprement en cas de coupure de courant.
- Éviter de déplacer les PC Fixes, autant que possible, et les garder fermés, dans un endroit peu exigu et à l'abri de la poussière.
- [Dépoussiérer ses ordinateurs](https://lecrabeinfo.net/nettoyer-et-enlever-la-poussiere-de-son-ordinateur.html) régulièrement. Attention à la méthode, de ne pas endommager votre ordinateur pendant le dépoussiérage.
- Ne pas boire de café/thé/coca à coté de votre ordinateur, portable ou non. Les liquides renversés sur les ordinateurs sont très souvent à l'origine de court-circuits endommageant le matériel, souvent de manière fatale.
- Toujours utiliser une [housse de protection](https://www.materiel.net/sac-sacoche-et-housse/l411/+fv387-10432/) rembourrée pour les ordinateurs portables lors de leur transport. Et éviter de se déplacer avec l'ordinateur ouvert et dans les bras lorsque l'on est dans la maison / au bureau (pour éviter tout choc).
- Utiliser une plaque refroidissant pour ordinateur portable, afin de maîtriser sa température lorsqu'il est en fonctionnement et en travail intensif.

## Aller plus loin

Le site I Fix It propose une page très complète pour la réparation des **Ordinateurs Portables** et Ordinateurs de bureau. Vous pourrez y trouver des informations très détaillées pour la réparation, ainsi que des guides pour chacun des appareils qui y ont été documentés. Le votre est peut être dans la liste !

- https://fr.ifixit.com/Device/PC_Laptop#main
- https://fr.ifixit.com/Device/PC_Desktop#Section_Changement
