# Comprendre


## Écologie, Émancipation : Les enjeux du Libre Emploi

Article : [Les grands enjeux](enjeux/general.md) (15 minutes de lecture)

Dans cet article, nous vous présentons ce qui est à l'oeuvre dans notre rapport aux appareils numériques, tant du point de vue des **consommateurs**, que celui des **constructeurs** et **développeurs** de logiciels. 

Il présente également les **impacts** sociaux, économiques et écologiques liés à l'utilisation du numérique, et les biais qui guident et aliènent notre comportment de consommateur de numérique.

---

Article : [La Domestication Numérique](enjeux/domestication.md) (20 minutes de lecture)

Cet article présente un concept énoncé par le passé par [Benjamin Bayart](https://fr.wikipedia.org/wiki/Benjamin_Bayart) (*si je me souviens bien, retrouver la référence, probablement une de ses nombreuses conférences, ou une interview de thinkerview*). La domestication numérique est un terme emprunté à la domestication humaine et animale pour faire le parallèle à un asservissement consenti au numérique.

## Comprendre son Ordinateur


#### [Y'a quoi là dedans?](guides-pc/dans-mon-ordinateur.md)

#### [Démarrage de l'ordinateur](guides-pc/demarrage.md)

- [BIOS et UEFI](guides-pc/bios-uefi.md) : quelles différences?

- [Mon ordinateur est lent ! Que faire ?](guides-pc/diagnostic-lenteur.md)

- [Pourquoi réinstaller? Quoi réinstaller?](guides-pc/reinstall.md)

- [Avant de réinstaller : Checklist](guides-pc/reinstall-checklist.md) : Se préparer au Grand saut.

- [Ventoy](guides-pc/ventoy.md) : Un outil pour réinstaller son ordinateur

## Systèmes d'exploitation (Work in progress)

#### [Linux](guides-pc/linux.md) : Un système résilient qui respecte votre ordinateur

- [C'est quoi une distribution linux?](guides-pc/linux/distributions.md)

- [Tester, et Installer Linux](/guides-pc/live-usb) via une clé USB dite "Live". 

#### [Windows](guides-pc/windows.md) : Le choix par défaut, le meilleur?

- [Installation de Windows](guides-pc/windows/install.md)

- [Après l'installation de Windows: protéger sa vie privée](guides-pc/windows/post-install.md)

- [Alléger un système Windows](guides-pc/windows/optimiser.md)

#### [macOS](guides-pc/macos.md) : Une illusion de liberté?



 

