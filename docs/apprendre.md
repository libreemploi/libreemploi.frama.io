# Apprendre à faire soi-même

## Matériel

- [Remettre en état un Ordinateur](guides-pc/remettre-en-etat.md) : Votre ordinateur est trop vieux? Trop lent ? Voici quelques pistes pour remettre en état votre machine et lui refaire une jeunesse.

## Dépanner et Réparer

* AU SECOURS : [Mon ordinateur ne démarre plus](guides-pc/mon-ordinateur-ne-demarre-plus.md) ! Un guide assez exhaustif et des méthodes permettant d'identifier d'où vient le problème, et comment dépanner un ordinateur qui ne démarre pas.

## Installer et Utiliser Linux

- [Essayer Linux grâce à une Clé USB "Live"](guides-pc/live-usb.md) : Si vous possédez une clé USB, vous pouvez tester une distribution Linux sur votre ordinateur sans rien changer, puis l'installer si vous voulez franchir le pas.

- [Installer Linux sur Votre ordinateur](guides-pc/installer-linux.md) : Cette page regroupe toutes les informations pour commencer à installer linux sur un ordinateur, et ne rien oublier au passage. 

## Utiliser son Ordinateur

- [Choisir un Navigateur](guides-pc/choisir-un-navigateur.md) : Aujourd'hui, beaucoup de navigateurs internet existent, et on se contente souvent du choix par défaut. Cet article présente les différentes alternatives et tente de les comparer du point de vue de leur **performance**, l'**utilisation mémoire** ainsi que leurs **pratiques éthiques**.



