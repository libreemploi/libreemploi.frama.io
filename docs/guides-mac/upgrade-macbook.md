# Refaire une jeunesse à un Mac / Macbook



## Les différentes Générations

Au fil du temps, Apple a produit différents modèles avec différentes particularités principales. Si on s'intéresse aux modèles des 20 dernières années (environ depuis 2004) on peut noter 3 grandes "ères" pour ses ordinateurs :

- L'ère PowerPC : les macs ayant été produits avant 2008, utilisaient des processeurs PowerPC de Motorola, très différents de ceux des PC, qui utilisent eux des processeurs x86 et x64 de Intel. Très anciens et aujourd'hui difficiles à garder en état de marche, ils ont été un temps des exemples de réparabilité et d'ouvrage matériel.
- L'ère Intel : De 2008 à 2019, Apple a décidé de changer de type de processeur et s'est fourni chez Intel. Ces ordinateurs ont souvent été qualifiés de MacIntel. C'est à cette époque que les "Grandes Innovations" matérielles sont survenues (écran rétina, force touchpad), mais également à cette période que Apple a pris un tournant très restrictif en matière de réparabilité et de mise à jour ou remplacement des composants.
- Depuis 2019, Apple a choisi de produire ses propres processeurs, sur une base de processeurs ARM, les mêmes qui équipent nos smartphones. Ces processeurs sont en vogue aujourd'hui car ils ont une bien meilleure efficacité énergétique. 

## Au niveau Matériel

Renommé pour son excellence matérielle, Apple a

## Au niveau Logiciel

### Open Core Boot

### Linux

