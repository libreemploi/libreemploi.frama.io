# Comment nous aider?

Pour l'instant, je dirais que la meilleure manière serait déjà de discuter ensemble de ce que nous pourrions faire de cet espace des communs. 

Le libre emploi a pour vocation d'être un espace où chacun peut apporter son expérience à travers des guides et des articles :

- Simples et concis
- Adaptés au grand public
- Capables de vulgariser la complexité

Actuellement, le site est en accès collaboratif sur [framagit](https://framagit.org/https://framagit.org/libreemploi/libreemploi.frama.io) sous forme d'un projet MkDocs.


## Contribuer au Site

[MkDocs](https://www.mkdocs.org/) est un système python capable de générer un site web à partir de simples documents rédigés en syntaxe [markdown](https://docs.framasoft.org/fr/grav/markdown.html), il permet d'éditer ces fichiers via des applications simples comme [MarkText](https://www.marktext.cc/), [Typora](https://typora.io/) (non libre), ou encore [Marker](https://flathub.org/fr/apps/com.github.fabiocolacio.marker)

La contribution au site est pour le moment un peu complexe et nécessite un peu de connaissances de [git](https://www.git.org) et [python](https://python.org/). Afin de contribuer, vous pouvez passer par le système de demandes de fusion (Merge Requests) de framagit, afin d'ajouter ou de modifier des articles.

Nous réfléchissons à une manière plus simple d'accepter et de valider des contributions d'utilisateurs qui ne nécessiterait pas l'utilisation un peu fastidieuse de git.

### Configuration et Installation (Juste une fois)

Actuellement la manière la plus simple de contribuer est depuis un système libre Linux (ou un système Linux WSL sous Windows)

Afin de pouvoir contribuer au site, il est nécessaire d'avoir installé sur votre machine:

- un client git, ou le package git sur votre système Linux
- le package CMake
- python en version 3.10 ou plus récent (ainsi que les modules pip et venv)
- un editeur de texte et de markdown
- un navigateur web, par exemple [Firefox](https://www.getfirefox.com)

Pour installer sur un système Ubuntu, Debian ou Linux mint :

`sudo apt update && sudo apt install python3 python3-pip python3-virtualenv cmake git`

Pour installer sur un système RedHat, Fedora ou CentOS :

`sudo dnf install python3 python3-pip python3-virtualenv cmake git`

Une fois les packages installés, vous pouvez cloner le dépot du libre emploi dans le dossier de votre choix

`git clone https://framagit.org/libreemploi/libreemploi.frama.io.git`

Pour entrer dans le dossier du dépot sur votre disque :

`cd libreemploi.frama.io`

Puis, effectuer la première fois, la configuration initiale pour travailler

`make install`

Désormais vous êtes prêt à travailler.

## Prévisualiser et construire le site (localement)

Lorsque vous voulez travailler sur le site localement et y apporter des modifications, tout se passe actuellement en ligne de commande. Il faut d'abord entrer dans le dossier du dépot:

`cd libreemploi.frama.io`

Puis activer l'environnement virtuel (créé par la commande `make install` lors de l'installation)

`source .venv/bin/activate`

une fois activé, votre terminal devrait préfixer l'invite de commande d'un `(.venv)`

désormais, vous pouvez reconstruire le site (dans le dossier Public) via la commande :

`make build`

si vous voulez travailler localement et tester vos changements directement dans un navigateur, vous pouvez utiliser la commande

`make serve`

et aller sur l'adresse https://127.0.0.1:8080/ dans votre navigateur