---
template: blogpost.html
title: Nos produits électroniques ont changé en 20 ans - Les systèmes pour gérer les déchets électoniques n'arrivent plus à suivre
date: 2023-06-17
author: Thomas Iché
image: ../images/header-blog-default-2.jpg
---

> Traduction de l'article : [Consumer electronics have changed a lot in 20 years – systems for managing e-waste aren’t keeping up](https://theconversation.com/consumer-electronics-have-changed-a-lot-in-20-years-systems-for-managing-e-waste-arent-keeping-up-147972) sur [The Conversation](https://theconversation.com/) du 2021-01-11

Auteurs originaux : 

- Callie Babbitt - Associate Professor of Sustainability, Rochester Institute of Technology
- Shahana Althaf - Postdoctoral associate, Yale University

(Sources et liens en anglais, non traduits)

---

Difficile d'imaginer vivre dans notre monde moderne [sans un téléphone portable à la main](https://blogs.worldbank.org/opendata/are-cell-phones-becoming-more-popular-toilets). Nos ordinateurs, tablettes, et smartphones ont transformé nos manières de communiquer, trvailler, appendre, partager l'actualité et nous divertir. Ils sont devenus encore plus essentiels, lorsque la pandémie de COVID-19 a déplacé l'école, le travail et les relations sociales sur internet.

Pourtant, peu de gens réalisent que notre dépendance à l'électronique est accompagné de coûts environnementaux vertigineux, allant de l'exploitation minière, jusqu'au traitement des déchets électroniques. Les consommateteurs peinent à résister à des produits toujours plus rapides, avec plus de stockage, de meilleures caméras, et leur renouvellement permanent ont créé [un problème croissant de gestion des déchets](https://time.com/5594380/world-electronic-waste-problem/). Sur la seule année 2019, [53 millions de mètres cubes de déchets électroniques ont été générés par l´humanité](https://www.itu.int/en/ITU-D/Environment/Documents/Toolbox/GEM_2020_def.pdf). 

A travers notre travail de chercheurs en résilience, nous étudions comment les comportmenents des consommateurs et l'innovation technologique influencent les produits que les gens achètent, combien de temps ils les gardent, et comment ces objets sont réemployés ou recyclés.

Nos recherches montrent qu'alors que la génération de déchets électroniques augmente au niveau mondial, [elle tend à décliner aux états unis](https://doi.org/10.1111/jiec.13074). Mais alors que certaines innovations aident à réduire le flux de déchets, celles-ci rendent également les produits plus difficiles à réparer et à recycler.

![Envoyer les déchets électroniques à la déchetterie nous empêche de récupérer des matériaux essentiels au recyclage. Joe Sohm/Visions of America /Getty Images ](https://images.theconversation.com/files/376843/original/file-20201231-15-1o0ofkb.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=1000&fit=clip)

> Envoyer les déchets électroniques à la déchetterie nous empêche de récupérer des matériaux essentiels au recyclage. Joe Sohm/Visions of America /Getty Images

## Du recyclage des appareils électroniques

Trente ans de données nous montrent pourquoi le volume de déchets électroniques aux états-unis sont en baisse : les nouveaux produits sont [plus légers et plus compacts que leurs prédécesseurs](https://apnews.com/article/bb5ff45b98f64123b3d408dd4a336b59). Les Smartphones et les ordinateurs portables ont supplanté les ordinateurs de bureau. Nos téléviseurs avec de fines dalles LCD ont remplacé les bonnes vieilles énormes [télévisions cathodiques](https://en.wikipedia.org/wiki/Cathode-ray_tube), et tous les services de streaming font maintenant le travail que faisaient auparavant des appareils indépendants : Lecteurs de CD, DVD, chaines Hi-Fi et balladeurs audio. Aujourd'hui les foyers américains produisent environ [10% de moins de déchets électroniques par tonne](https://doi.org/10.1111/jiec.13074) que lors de leur pic en 2015.

La mauvaise nouvelle, c'est que [seulement 35% de ces déchets sont recyclés](https://www.epa.gov/facts-and-figures-about-materials-waste-and-recycling).
Les consommateurs ne savent généralement pas où confier leurs déchets électroniques pour les recycler. Si on les entrepose dans des décharges, ils se décomposent en produits toxiques et polluants, qui inflitrent la terre environnante. Parmi ces substances, on compte le [plomb](https://doi.org/10.1080/10962247.2019.1640807) employé dans les anciennes cartes de circuits imprimés, le mercure, présent dans les premières dalles LCD, ainsi que des [additifs inifuges](https://www.aljazeera.com/news/2020/9/30/toxins-in-plastics-blamed-for-health-environment-hazards) dans les plastiques. Cette pollution pose de graves problèmes aux populations environnantes ainsi qu'à la biodiversité.

Il y a un besoin urgent de recycler ces déchets, à la fois pour la protection de la santé publique, mais également pour récupérer de précieux métaux. Ces minéraux rares sont généralement exploités dans des lieux dont les impacts sociaux et écologiques [exposent ces pays à de graves pollutions](https://www.washingtonpost.com/graphics/business/batteries/congo-cobalt-mining-for-lithium-ion-battery/). Le réemploi et le recyclage permettent de réduire la demande en “[minéraux conflictuels](https://www.newsecuritybeat.org/2020/09/companies-struggle-comply-conflict-mineral-reporting-rules/)” et permettraient de [créer de nombreux emplois et revenus](https://www.weforum.org/agenda/2019/01/how-a-circular-approach-can-turn-e-waste-into-a-golden-opportunity/).

Toutefois, ce n'est pas un processus simple : le démontage et la séparation des éléments électroniques est un procédé coûteux et fastidieux. 

Certaines entreprises ont illégalement [entreposé](https://resource-recycling.com/e-scrap/2020/12/03/former-president-of-crt-processor-sentenced-to-prison/) ou [abandonné](https://resource-recycling.com/e-scrap/2013/08/23/abandoned-warehouses-full-crts-found-several-states/) des Déchets électroniques. Un dépôt de Denver a même été qualifié de “[désastre environnemental](https://resource-recycling.com/e-scrap/2013/08/23/abandoned-warehouses-full-crts-found-several-states/)” lorsque 8000 tonnes de tubes cathodiques au plomb furent découverts sur ce site en 2013.

Les états-unis [exportent 40% des déchets électroniques produits](https://www.pbs.org/newshour/science/america-e-waste-gps-tracker-tells-all-earthfix). Une partie est emmenée dans des régions, telles que l'asie du sud-est où règnent [très peu de régulation environnementale et de protection des conditions de santé des travailleurs](https://www.nytimes.com/2019/12/08/world/asia/e-waste-thailand-southeast-asia.html) qui réparent ou recyclent de l'électronique.

## Démonter des appareils, remonter des données

Les risques sur la santé et l'environnement ont conduit 25 états ainsi que le district de Columbia a mettre en place [des lois sur le recyclage des déchets électroniques](https://www.ecycleclearinghouse.org/maps.aspx).
Certaines de ces mesures ont interdit d'entreposer des produits électroniques dans des décharges, alors que d'autres ont obligé les fabricants à financer l'effort de recyclage. Toutes ces lois ciblent les plus gros appareils, qui peuvent contenir jusqu'à 2kg de plomb.

Nous vouliions savoir si ces lois, adoptées entre 2003 et 2011 pouvaient soutenir l'effort sur la génération actuelle de déchets électroniques. Pour en avoir une idée claire, il nous a fallu revoir les estimations des quantités produites de déchets électroniques aux états-unis.

Nous avons collecté les informations concernant les ventes de produits électroniques des [années 1950](https://www.theatlantic.com/technology/archive/2014/04/a-terminal-condition/361313/) à nos jours, grâce aux rapports industriells, données publiques du gouvernement, et les sondages publics. Puis, nous avons [démonté presque 100 appareils](https://doi.org/10.1038/s41597-020-0573-9), de vieux magnétoscopes, à de très récents smartphones, pour peser et identifier les matériaux qu'ils contiennent.

![](https://images.theconversation.com/files/374938/original/file-20201214-18-e30oa9.PNG?ixlib=rb-1.1.0&q=45&auto=format&w=1000&fit=clip)

> Un chercheur démonte un smartphone pour identifier les matériaux qu'il contient. Shahana Althaf, CC BY 

![](https://images.theconversation.com/files/374942/original/file-20201214-21-1eto45i.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=1000&fit=clip)

> Cette tablette numérique a été disséquée et montre ses components. Chacun a été noté, pesé et analysé par les chercheurs. Callie Babbitt, CC BY 

Nous avons crée un [modèle d'analyse des données](https://doi.org/10.5281/zenodo.3986969), qui a produit un des plus détaillés rapports de consommation et recyclage électronique dans les états-unis à ce jour.

## Les déchets électroniques sont plus minces, mais pas forcément plus verts

La grosse surprise de ce rapport fut que les ménages américains [produisent moins de déchets électroniques qu'auparavant](https://doi.org/10.1111/jiec.13074),

Toutefois, ces innovations n'ont pas eu qu'un impact positif : pour réduire la taille et le poids des appareils, les fabricant ont miniaturisé les composants et collé des parties ensemble, les rendant difficiles à réparer et coûteuses à recycler. Les [batteries Lithium-ion](https://doi.org/10.1007/s10098-020-01890-3) posent un autre problème : Elles sont difficiles à détecter et retirer, car elles peuvent causer des [incendies désastreux](https://www.theverge.com/2020/2/28/21156477/recycling-plants-fire-batteries-rechargeable-smartphone-lithium-ion) lors de leur transport, démontage ou recyclage.

Les fonctionnalités dernier cri - des processeurs rapides, écrans ultra-hd tactiles ultra précis, et l'autonomie grandissante - impliquent l'utilisation de métaux tels que le cobalt, l'indium ainsi que des [terres rares](https://theconversation.com/what-are-rare-earths-crucial-elements-in-modern-technology-4-questions-answered-101364) qui requièrent énormément d'énergie à extraire du sol (NDT: sans compter la pollution des sols). Les techologies de recyclage ne semblent pas pouvoir les récupérer de manière rentable. Toutefois, certaines innovations commencent à émerger.

![](https://images.theconversation.com/files/376830/original/file-20201231-49513-1tf9ypc.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=1000&fit=clip)

> Le nouveau robot de Apple, Daisy, peut démonter 9 modèles différents d'iPhone pour en récupérer des matériaux que les appareils de recyclage traditionnels en sont incapables. Apple 

## Reléguer le déchet à sa place de ressource

Nous sommes persuadés que ces nouveaux défis vont demander une [approche proactive](https://doi.org/10.1016/j.resconrec.2019.05.038) qui traite les déchets électroniques en tant que resources, et non des déchets. On considère désormais que l'Or, l'argent, le palladium, ainsi que d'autres matériaux précieux sont plus concentrés dans nos déchets électroniques qu'ils le sont dans le sol. 

“[Le minage urbain](https://www.bbc.com/future/article/20200407-urban-mining-how-your-home-may-be-a-gold-mine),” dans sa forme du recyclage électronique pourrait remplacer les besoins en forages et minage de métaux de plus en plus rares, en réduisant les dommages causés à la nature. Celà mènerait également à [réduire la dépendance des USA](https://doi.org/10.1016/j.resconrec.2020.105248) aux [minéraux importés aux autres pays](https://www.cato.org/blog/chinas-critical-minerals-national-security-meaning-supply-chain-interdependence).

![](https://images.theconversation.com/files/376714/original/file-20201228-17-1yhxq7y.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=754&fit=clip)

> Concentration de produits toxiques (gauche) et précieux (droite) dans les produits déchets électroniques aux U.S.A. Althaf et al. 2020

Le gouvernement, les industriels, et les consommateurs ont tous un rôle à jouer. Le progrès va requérir une conception de produits [faciles à réparer](https://www.ifixit.com/) et réutiliser, ainsi qu'inciter les consommateurs à [garder leurs appareils plus longtemps](https://earth911.com/eco-tech/ways-to-reuse-old-laptop/).

Nous avons également besoin de mettre à jour nos lois en place qui forment maintenant un patchowrk de régulations désuettes. Établir des lieux [pratiques](https://knowledge.wharton.upenn.edu/article/how-u-s-laws-do-and-dont-support-e-recycling-and-reuse/), [fiables](https://sustainableelectronics.org/recyclers) [de collecte et recyclage](https://e-stewards.org/) pourra nous préserver de ces appareils dans nos décharges. 

Avec des opérations standardisées, les opérateurs de recyclage pourront récupérer bien plus de matériaux précieux de notre flux de déchets électroniques.

C'est grace à des efforts comme celui ci que nous pourrons retrouver l'équilibre entre les besoins électroniques, tout en protégeant la santé de tous, et l'environnement.

> ## Notes du traducteur
> J'ai trouvé intéressant de traduire cet article car il résume bien l'évolution de la composition des appareils électroniques et le retard des 20 dernières années quant aux changements de composition de ces appareils. <br/> <br/> Aujourd'hui avec la raréfaction des métaux et les externalités minières, il est à noter que les pouvoirs publics n'ont pas suivi.<br/> La miniaturisation et l'irréparabilité actuelles posent des problèmes bien plus graves qu'auparavant car les appareils sont désormais très difficiles à recycler, malgré le fait qu'ils contiennent moins de polluants. <br/><br/> Il en ressort très clairement un manque d'information du public quant à ces enjeux : Comment reprendre le pouvoir afin de pouvoir faire durer nos appareils? Quelles actions au moment du recyclage? <br/><br/>  Pour aller plus loin, je vous conseille également [cette conférence d'Aurore Stéphant (SystExt)](https://www.youtube.com/watch?v=i8RMX8ODWQs). 

