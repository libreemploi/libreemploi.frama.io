---
template: blogpost.html
title: Transcript - Jancovici à propos des impacts du numérique (conférence chez Orange)
date: 2023-10-08
author: Thomas Iché
image: ../images/header-jancovici-860x200.jpg
---

> Transcript adapté et commenté de la vidéo youtube : [Jancovici démonte les avantages du numérique chez Orange](https://www.youtube.com/watch?v=njtez3Md0bs) de la chaine [Ethique et Tac](https://www.youtube.com/@ethiqueettac) du 2023-10-08 (abonnez vous, elle est top!)

**Sources et Ressources:**

- Plan de transformation de l'économie française : https://ilnousfautunplan.fr/le-plan/
- Travail du shift sur le numérique : https://theshiftproject.org/category/thematiques/numerique/
- Atelier de sensibilisation - La fresque du numérique: https://www.fresquedunumerique.org/

> **Ce n'est pas tous les jours qu'on a des vidéos sur youtube qui collent parfaitement aux enjeux du libre emploi, et ce soir était un soir un peu particulier. J'aime bien Jean-Marci Jancovici pour son franc-parler et c'est un de ceux qui a réussi à me faire mettre un pied à l'étrier sur toutes ces problématiques. **
>
> **Après, il faut dire que le bougre sait s'y prendre pour parler à des capitalistes, même si bon, passé son côté taquin, je crains fort qu'il ne soit rien de plus qu'un token. Au final, son intervention ici chez Orange, à propos du numérique résume bien la panade dans laquelle on est et les efforts à fournir. Il présente, au delà des enjeux du CO2 et l'énergie (son domaine de prédilection), les enjeux de ressources métalliques liées à la fabrication et au recyclage.**
>
> **Son interlocutrice, laisse également soupirer le conflit d'intérêt RSE*, tout bonnement insoluble, auquel est confronté l'entreprise.**

Note: j'ai raccourci le transcript pour me focaliser sur les réponses de Jean-Marc Jancovici et moins sur son interaction.

---

## Pourquoi pas de chapitre sur le numérique dans le PTEF*?

(* PTEF = Plan de transformation de l'économie française)

Alors il n'y a pas de chapitre sur le numérique dans le PTEF, tout simplement parce qu'on avait déjà commis plusiers travaux sur le sujet auparavant, au Shift Project et donc c'est une simple question de forme. 

En l'esprit vous pouvez considérer que ce que l'on a proposé sur le numérique en fait partie aussi ; on n'est juste pas allé au stade des recommandations. On n'était pas mature sur le numérique au moment où on a fait le PTEF, pour pouvoir proposer des recommandations pour aligner le numérique avec des baisses de 5% par an. Mais c'est *work in progress* au sens où on est en ce moment en train de trouver -*parce que nous sommes une petite organisation*- le financement pour faire ça.

Donc en fait ce qu'on voudrait faire c'est un [livre blanc](https://fr.wikipedia.org/wiki/Livre_blanc) du numérique, pour aligner le numérique avec les 5% de baisse des émissions par an* 

> Note: le PTEF a pour objectif de réaliser une baisse des émissions de GES (dont CO2) de 5% chaque année, 5% correspondant à la baisse observée en 2020 due aux confinements et au ralentissement de l'activité Française. L'objectif est de ralentir chaque année de 5% par rapport à l'année précédente.

Alors [...] on a du mal à trouver des gens qui nous financent facilement tout simplement parce que, nonobstant les déclarations faites sur le côté "je vais diminuer les émissions chez tous les autres grâce au numérique", en fait les gens savent très bien que c'est pour partie de la façade. Et que si on veut aligner l'industrie du numérique avec une baisse de 5% par an dans le monde, ça va forcément contracter le numérique.

Alors pourquoi ça va contracter le numérique? [...] Quand vous regardez d'où viennent les émissions du numérique dans le monde, qui font 4%. Alors c'était 4% en 2018 quand on a sorti le rapport du shift project, aujourd'hui c'est peut être 5(%), des émissions planétaires.... tout compris.

Là dedans vous avez en gros la moitié qui est liée à la fabrication et au renouvellement des composants matériels : donc écran connecté que j'ai sous le nez, là, les téléphones que vous avez tous dans votre poche, les ordinateurs que vous avez, les composants de réseau (donc ce qui permet de faire le [backbone](https://fr.wikipedia.org/wiki/Dorsale_Internet) d'Orange, les antennes relais, etc.).

Les émissions **chaque année** liées à la **fabrication** de tout ça pour **accroitre** ou **renouveler** le parc, c'est **la moitié de l'empreinte**.

L'**autre moitié de l'empreinte** ce sont les émissions de **fonctionnement** de tout ça, dans laquelle vous avez, sur 50% des émissions :

- 20% liés aux fonctionnement des **serveurs**
- 30% qui viennent du fonctionnement de tout le reste (les réseaux et les terminaux)

Si on veut baisser ça de 5% par an, on n'y arrivera pas avec des mesures strictement techniques, au surplus dans un monde où le trafic *data* doit augmenter de quelques dizaines de % par an.

(Ça doit être à peu près ça, par exemple les banques c'est 30% par an)

Ça voudrait dire qu'on arrive à faire tous les ans 35% d'efficacité énergétique (par octet ou par cm carré d'écran), et je ne pense pas qu'on y soit. (ndlr : différent de la loi de [koomey](https://fr.wikipedia.org/wiki/Loi_de_Koomey) qui ne s'intéresse qu'à la puissance de calcul, et pas aux réseaux)

Donc, bien entendu, si on veut baisser les émissions du numérique de 5% par an, comme celles de tout le monde, on va devoir **limiter les usages**, et en particulier **l'usage de la vidéo**, voilà... c'est ça qu'on va devoir limiter.

On est passé dans des systèmes qui sont beaucoup plus gourmands ces dernières années et dernières décennies, par exemple, la télévision hertzienne (numérique ou pas) était bien moins gourmande en ressources que la télévision point à point qui passe par le réseau fibre. C'est évident.



> **NDLR, Petite Explication : ** lorsque la télévision était diffusée par voie aérienne (hertzienne), un seul flux de données par chaine transitait dans les airs, envoyé par une tour radio, et nos antennes râteau venaient "écouter" les flux en fonction de la chaine souhaitée.
>
> Dans le cas point à point : désormais, une **copie du flux** est réalisée et **routée** à travers internet, pour **chaque téléviseur ou box connectée**, ce qui revient à un flux de données infiniment plus conséquent et un encombrement des infrastructures réseau (nécessitant toujours plus d'équipement pour faire passer les flux)

## Au niveau de la consommation et des ressources

Donc, aujourd'hui on est plutôt dans le sens où on va vers une **augmentation de la consommation d'énergie et de matières par usage** plutôt que l'inverse.

Pour vous donner des ordres de grandeur, la **fabrication d'un smartphone** (d'un fabricant mondial que je ne nommerai pas, qui est une des premières capitalisations boursières du monde), c'est un peu moins de **100kg de CO2** donc typiquement, chez vous (ndlr: Orange),  une contribution à la baisse des émissions de CO2, c'est **d'arrêter de pousser les consommateurs à renouveler leur smartphone** et c'est **au contraire les pousser à le garder le plus longtemps possible**.

Idem pour les ordinateurs.

Et évidemment, si vous gardez vos téléphones le plus longtemps possible, ce qu'on a coutume d'appeler le progrès technique se passe **beaucoup moins vite** puisque à chaque fois que vous renouvelez votre appareil vous avez un truc qui prend des meilleures photos, enregistre plus de vidéos, a un meilleur débit,  etc. Si vous le gardez 10 ans au lieu de le garder 2 ans, vous avez pas ça.

> NDLR: JMJ fait un petit raccourci sur le fait que le progrès technique ne serait lié qu'au matériel et ses évolutions, pourtant, dans le numérique, la part logicielle et notamment son **optimisation** est critique dans l'évolution. Peu d'exemples sont présents, pourtant on peut noter l'arrivée du [project butter](https://www.phonandroid.com/project-butter-rappel-de-ce-que-vous-devez-savoir.html) sur android, qui a permis une plus grande optimisation du rendu et de la réactivité des interfaces, grâce à une optimisation logicielle. Celà a permis à de nombreux terminaux de pouvoir améliorer leur durée de vie.

Pareil pour les ordinateurs.

Et enfin, parce que le fonctionnement des serveurs c'est 20%, il faut **limiter l'augmentation des serveurs** et donc limiter **l'inflation de données**, et même **la faire baisser**.

## Un modèle de société dépendant du numérique

Alors ça peut aller chercher loin hein, dans le domaine du grand public, c'est ce que je viens d'évoquer : c'est limiter le renouvellement du matériel et limiter les débits vidéo. 

Mais dans le domaine professionnel, l'inflation réglementaire ça conduit à une inflation de data. Par exemple dans le domaine bancaire, l'obligation de la comparaison des prix, etc. ça duplique des flux.

[...] Mais c'est quelque chose qui pousse, structurellement... On a tendance, quand vous regardez bien ce qui se passe dans le monde, à **ajuster la complexité réglementaire du monde, à la puissance informatique**.

Alors demain matin, si on avait plus d'ordinateur du tout, on serait totalement infoutus de gérer le corpus réglementaire que nous avons aujourd'hui à gérer.

Pour commencer, on ne pourrait pas tenir vos comptes sur papier... Or vous êtes obligés de tenir des comptes.

## Le rapport aux resources qui composent nos appareils..

Donc vous avez aujourd'hui le corpus réglementaire on est incapable de le gérer sans un ordinateur, donc sans les 40 métaux que vous avez dans un ordinateur. Aujourd'hui les obligations réglementaires et le trafic data, ça porte un nom : ça s'appelle du **cuivre** de l'**indium**, de l'**arsenic**, du **chrome**, de l'**étain**, du **plomb**, du **fer**, etc.

Donc ce qu'on va trouver dans les ordinateurs, les terres rares vous en avez un peu, (de l'indium dans les écrans, du gadolinium, etc) mais vous avez surtout des métaux ordinaires, vous avez de l'or. En fait ce qui vaut le plus cher dans un smartphone quand on le recycle, c'est l'or.

Les deux métaux qu'on va récupérer dans un smartphone qu'on recycle, c'est l'**or** et le **cuivre**, éventuellement **l'étain**. Le reste part à la poubelle, parce que c'est trop dilué, trop mélangé, et l'énergie de récupération de chacun des composants pris individuellement est trop élevée.

Donc on en fait rien, on entasse ça dans un coin.... ou ça reste dans vos tiroirs car ça peut toujours servir disait Gad Elmaleh (ndlr j'ai pas la référence)

## .. le rapport à l'eau...

Vous avez également de l'eau, car on en utilise, et du reste ça peut être un sujet, parce que les fondeurs de semi-conducteurs quand ils sont dans un endroit où il y a une pénurie d'eau, parce qu'il y a une sécheresse, ça peut les affecter de deux manières différentes, d'abord parce que l'usine a besoin d'eau, et surtout parce que l'usine a besoin d'électricité, et si vous êtes dans un endroit, par exemple le Sichuan, où l'essentiel de la production est hydroélectrique, s'il n'y a plus assez d'hydro-électricité, on met la production des usines au ralenti.

Il est évident qu'aujourd'hui on a bati notre monde sur la disponibilité perpétuelle de matériaux qui ne sont pas perpétuellement disponibles, donc je vous invite à réfléchir a : qu'est ce qui se passe si demain matin on ne peut plus se réapprovisionner en puces, 

(on a déjà eu un petit apercu)

## .. et à la raréfaction des ressources minières

... voilà eh bien c'est un petit apéritif, parce que moins de pétrole c'est un monde qui va se démondialiser. Or les 50 métaux qu'il y a dans un ordinateur, y'en a pas 1 en France parce qu'il y a pas de mine, alors on pourrait rouvrir certaines mines, mais on les aura jamais tous les 50.

Parce que les composants miniers qui vont rentrer dans les ordinateurs, sont **de plus en plus difficiles à extraire de la croûte terrestre**, parce que la **teneur des mines en métaux baisse**.

Pour vous donner un ordre de grandeur, quand on a commencé à exploiter le cuivre en espagne à *Rio Tinto*, il y avait environ **200kg de cuivre par tonne de minerai**. Aujourd'hui la **teneur moyenne en cuivre**  moyenne **dans le monde**, c'est **4kg de cuivre par tonne de minerai**, donc ca veut dire que la quantité d'énergie qu'il faut dépenser pour extraire une tonne de métal des entrailles de la terre, n'arrête pas d'augmenter.

(NDLR : Sans compter l'utilisation importante d'eau et la pollution des sols liée aux boues d'extraction, et à tous les produits du raffinage)

Donc il faut plus de métal pour la transition, et il faut plus d'énergie par tonne de métal, donc on va avoir un petit sujet. Et puis cette énergie elle est essentiellement fossile.

Donc voilà, on a d'autre limites qui vont s'appliquer au numérique, que simplement la limite en CO2, il y a une limite de ressources, et une limite d'énergie.... et cette limite de ressources et cette limite d'énergie peuvent nous jouer de sales tours, et **finiront par nous jouer des sales tours si c'est pas nous qui mettons les limites**.

## Dernière question : le numérique permet il d'éviter des émissions dans d'autres secteurs ?

J'ai vu passer une étude qui a été commise par Orange je crois*, donc je vais dire beaucoup de mal de la maison qui vous héberge... disant que pour 1 émis dans le numérique, on économisait 10 ailleurs.

(* NDLR: en fait il s'agissait de l'ancien PDG de orange)

Je vais vous livrer une réflexion : l'empreinte carbone du numérique c'est 4%, si on était dans ce ration de 1:10 ça voudrait dire que le numérique aurait fait économiser 40% des émissions, je pense qu'on le verrait.

Donc en fait ce calcul ne repose sur rien : je ne l'ai pas lu dans le détail pour savoir où il pêche, mais c'est évident qu'il pêche. 

En fait quand vous regardez, c'est une courbe que je montre souvent (vous pouvez voir ça sur n'importe quel enregistrement vidéo de mes conférences), quand vous regardez la courbe des émissions des Gaz à effet de serre, dans le monde depuis 1 siècle et demi, on ne voit strictement aucune inflexion au moment où le digital prend son essor, aucune.

Ce qui veut dire que l'effet net du digital, ça n'a été absolument pas de ralentir la hausse des émissions.... sinon ça se verrait.

[...]

Et je ne vois pas comment les jeux en ligne, Netflix et le Porno, pourraient faire baisser les émissions.

