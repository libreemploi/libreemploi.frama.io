---
template: blogpost.html
title: Premier article de blog!
date: 2023-06-15
author: Thomas Iché
image: ../images/header-blog-default-1.jpg
---

Bienvenue à tous! 

Ceci est le premier post du blog du libre emploi! Ce blog recensera les actualités du site. Nous publierons des articles régulièrement sur ce blog pour vous présenter les nouveaux projets, outils et expérimentations.

---

Mais avant tout, je voulais présenter brièvement les enjeux du libre emploi:  Le site joue sur les mots "libre", "emploi" et partage les lettres "re" qui peuvent également former "réemploi". 

Le but de ce site est de chercher et trouver des méthodes applicables au public afin d'informer sur les enjeux du réemploi numérique, de l'obsolescence programmée, et comment la contrer à travers le logiciel libre.

Combattre l'obsolescence progammée grâce aux logiciels libres est un moyen d'émancipation pour les citoyens : il nous permet de garder le contrôle sur des appareils numériques dont l'usage devient de plus en plus soumis à la volonté des constructeurs:

- incompatibilités logicielles arbitraires
- support du matériel raccourci dans le temps
- interruption des mises à jour après un certain temps
- blocage des solutions alternatives à celles du constructeur

Ceci bien entendu ne s'applique pas seulement à nos ordinateurs, mais également à nos smartphones.


## C'est bien, mais du coup un logiciel libre c'est quoi?

Un [logiciel libre](https://fr.wikipedia.org/wiki/Logiciel_libre), à contrario d'un [logiciel propriétaire](https://fr.wikipedia.org/wiki/Logiciel_propri%C3%A9taire) (ou encore appelé __privateur__) est un logiciel dont le code code est ouvert au public et répond à 4 grandes libertés fondamentales:
- liberté d'*utiliser* le logiciels sans restriction ni contrainte (y compris financière)
- liberté d'*étudier* le code source utilisé
- liberté de *modifier* le code source pour l'*améliorer* ou encore l'*adapter* à nos besoins
- liberté de *copier* et *redistribuer* 

Il en existe un grand nombre qui, souvent, gravitent autour des systèmes GNU/Linux, mais pas seulement. Elles sont très souvent moins mises en avant, car alternatives à des solutions commerciales qui ont pignon sur rue et qui ont droit à de la publicité.

## Quelques exemples d'obsolescence programmée et de logiciels privateurs dans l'actualité récente (et un peu moins récente)

[(MacGénération) MacOS Sonoma : une compatibilité mac encore plus restreinte que pour Ventura](https://www.macg.co/macos/2023/06/macos-sonoma-une-compatibilite-mac-encore-plus-restreinte-que-pour-ventura-137319) : Dans cet article, Apple annonce qu'il va supporter encore moins de ses appareils sur la prochaine mise à jour. Le problème principal n'est pas réellement les fonctionnalités mais plutôt les mises à jour de sécurité (généralement supportées encore quelques années), et le support des apps, qui, pour beaucoup, se mettent à nécessiter la dernière version du système pour fonctionner. Sur le temps long, les appareils deviennent obsolètes car ils ne peuvent plus faire tourner les applications.


[(MacGénération) Mac Apple Silicon : il faut probablement faire le deuil des cartes graphiques dédiées et des eGPU](https://www.macg.co/materiel/2023/06/mac-apple-silicon-il-faut-probablement-faire-le-deuil-des-cartes-graphiques-dediees-et-des-egpu-137471) Dans cet article un peu technique, l'auteur explique les raisons de l'incompatibilité récente avec les GPU externes (des boitiers connectables à votre ordinateur portable), qui lui confèrent à la maison les performances d'une carte graphique de PC de Bureau ou de Jeu. Pas mal d'utilisateurs de mac s'en sont équipés au cours des années 2010 (surtout des professionnels), malheureusement, par un choix arbitraire, Apple décide de rendre ce matériel tout de même onéreux, incompatible avec ses nouveaux produits. Pire, son incompatibilité serait même matérielle. 

[Windows 11 : vous allez être obligé d’utiliser un compte Microsoft et une connexion Internet](https://www.frandroid.com/marques/microsoft/1232703_windows-11-pro-bientot-une-connexion-internet-et-un-compte-microsoft-obligatoire-pour-la-configuration). Dans cet article, on apprend que Microsoft retire sa fonctionnalité de Compte d'utilisateur local et force l'utilisation d'un compte dans le cloud sans raison apparente. Précédemment, il était encore possible de créer un compte local [en bataillant dans une interface un peu confuse](https://www.01net.com/astuces/comment-configurer-votre-nouveau-pc-windows-11-sans-compte-microsoft.html)

En bref, ce ne sont que quelques exemples parmi tant qui mettent en lumière que les produits numériques que nous achetons, sont soumis à des décisions des constructeurs, ou des développeurs qui nous échappent complètement.

## D'accord, c'est bien gentil, mais je peux faire quoi moi?

D'abord, la première chose à se demander, c'est si l'on a réellement besoin de changer d'ordinateur/smartphone lorsque notre matériel est décrété obsolète par les constructeurs ou les développeurs. La plupart du temps, nous cédons à la pression par manque d'information, mais également avec les injonctions de la publicité. 



Avant de changer un appareil électronique (smartphone, PC ou portable), la première chose à se demander est: est-ce que je peux installer un système alternatif 