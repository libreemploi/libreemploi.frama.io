# Expérimentations

## Restauration de machines

- ["Nadin"](#) - DELL Inspiron 1650 (2007)
- ["Matra"](#) - HP Elitebook ????
- ["Patate"](#) - ASUS EEEPC 1015 PN

## Smartphones et Tablettes
- ["Grouper 01"](#) - Nexus 7 Wifi 2013


## Articles à lire (en vrac)

- [Empreinte Environnementale et sécurité des logiciels Microsoft](https://www.cigref.fr/wp/wp-content/uploads/2021/10/08102021_CP_Empreinte-environnementale-et-securite-des-logiciels-Microsoft_FINAL.pdf) - CIGREF [Article lié](https://www.solutions-magazine.com/microsoft-halte-a-lobsolescence-programmee/)